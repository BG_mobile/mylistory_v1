package com.bugingroup.mylistory.AWS.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ImageView;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Various Http helper routines
 */
public class HttpUtils {

    /**v
     * Makes a http request to the specified endpoint
     */
    public static String invokeHttpRequest(URL endpointUrl,
                                           String httpMethod,
                                           Map<String, String> headers,
                                           byte[] req) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        HttpURLConnection connection = createHttpConnection(endpointUrl, httpMethod, headers);
        try {
            if ( req.length>0 ) {

                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.write(req,0,req.length);
                wr.flush();
                wr.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
            throw new RuntimeException("Request failed. " + e.getMessage(), e);
        }
        return executeHttpRequest(connection);
    }


    /**
     * Makes a http request to the specified endpoint
     */
    public static String invokeHttpGet(URL endpointUrl,
                                           String httpMethod,
                                           Map<String, String> headers,
                                           String requestBody) {
        System.out.println("--------- Request 0 ---------");
        HttpURLConnection connection = createHttpConnection(endpointUrl, httpMethod, headers);
        System.out.println("--------- Request 1 ---------");
        try {
            System.out.println("--------- Request 2 ---------");
            if ( requestBody != null ) {
                System.out.println("--------- Request 3 ---------");
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.writeBytes(requestBody);
                wr.flush();
                wr.close();
            }
        } catch (Exception e) {
            System.out.println("--------- Request 4 ---------");
            e.printStackTrace();
            System.err.println(e);
            throw new RuntimeException("Request failed. " + e.getMessage(), e);
        }
        return executeHttpRequest(connection);
    }

    public static String executeHttpRequest(HttpURLConnection connection) {
        System.out.println("--------- Request 5 ---------");

        try {
            // Get Response
            InputStream is;
            System.out.println("--------- Request 6 ---------");
            try {
                System.out.println("--------- Request 7 ---------");
                is = connection.getInputStream();
                System.out.println("--------- Request 7.1 ---------");
            } catch (IOException e) {
                System.out.println("--------- Request 8 ---------");
                e.printStackTrace();
                System.err.println(e);
                is = connection.getErrorStream();
            }
            System.out.println("--------- Request 9 ---------");
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            System.out.println("--------- Request 10 ---------");
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            System.out.println("--------- Request 11 ---------");
            rd.close();
            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
            throw new RuntimeException("Request failed0000" + e.getMessage(), e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static HttpURLConnection createHttpConnection(URL endpointUrl,
                                                         String httpMethod,
                                                         Map<String, String> headers) {
        try {
            HttpURLConnection connection = (HttpURLConnection) endpointUrl.openConnection();
            connection.setRequestMethod(httpMethod);

            if ( headers != null ) {
               System.out.println("--------- Request headers ---------");
                for ( String headerKey : headers.keySet() ) {
                    System.out.println(headerKey + ": " + headers.get(headerKey));
                    connection.setRequestProperty(headerKey, headers.get(headerKey));
                }
            }

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            return connection;
        } catch (Exception e) {
            throw new RuntimeException("Cannot create connection. " + e.getMessage(), e);
        }
    }

    public static String urlEncode(String url, boolean keepPathSlash) {
        String encoded;
        try {
            encoded = URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 encoding is not supported.", e);
        }
        if ( keepPathSlash ) {
            encoded = encoded.replace("%2F", "/");
        }
        return encoded;
    }

    public static void  DownloadImage(URL endpointUrl, ImageView imageView, Map<String,String> maps){

        try {

            byte[] bitmapdata ;

            URLConnection ucon = endpointUrl.openConnection();
            for ( String headerKey : maps.keySet() ) {
                System.out.println(headerKey + ": " + maps.get(headerKey));
                ucon.setRequestProperty(headerKey, maps.get(headerKey));
            }
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            ByteArrayBuffer baf = new ByteArrayBuffer(500);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            bitmapdata =  baf.toByteArray();
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata .length);
            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            Log.d("ImageManager", "Error: " + e.toString());
            e.printStackTrace();
            System.err.println(e);
        }

    }
}
