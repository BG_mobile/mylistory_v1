package com.bugingroup.mylistory;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.sevenheaven.iosswitch.ShSwitchView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bugingroup.mylistory.MainActivity.t;

/**
 * Created by Nurik on 11.11.2016.
 */

public class ActivityTheme extends Activity {

    @BindView(R.id.white) ShSwitchView white;
    @BindView(R.id.black) ShSwitchView black;
    @BindView(R.id.blue) ShSwitchView blue;
    @BindView(R.id.pink) ShSwitchView pink;
    @BindView(R.id.green) ShSwitchView green;

    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.done)
    TextView done;
    @BindView(R.id.back)
    ImageButton back;

    @BindView(R.id.equilibrium)
    LinearLayout equilibrium;

    @BindView(R.id.luxurios)
    LinearLayout luxurios;

    @BindView(R.id.legend)
    LinearLayout legend;

    @BindView(R.id.tenderness)
    LinearLayout tenderness;

    @BindView(R.id.cyaba)
    LinearLayout cyaba;

    int theme;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_profile49);
        ButterKnife.bind(this);
        SharedPreferences prefs = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
        theme = prefs.getInt("theme",0);
        switch (theme) {
                case Utils.THEME_DEFAULT:
                case Utils.THEME_WHITE:
                    white.setOn(true);
                    back.setColorFilter(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.white));
                    done.setTextColor(getResources().getColor(R.color.white));
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.golden));
                    ChangestatusBarColor(getResources().getColor(R.color.golden));
                    break;
                case Utils.THEME_BLUE:
                    blue.setOn(true);
                    back.setColorFilter(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.white));
                    done.setTextColor(getResources().getColor(R.color.white));
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.actionbar_color_bl));
                    ChangestatusBarColor(getResources().getColor(R.color.actionbar_color_bl));
                    break;
                case Utils.THEME_GREEN:
                    green.setOn(true);
                    back.setColorFilter(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.white));
                    done.setTextColor(getResources().getColor(R.color.white));
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.green));
                    ChangestatusBarColor(getResources().getColor(R.color.green));

                    break;
                case Utils.THEME_PINK:
                    pink.setOn(true);
                    back.setColorFilter(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.white));
                    done.setTextColor(getResources().getColor(R.color.white));
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.actionbar_color_pi));
                    ChangestatusBarColor(getResources().getColor(R.color.actionbar_color_pi));

                    break;
                case Utils.THEME_BLACK:
                    black.setOn(true);
                    back.setColorFilter(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.white));
                    done.setTextColor(getResources().getColor(R.color.white));
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.black));
                    ChangestatusBarColor(getResources().getColor(R.color.black));
                    break;
            }
        final SharedPreferences.Editor editor = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();

        white.setEnabled(false);
        black.setEnabled(false);
        blue.setEnabled(false);
        green.setEnabled(false);
        pink.setEnabled(false);

        equilibrium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                white.setOn(true,true);
                black.setOn(false,true);
                blue.setOn(false,true);
                pink.setOn(false,true);
                green.setOn(false,true);

                theme = Utils.THEME_WHITE;
                back.setColorFilter(getResources().getColor(R.color.white));
                title.setTextColor(getResources().getColor(R.color.white));
                done.setTextColor(getResources().getColor(R.color.white));
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.golden));
                ChangestatusBarColor(getResources().getColor(R.color.golden));

            }
        });

        luxurios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    black.setOn(true,true);
                    white.setOn(false,true);
                    blue.setOn(false,true);
                    pink.setOn(false,true);
                    green.setOn(false,true);
                    theme = Utils.THEME_BLACK;
                    back.setColorFilter(getResources().getColor(R.color.white));
                    title.setTextColor(getResources().getColor(R.color.white));
                    done.setTextColor(getResources().getColor(R.color.white));
                    ChangestatusBarColor(getResources().getColor(R.color.black));
                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.black));

            }
        });

        legend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blue.setOn(true,true);
                white.setOn(false,true);
                black.setOn(false,true);
                pink.setOn(false,true);
                green.setOn(false,true);
                back.setColorFilter(getResources().getColor(R.color.white));
                title.setTextColor(getResources().getColor(R.color.white));
                done.setTextColor(getResources().getColor(R.color.white));
                ChangestatusBarColor(getResources().getColor(R.color.actionbar_color_bl));
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.actionbar_color_bl));
                theme = Utils.THEME_BLUE;


            }
        });

        tenderness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pink.setOn(true,true);
                white.setOn(false,true);
                black.setOn(false,true);
                blue.setOn(false,true);
                green.setOn(false,true);
                back.setColorFilter(getResources().getColor(R.color.white));
                title.setTextColor(getResources().getColor(R.color.white));
                done.setTextColor(getResources().getColor(R.color.white));
                ChangestatusBarColor(getResources().getColor(R.color.actionbar_color_pi));
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.actionbar_color_pi));
                theme = Utils.THEME_PINK;

            }
        });

        cyaba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                green.setOn(true,true);
                white.setOn(false,true);
                black.setOn(false,true);
                blue.setOn(false,true);
                pink.setOn(false,true);
                back.setColorFilter(getResources().getColor(R.color.white));
                title.setTextColor(getResources().getColor(R.color.white));
                done.setTextColor(getResources().getColor(R.color.white));
                ChangestatusBarColor(getResources().getColor(R.color.green));
                relativeLayout.setBackgroundColor(getResources().getColor(R.color.green));
                theme = Utils.THEME_GREEN;


            }
        });




//        white.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
//            @Override
//            public void onSwitchStateChange(boolean isOn) {
//                if(isOn){
//                    white.setEnabled(false);
//                    black.setOn(false);
//                    blue.setOn(false);
//                    pink.setOn(false);
//                    green.setOn(false);
//
//                    black.setEnabled(true);
//                    blue.setEnabled(true);
//                    pink.setEnabled(true);
//                    green.setEnabled(true);
//                    theme = Utils.THEME_WHITE;
//                    back.setColorFilter(getResources().getColor(R.color.white));
//                    title.setTextColor(getResources().getColor(R.color.white));
//                    done.setTextColor(getResources().getColor(R.color.white));
//                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.golden));
//                    ChangestatusBarColor(getResources().getColor(R.color.golden));
//
////                    editor.putInt("theme",Utils.THEME_WHITE);
////                    editor.commit();
////                    Utils.changeToTheme(ActivityTheme.this, Utils.THEME_WHITE);
//                }
//            }
//        });
//
//        black.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
//            @Override
//            public void onSwitchStateChange(boolean isOn) {
//                if(isOn){
//                    black.setEnabled(false);
//                    white.setOn(false);
//                    blue.setOn(false);
//                    pink.setOn(false);
//                    green.setOn(false);
//                    white.setEnabled(true);
//                    blue.setEnabled(true);
//                    pink.setEnabled(true);
//                    green.setEnabled(true);
//                    theme = Utils.THEME_BLACK;
//                    back.setColorFilter(getResources().getColor(R.color.white));
//                    title.setTextColor(getResources().getColor(R.color.white));
//                    done.setTextColor(getResources().getColor(R.color.white));
//                    ChangestatusBarColor(getResources().getColor(R.color.black));
//                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.black));
//
//                }
//            }
//        });

//        blue.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
//            @Override
//            public void onSwitchStateChange(boolean isOn) {
//                if(isOn){
//                    back.setColorFilter(getResources().getColor(R.color.white));
//                    title.setTextColor(getResources().getColor(R.color.white));
//                    done.setTextColor(getResources().getColor(R.color.white));
//                    ChangestatusBarColor(getResources().getColor(R.color.actionbar_color_bl));
//                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.actionbar_color_bl));
//                    theme = Utils.THEME_BLUE;
//                    white.setOn(false);
//                    black.setOn(false);
//                    pink.setOn(false);
//                    green.setOn(false);
////                    editor.putInt("theme",Utils.THEME_BLUE);
////                    editor.commit();
////                    Utils.changeToTheme(ActivityTheme.this, Utils.THEME_BLUE);
//                }
//            }
//        });

//        pink.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
//            @Override
//            public void onSwitchStateChange(boolean isOn) {
//                if(isOn){
//                    back.setColorFilter(getResources().getColor(R.color.white));
//                    title.setTextColor(getResources().getColor(R.color.white));
//                    done.setTextColor(getResources().getColor(R.color.white));
//                    ChangestatusBarColor(getResources().getColor(R.color.actionbar_color_pi));
//                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.actionbar_color_pi));
//                    theme = Utils.THEME_PINK;
//                    white.setOn(false);
//                    black.setOn(false);
//                    blue.setOn(false);
//                    green.setOn(false);
////                    editor.putInt("theme",Utils.THEME_PINK);
////                    editor.commit();
////                    Utils.changeToTheme(ActivityTheme.this, Utils.THEME_PINK);
//
//                }
//            }
//        });

//        green.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
//            @Override
//            public void onSwitchStateChange(boolean isOn) {
//                if(isOn){
//                    back.setColorFilter(getResources().getColor(R.color.white));
//                    title.setTextColor(getResources().getColor(R.color.white));
//                    done.setTextColor(getResources().getColor(R.color.white));
//                    ChangestatusBarColor(getResources().getColor(R.color.green));
//                    relativeLayout.setBackgroundColor(getResources().getColor(R.color.green));
//                    theme = Utils.THEME_GREEN;
//                    white.setOn(false);
//                    black.setOn(false);
//                    blue.setOn(false);
//                    pink.setOn(false);
////                    editor.putInt("theme",Utils.THEME_GREEN);
////                    editor.commit();
////                    Utils.changeToTheme(ActivityTheme.this, Utils.THEME_GREEN);
//
//                }
//            }
//        });



    }
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile49, container, false);
        ButterKnife.bind(this,view);

        return view;
    }


    @OnClick(R.id.back)
    void onBackClick(){

        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @OnClick(R.id.done)
    void onDoneClick(){
        SharedPreferences.Editor editor = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
        editor.putInt("theme",theme);
        editor.commit();
        t = theme;
        Utils.changeToTheme(ActivityTheme.this, theme,MainActivity.class);
    }


    public void ChangestatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

}

