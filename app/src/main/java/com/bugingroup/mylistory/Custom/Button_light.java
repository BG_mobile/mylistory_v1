package com.bugingroup.mylistory.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Nurik on 04.03.2016.
 */
public class Button_light extends Button {

        public Button_light(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public Button_light(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public Button_light(Context context) {
            super(context);
            init();
        }

        private void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                    "font/light.ttf");
            setTypeface(tf);
        }

}
