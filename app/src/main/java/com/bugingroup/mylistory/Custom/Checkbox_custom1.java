package com.bugingroup.mylistory.Custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 20.10.2016.
 */

public class Checkbox_custom1 extends CheckBox {



    public Checkbox_custom1(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setButtonDrawable(new StateListDrawable());
    }
    @Override
    public void setChecked(boolean t){
        if(t)
        {
            this.setBackgroundResource(R.drawable.ic_checked);
        }
        else
        {
            this.setBackgroundResource(R.drawable.no_cheked);
        }
        super.setChecked(t);
    }

}