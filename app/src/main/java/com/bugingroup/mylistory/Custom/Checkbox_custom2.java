package com.bugingroup.mylistory.Custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 20.10.2016.
 */

public class Checkbox_custom2 extends CheckBox {



    public Checkbox_custom2(Context context, AttributeSet attrs) {
        super(context, attrs);
        //setButtonDrawable(new StateListDrawable());
    }
    @Override
    public void setChecked(boolean t){
        if(t)
        {
            this.setBackgroundResource(R.drawable.home22_ic_selected);
        }
        else
        {
            this.setBackgroundResource(R.drawable.home22_ic_unselect);
        }
        super.setChecked(t);
    }

}