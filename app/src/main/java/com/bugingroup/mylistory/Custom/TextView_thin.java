package com.bugingroup.mylistory.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Nurik on 04.03.2016.
 */
public class TextView_thin extends TextView {

        public TextView_thin(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public TextView_thin(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public TextView_thin(Context context) {
            super(context);
            init();
        }

        private void init() {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                    "font/thin.ttf");
            setTypeface(tf);
        }

}
