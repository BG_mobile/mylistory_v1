package com.bugingroup.mylistory.FRAGMENT.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 18.01.2017.
 */
public class AllComment extends Activity {

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.listView)
    ListView listView;

    String postId;
    ArrayList<FeedItem> feedItem;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_all_content);
        ButterKnife.bind(this);
        postId = getIntent().getStringExtra("postId");
        GetAllComment();
    }

    @OnClick(R.id.back)
    public void onBackClick(View v){
finish();
    }
    @OnClick(R.id.sendButton)
    public void onClick(View v){
        if(description.getText().length()>0){
            String body = "{\"postId\":\""+postId+"\",\"description\":\""+description.getText().toString()+"\"}";

            MainRestApi.post(AllComment.this,"addPostComment",body,new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if(response.getBoolean("success")){
                            feedItem.clear();
                            GetAllComment();
                            description.setText(null);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    private void GetAllComment() {
        feedItem = new ArrayList<FeedItem>();
        RequestParams params = new RequestParams();
        params.put("postId", postId);
        params.put("start","0");
        params.put("limit","40");
        MainRestApi.get("comments",params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){

                try {
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        JSONArray list = body.getJSONArray("list");
                        for(int i=list.length()-1;i>=0;i--){
                            JSONObject comment = (JSONObject) list.get(i);
                            FeedItem item = new FeedItem();
                            item.setDescription(comment.has("description")?comment.getString("description"):" ");
                            item.setCreateDate(comment.getLong("createDate"));

                            JSONObject user = comment.getJSONObject("user");
                            JSONObject jsonObject =new JSONObject("{\"avatar\":{ }}");
                            JSONObject avatar =user.has("avatar")?user.getJSONObject("avatar"):jsonObject.getJSONObject("avatar");

                            if(!avatar.has("headers")){
                                item.setAvatar_hasHeader(false);
                            }else {
                                item.setAvatar_hasHeader(true);
                                JSONObject ava_headers = avatar.getJSONObject("headers");
                                item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                item.setAva_authorization(ava_headers.getString("Authorization"));
                                item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                item.setAva_Host(ava_headers.getString("Host"));
                            }
                            item.setAvatar_url(avatar.has("url")?avatar.getString("url"):"no");
                            item.setAva_resourceId(avatar.has("resourceId")?avatar.getString("resourceId"):"no");
                            item.setUser_id(user.getString("id"));
                            item.setLogin(user.has("login")?user.getString("login"):"");
                            item.setName(user.has("name")?user.getString("name"):"");
                            feedItem.add(item);
                        }

                        ListViewAdapter adapter = new ListViewAdapter(AllComment.this,feedItem);
                        listView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        ListViewAdapter(Activity activity, ArrayList<FeedItem> feed_list) {

            this.activity = activity;
            this.feed_item = feed_list;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.name)
            TextView name;

            @BindView(R.id.description)
            TextView description;

            @BindView(R.id.createDate)
            TextView createDate;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.allcomment_item_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);

//            viewHolder.name.setText(item.getName());
            viewHolder.name.setText(item.getLogin());
            viewHolder.description.setText(item.getDescription());
            viewHolder.createDate.setText(URL_CONSTANT.getTimeAgo(item.getCreateDate(), AllComment.this));



            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    description.setText(null);
                    description.append(item.getLogin()+", ");
                }
            });
            return convertView;
        }
    }

}
