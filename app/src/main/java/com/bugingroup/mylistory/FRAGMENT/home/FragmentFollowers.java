package com.bugingroup.mylistory.FRAGMENT.home;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentFollowers extends Fragment {

    private ListViewAdapter adapter;

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.title)
    TextView title;

    ArrayList<FeedItem> feedItem;
    String following , subscribe,userId="null";
    String USERID;

    int textColor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_followers, container, false);
        ButterKnife.bind(this,view);
        following = getResources().getString(R.string.unsubscribe);
        subscribe = getResources().getString(R.string.subscribe);
        title.setText(getResources().getString(R.string.followers));
        USERID = prefsMain.getString("userId","");

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
        }
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit","40");
        if(!userId.equals("null")){
            params.put("userId",userId);
        }
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
        textColor = typedValue.data;



        MainRestApi.get("findUserFollowers",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    feedItem = new ArrayList<FeedItem>();
                    if(response.getBoolean("success")){
                        Log.d("FOLLOWINGR",response+"*");
                        JSONObject body = response.getJSONObject("body");
                        if(body.has("list")) {
                            JSONArray list = body.getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject posts = (JSONObject) list.get(i);
                                FeedItem item = new FeedItem();
                                item.setUser_id(posts.has("id")?posts.getString("id"):"0");
                                item.setLogin(posts.has("login")?posts.getString("login"):"");
                                item.setName(posts.has("name")?posts.getString("name"):"");
                                item.setSurname(posts.has("surname")?posts.getString("surname"):"");
                                item.setFollowed(posts.has("followed")?posts.getBoolean("followed"):false);
                                JSONObject jsonObject =new JSONObject("{\"avatar\":{ }}");
                               JSONObject avatar =posts.has("avatar")?posts.getJSONObject("avatar"):jsonObject.getJSONObject("avatar");
                                if (!avatar.has("headers")) {
                                    item.setAvatar_hasHeader(false);
                                } else {
                                    JSONObject headers = avatar.getJSONObject("headers");
                                    item.setAva_x_amz_content_sha256(headers.getString("x-amz-content-sha256"));
                                    item.setAva_authorization(headers.getString("Authorization"));
                                    item.setAva_x_amz_date(headers.getString("x-amz-date"));
                                    item.setAva_Host(headers.getString("Host"));
                                    item.setAvatar_hasHeader(true);
                                }

                                item.setAvatar_url(avatar.has("url")?avatar.getString("url"):"no");
                                item.setResourceId(avatar.has("resourceId")?avatar.getString("resourceId"):"no");
                                feedItem.add(item);

                            }
                            adapter = new ListViewAdapter(getActivity(), feedItem);
                            listView.setAdapter(adapter);
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});

        return view;

    }


    @OnClick(R.id.back)
    void backClik(){
        getActivity().onBackPressed();
    }


    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        ListViewAdapter(Activity activity, ArrayList<FeedItem> feed_list) {
            this.activity = activity;
            this.feed_item = feed_list;
        }


        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return feed_item.size();
        }


        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;
            @BindView(R.id.avatar)
            CircleImageView avatar;
            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }


        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.followers_item_list, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            item = feed_item.get(position);

            int[] attrs = new int[] { R.attr.bg_button_subscribe /* index 0 */};
            TypedArray ta = getActivity().obtainStyledAttributes(attrs);
            final Drawable bg_button_subscribe = ta.getDrawable(0 /* index */);
            ta.recycle();
            int[] attrs2 = new int[] { R.attr.bg_button_subscribe_golden /* index 0 */};
            TypedArray ta2 = getActivity().obtainStyledAttributes(attrs2);
            final Drawable bg_button_subscribe_golden = ta2.getDrawable(0 /* index */);
            ta2.recycle();

            if(item.getUser_id().equals(USERID)){
                viewHolder.subscripe_btn.setVisibility(View.GONE);
            }else {
                if (!item.getFollowed()) {
                    viewHolder.subscripe_btn.setText(subscribe);
                    viewHolder.subscripe_btn.setBackground(bg_button_subscribe_golden);
                    viewHolder.subscripe_btn.setTextColor(getResources().getColor(R.color.white));
                } else {
                    viewHolder.subscripe_btn.setText(following);
                    viewHolder.subscripe_btn.setBackground(bg_button_subscribe);
                    viewHolder.subscripe_btn.setTextColor(textColor);
                }
            }
//            viewHolder.user_name.setText(item.getSurname()+" "+item.getName());
            viewHolder.user_name.setText(item.getLogin());

            Map<String, String> header = new HashMap<String, String>();

            if(!item.getAvatar_hasHeader()){
                header.put("auth",MainActivity.token);
            }else {
                header.put("x-amz-content-sha256",item.getAva_x_amz_content_sha256());
                header.put("Authorization",item.getAva_authorization());
                header.put("x-amz-date",item.getAva_x_amz_date());
                header.put("Host",item.getAva_Host());
            }
            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(header)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();
            if(!item.getAvatar_url().equals("no"))
                ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);


            viewHolder.subscripe_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    RequestParams params = new RequestParams();
                    params.put("favoriteId", item.getUser_id());

                    if(item.getFollowed()){
                        MainRestApi.post("unfollowing",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                Log.d("FOLLOWINGUNR",response+"*");
                                try {
                                    if(response.getBoolean("success")){
                                        viewHolder.subscripe_btn.setText(subscribe);
                                        viewHolder.subscripe_btn.setBackground(bg_button_subscribe_golden);
                                        viewHolder.subscripe_btn.setTextColor(getResources().getColor(R.color.white));
                                        item.setFollowed(false);
                                        notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }else {
                        MainRestApi.post("addFollower",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                try {
                                    if(response.getBoolean("success")){
                                        viewHolder.subscripe_btn.setText(following);
                                        viewHolder.subscripe_btn.setBackground(bg_button_subscribe);
                                       item.setFollowed(true);
                                        notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }




                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });

            return convertView;
        }


    }


}