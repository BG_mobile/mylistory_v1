package com.bugingroup.mylistory.FRAGMENT.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.profile.FragmentEditPosts;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch24;
import com.bugingroup.mylistory.LikedUsers;
import com.bugingroup.mylistory.FRAGMENT.search.Fragment.SearchByTag;
import com.bugingroup.mylistory.ImagePicker.Views.TagImageView;
import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.SingleChat;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.Player;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.tag_library.DIRECTION;
import com.bugingroup.tag_library.TagViewGroup;
import com.bugingroup.tag_library.views.ITagView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.GridHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.dialogPlus;
import static com.bugingroup.mylistory.MainActivity.token;
import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome15 extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ListViewAdapter Ladapter;
    ArrayList<TagGroupModel> labels;
    ArrayList<FeedItem> feedItem;

    Parcelable state;


    @BindView(R.id.toInterestingPeople) ImageButton toInterestingPeople;
    @BindView(R.id.toDirect) ImageButton toDirect;
    @BindView(R.id.listView) ListView listView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    DialogPlusAdapter DPadapter;
    RelativeLayout footer_edit_layout;
    int selected_user=100;
    String resourse_id,postId,USERID;
    ArrayList<FeedItem> feedItemP;
    boolean loadingMore = true;
    int page=0;
    View footer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home15, container, false);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        ButterKnife.bind(this,view);
        USERID = prefsMain.getString("userId","");
        feedItem = new ArrayList<FeedItem>();

        toInterestingPeople.setOnClickListener(OnImageButtonclickListener);
        toDirect.setOnClickListener(OnImageButtonclickListener);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        onRefresh();
                                    }
                                }
        );

        feedItemP = new ArrayList<FeedItem>();
        DPadapter = new DialogPlusAdapter(getActivity(),feedItemP);
        footer = View.inflate(getActivity().getApplicationContext(), R.layout.footer_progress, null);
        listView.addFooterView(footer);
        footer.setVisibility(View.GONE);

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
           if( totalItemCount!=0 && firstVisibleItem+visibleItemCount == totalItemCount )
                {
                    if(!loadingMore)
                    {
                        ListPosts(page);
                        loadingMore=true;
                    }
                }
            }
        });


        findUsers("");
        return view;
    }
    @Override
    public void onRefresh() {
        page =0;
        feedItem.clear();
        ListPosts(page);
    }


    private void ListPosts(int Page) {
        final RequestParams params = new RequestParams();
        params.put("start", Page+"");
        params.put("limit", "20");

        MainRestApi.get("getTapePosts",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.d("RESPONs",response+"/");
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if (!body.has("list")) {
                                Fragment fragment = new FragmentSearch24();
                                ((MainActivity) getActivity()).replaceFragment(fragment);
                            }else {
                                JSONArray list = body.getJSONArray("list");
                                int l = list.length();
                                    Log.d("Llllllllll", l + "))");
                                    if (l == 20) {
                                        loadingMore = false;
                                        footer.setVisibility(View.VISIBLE);
                                    } else {
                                        loadingMore = true;
                                        footer.setVisibility(View.GONE);
                                    }

                                    for (int i = 0; i < list.length(); i++) {
                                        JSONObject posts = (JSONObject) list.get(i);
                                        JSONObject contentsOJ = new JSONObject("{\"contents\":[ ]}");
                                        JSONArray contents = posts.has("contents") ? posts.getJSONArray("contents") : contentsOJ.getJSONArray("contents");
                                        JSONObject jsonObject = new JSONObject("{\"avatar\":{ }}");
                                        JSONObject scren = new JSONObject("{\"screenshot\":{ }}");
                                        JSONObject avatar = posts.has("avatar") ? posts.getJSONObject("avatar") : jsonObject.getJSONObject("avatar");
                                        for (int j = 0; j < contents.length(); j++) {
                                            JSONObject content = (JSONObject) contents.get(j);
                                            JSONObject screenshot = content.has("screenshot") ? content.getJSONObject("screenshot") : scren.getJSONObject("screenshot");
                                            FeedItem item = new FeedItem();
                                            item.setUser_id(posts.getString("userId"));
                                            item.setPost_id(posts.getString("postId"));
                                            item.setLikeCount(posts.has("likeCount") ? posts.getInt("likeCount") : 0);
                                            item.setCommentCount(posts.has("commentCount") ? posts.getInt("commentCount") : 0);
                                            item.setLogin(posts.has("login") ? posts.getString("login") : "");
                                            item.setName(posts.has("userName") ? posts.getString("userName") : "");
                                            item.setLiked(posts.getBoolean("liked"));
                                            item.setCreateDate(posts.getLong("createDate"));
                                            item.setDescription(posts.has("description") ? posts.getString("description") : "");
                                            if (!avatar.has("headers")) {
                                                item.setAvatar_hasHeader(false);
                                            } else {
                                                item.setAvatar_hasHeader(true);
                                                JSONObject ava_headers = avatar.getJSONObject("headers");
                                                item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                                item.setAva_authorization(ava_headers.getString("Authorization"));
                                                item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                                item.setAva_Host(ava_headers.getString("Host"));
                                            }
                                            if (!content.has("headers")) {
                                                item.setHasHeader(false);
                                            } else {
                                                JSONObject header = content.getJSONObject("headers");
                                                item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                                item.setAuthorization(header.getString("Authorization"));
                                                item.setX_amz_date(header.getString("x-amz-date"));
                                                item.setHost(header.getString("Host"));
                                                item.setHasHeader(true);
                                            }
                                            item.setContentType(content.has("contentType") ? content.getString("contentType") : "PICTURE");
                                            item.setUrl(content.has("url") ? content.getString("url") : "no");
                                            item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                            item.setResourceId(content.has("resourceId") ? content.getString("resourceId") : "no");
                                            item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                            item.setScren_resourceId(screenshot.has("resourceId") ? screenshot.getString("resourceId") : "no");
                                            item.setScren_url(screenshot.has("url") ? screenshot.getString("url") : "no");

                                            if (!screenshot.has("headers")) {
                                                item.setScren_hasHeader(false);
                                            } else {
                                                JSONObject header = screenshot.getJSONObject("headers");
                                                item.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                                item.setScren_authorization(header.getString("Authorization"));
                                                item.setScren_x_amz_date(header.getString("x-amz-date"));
                                                item.setScren_Host(header.getString("Host"));
                                                item.setScren_hasHeader(true);
                                            }
                                            labels = new ArrayList<TagGroupModel>();
                                            if (content.has("labels")) {
                                                JSONArray lab_array = content.getJSONArray("labels");
                                                for (int k = 0; k < lab_array.length(); k++) {
                                                    JSONObject labObect = (JSONObject) lab_array.get(k);
                                                    TagGroupModel model = new TagGroupModel();
                                                    TagGroupModel.Tag tag1 = new TagGroupModel.Tag();
                                                    tag1.setDirection(DIRECTION.RIGHT_CENTER.getValue());
                                                    JSONObject user = labObect.getJSONObject("user");
                                                    String n = user.has("name") ? user.getString("name") : "";
                                                    String m = user.has("surname") ? user.getString("surname") : "";
                                                    String login = user.has("login") ? user.getString("login") : "";
                                                    tag1.setName(login);

                                                    model.getTags().add(tag1);
                                                    model.setPercentY(Float.parseFloat(labObect.getString("y")));
                                                    model.setPercentX(Float.parseFloat(labObect.getString("x")));
                                                    model.setUser_id(labObect.getString("userId"));
                                                    labels.add(model);
                                                }
                                            }
                                            item.setLabels(labels);
                                            feedItem.add(item);
                                        }
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorCode"), Toast.LENGTH_SHORT).show();
                    }
                    page=page+20;
                    swipeRefreshLayout.setRefreshing(false);
                    int currentPosition = listView.getFirstVisiblePosition()==0?0:listView.getFirstVisiblePosition()+1;
                    Ladapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(Ladapter);
                    if(state != null) {
                        Log.d("HAHAH", "IIIIIItrying to restore listview state.."+page);
                        listView.onRestoreInstanceState(state);
                    }

                    listView.setSelectionFromTop(currentPosition, 0);
                 //   ListPosts(page);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });


    }

    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.toInterestingPeople:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentHome18());
                    break;
                case R.id.toDirect:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentHome19());

                    break;
            }
        }
    };




    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        boolean[] doubleclick;
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        // Animation Helpers
        private final DecelerateInterpolator DECCELERATE_INTERPOLATOR
                = new DecelerateInterpolator();
        private final AccelerateInterpolator ACCELERATE_INTERPOLATOR
                = new AccelerateInterpolator();

        ListViewAdapter(Activity activity, ArrayList<FeedItem> feed_list) {
            this.activity = activity;
            this.feed_item = feed_list;
            doubleclick = new boolean[feed_list.size()];
            Arrays.fill(doubleclick, Boolean.FALSE);
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.likeCount)
            TextView likeCount;

            @BindView(R.id.user_name)
            TextView userName;

            @BindView(R.id.name)
            TextView name;

            @BindView(R.id.comment)
            TextView comment;

            @BindView(R.id.btn_send)
            ImageButton btn_share;

            @BindView(R.id.btn_comment)
            ImageButton btn_comment;

            @BindView(R.id.description)
            TextView description;

            @BindView(R.id.show_menu)
            ImageButton show_menu;

            @BindView(R.id.to_other_profile)
            RelativeLayout to_other_profile;

            @BindView(R.id.tagImageView)
            TagImageView tagImageView;

            @BindView(R.id.is_mark)
            ImageView is_mark;

            @BindView(R.id.is_mute)
            ImageView is_mute;

            @BindView(R.id.contents_video)
            Player contents_video;

            @BindView(R.id.like_layout)
            RelativeLayout like_layout;

            @BindView(R.id.heart)
            ImageView heartImageView;

            @BindView(R.id.circleBg)
            View circleBackground;

            @BindView(R.id.createDate)
            TextView createDate;

            @BindView(R.id.progressBar)
            ProgressBar progressBar;


            @BindView(R.id.avatar)
            ImageView avatar;
            @BindView(R.id.like)
            ImageView like;

            private HashTagHelper mTextHashTagHelper;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home15_item_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);

            if(item.getLabels().size()>0) {
                viewHolder. tagImageView.setTagList(item.getLabels(),new TagViewGroup.OnTagGroupClickListenerr() {

                    @Override
                    public void onTagClick(TagViewGroup group, ITagView tag, int index) {
                        Bundle bundle = new Bundle();
                        bundle.putString("userId", feedItem.get(position).getLabels().get(viewHolder.tagImageView.getTagGroupIndex(group)).getUser_id());
                        Fragment fragment = new FragmentHome16();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }

                });
                viewHolder.is_mark.setVisibility(View.VISIBLE);
            }else {
                viewHolder.tagImageView.Remove();
                viewHolder.is_mark.setVisibility(View.GONE);
            }


            final HashMap<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);



            viewHolder.show_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    if (!USERID.equals(item.getUser_id())) {
                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.home17_menu_dialog);
                        dialog.show();
                        TextView save = (TextView) dialog.findViewById(R.id.save);
                        if(!item.getContentType().equals("PICTURE")){
                            save.setVisibility(View.GONE);
                        }else {
                            save.setVisibility(View.VISIBLE);
                        }
                        save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                URL_CONSTANT.SaveImage(getActivity(),viewHolder.tagImageView.getImageBitmap());
                                dialog.dismiss();
                            }
                        });
                        TextView block_user = (TextView) dialog.findViewById(R.id.block_user);
                        block_user.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                item = feed_item.get(position);
                                RequestParams params = new RequestParams();
                                params.put("ignoredUser", item.getUser_id());


                                MainRestApi.post("ignoreUser", params, new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                        Log.d("ignoreUser", response + "*");
                                        try {
                                            if (response.getBoolean("success")) {
                                                dialog.dismiss();
                                                notifyDataSetChanged();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                            }
                        });
                        TextView complain = (TextView) dialog.findViewById(R.id.complain);
                        complain.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        TextView send_message = (TextView) dialog.findViewById(R.id.send_message);
                        send_message.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.d("ASAS", item.getUser_id());
                                String body = "{\"name\":\"nurik\", \"users\":[\"" + item.getUser_id() + "\"]}";

                                MainRestApi.post(getActivity(), "addChat", body, new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        try {
                                            Log.d("RESPONS", response + "*");
                                            if (response.getBoolean("success")) {
                                                dialog.dismiss();
//                                            startActivity(new Intent(getActivity(),SingleChat.class)
//                                                    .putExtra("chatId", response.getString("body")));

                                                startActivity(new Intent(getActivity(), SingleChat.class)
                                                        .putExtra("chatId", response.getString("body"))
                                                        .putExtra("userId", item.getUser_id())
                                                        .putExtra("ava_headers", ava_headers)
                                                        .putExtra("avatar_url", item.getAvatar_url())
                                                        .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                                                    .putExtra("name",item.getName()));
                                                        .putExtra("name", item.getLogin()));
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        });

                    }else {

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.profile44_menu_dialog);
                        dialog.show();
                        TextView save = (TextView) dialog.findViewById(R.id.save);
                        if(!item.getContentType().equals("PICTURE")){
                            save.setVisibility(View.GONE);
                        }else {
                            save.setVisibility(View.VISIBLE);
                        }
                        save.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                URL_CONSTANT.SaveImage(getActivity(),viewHolder.tagImageView.getImageBitmap());
                                dialog.dismiss();
                            }
                        });
                        TextView delete = (TextView) dialog.findViewById(R.id.delete);
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                item = feed_item.get(position);

                                RequestParams params = new RequestParams();
                                params.put("postId", item.getPost_id());

                                MainRestApi.post("deletePost",params,new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                        Log.d("CHANGE PASS",response+"**");
                                        try {
                                            if(response.getBoolean("success")){
                                            feed_item.remove(position);
                                                Ladapter.notifyDataSetChanged();
                                            }else {
                                                Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                            }
                                            dialog.dismiss();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        dialog.dismiss();

                                    }});
                            }
                        });
                        TextView edit = (TextView) dialog.findViewById(R.id.edit);
                        edit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                item = feed_item.get(position);

                                Bundle bundle = new Bundle();
                                bundle.putString("postId", item.getPost_id());
                                Fragment fragment = new FragmentEditPosts();
                                fragment.setArguments(bundle);
                                ((MainActivity) getActivity()).replaceFragment(fragment);
                            }
                        });
                    }

                }






            });

            viewHolder.createDate.setText(URL_CONSTANT.getTimeAgo(item.getCreateDate(), getActivity()));


            if (item.getLiked()) {
                viewHolder.like.setImageResource(R.drawable.ic_like);
            } else {
                viewHolder.like.setImageResource(R.drawable.ic_favorite);
            }
            final Resources res = getResources();
            int count = item.getLikeCount();
            String text = count == 0 ? "" : res.getString(R.string.like, count);
            viewHolder.likeCount.setText(text);

            viewHolder.likeCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("postId", item.getPost_id());
                    Fragment fragment = new LikedUsers();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);

                }
            });


            if (item.getCommentCount() == 0)
                viewHolder.comment.setVisibility(View.GONE);
            else {
                viewHolder.comment.setVisibility(View.VISIBLE);
                int comment_count = item.getCommentCount();
                String comment_text = res.getString(R.string.comment, comment_count);
                viewHolder.comment.setText(comment_text);
            }

            viewHolder.userName.setText(item.getLogin());
//            viewHolder.userName.setText(item.getName());
            if(item.getDescription().equals("")) {
                viewHolder.name.setVisibility(View.GONE);
                viewHolder.description.setVisibility(View.GONE);
            }else {
                viewHolder.name.setVisibility(View.VISIBLE);
                viewHolder.description.setVisibility(View.VISIBLE);
//                viewHolder.name.setText(item.getName());
                viewHolder.name.setText(item.getLogin()+" : ");
                viewHolder.description.setText(item.getDescription());
            }
            viewHolder.mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hash_tag), new HashTagHelper.OnHashTagClickListener() {
                @Override
                public void onHashTagClicked(String hashTag) {
                    Bundle bundle = new Bundle();
                    bundle.putString("hash_tag", hashTag);
                    Fragment fragment = new SearchByTag();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });
            viewHolder.mTextHashTagHelper.handle(viewHolder.description);




            Map<String, String> headers_content = new HashMap<String, String>();
            if (item.getHasHeader()) {
                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers_content.put("x-amz-date", item.getX_amz_date());
                headers_content.put("Host", item.getHost());
                headers_content.put("Authorization", item.getAuthorization());
            } else {
                headers_content.put("auth", token);
            }

            if(item.getContentType().equals("PICTURE")){
                viewHolder.contents_video.setVisibility(View.GONE);
                viewHolder.tagImageView.setVisibility(View.VISIBLE);
                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_content)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                viewHolder.tagImageView.setImageUrl(item.getUrl(),header_options,viewHolder.progressBar);

                viewHolder.is_mute.setVisibility(View.GONE);
            }else {

                Map<String, String> headers_screen = new HashMap<String, String>();
                if (item.getScren_hasHeader()) {
                    headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                    headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                    headers_screen.put("Host", item.getScren_Host());
                    headers_screen.put("Authorization", item.getScren_authorization());
                } else {
                    headers_screen.put("auth", token);
                }


                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_screen)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                viewHolder.tagImageView.setImageUrl(item.getScren_url(),header_options,viewHolder.progressBar);

                ViewTreeObserver vto = viewHolder.tagImageView.getViewTreeObserver();
                vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        viewHolder.tagImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                        viewHolder.contents_video.setVideoSize(viewHolder.tagImageView.getMeasuredWidth(),viewHolder.tagImageView.getMeasuredHeight());
                        return true;
                    }
                });

                viewHolder.is_mute.setImageResource(R.drawable.ic_volume_off);
                try {
                    Method setVideoURIMethod= viewHolder.contents_video.getClass().getMethod("setVideoURI", Uri.class, Map.class);
                    setVideoURIMethod.invoke(viewHolder.contents_video, Uri.parse(item.getUrl()),headers_content);
                    viewHolder.contents_video.start();
                } catch (NoSuchMethodException e) {
                    System.out.println(e);
                    e.printStackTrace();
                } catch (InvocationTargetException | IllegalAccessException e) {
                    e.printStackTrace();
                }

                viewHolder.contents_video.setVisibility(View.VISIBLE);
                viewHolder.tagImageView.setVisibility(View.INVISIBLE);
                viewHolder.progressBar.setVisibility(View.GONE);

            }

            viewHolder.to_other_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });

            viewHolder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.addHeader("auth", MainActivity.token);
                    RequestParams params = new RequestParams();
                    params.put("postId", item.getPost_id());

                    client.post(MainActivity.METHOD + "userLike", params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            /// / If the response is JSONObject instead of expected JSONArray

                            try {
                                Log.d("LLLL",response+"**");
                                if (response.getBoolean("success")) {
                                    if (response.getString("body").equals("LIKE")) {
                                        item.setLikeCount(item.getLikeCount() + 1);
                                        viewHolder.like.setImageResource(R.drawable.ic_like);
                                        String text =  res.getString(R.string.like, item.getLikeCount());
                                        viewHolder.likeCount.setText(text);
                                        item.setLiked(true);
                                    } else {
                                        item.setLikeCount(item.getLikeCount() - 1);
                                        viewHolder.like.setImageResource(R.drawable.ic_favorite);
                                        String text =  res.getString(R.string.like, item.getLikeCount());
                                        viewHolder.likeCount.setText(text);
                                        item.setLiked(false);
                                    }
                                    notifyDataSetChanged();
                                } else {
                                    Toast.makeText(activity, "Сервер не доступен", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });

            viewHolder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    startActivity(new Intent(activity, AllComment.class).putExtra("postId", item.getPost_id()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
            });



            viewHolder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item = feed_item.get(position);
                    startActivity(new Intent(activity, AllComment.class).putExtra("postId", item.getPost_id()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
            });

            viewHolder.btn_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    resourse_id = item.getResourceId();
                    postId = item.getPost_id();
                    final GridHolder holder = new GridHolder(4);
                    dialogPlus = DialogPlus.newDialog(getActivity())
                            .setAdapter(DPadapter)
                            .setContentHolder(holder)
                            .setCancelable(true)
                            .setExpanded(false, 500)
                            .setHeader(R.layout.header_fragment26_view)
                            .setFooter(R.layout.footer_fragment26_view)
                            .setOnItemClickListener(new OnItemClickListener() {
                                @Override
                                public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                                }
                            })
                            .create();


                    final EditText search = (EditText) dialogPlus.findViewById(R.id.search);
                    search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                feedItemP.clear();
                                selected_user=100;
                                findUsers(v.getText().toString());
                                URL_CONSTANT.hideSoftKeyboard(getActivity());
                                return true;
                            }
                            return false;
                        }
                    });

                    Button cancelBtn = (Button) dialogPlus.findViewById(R.id.cancelBtn);
                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogPlus.dismiss();
                        }
                    });

                    footer_edit_layout = (RelativeLayout) dialogPlus.findViewById(R.id.edit_layout);
                    final EditText message  = (EditText) dialogPlus.findViewById(R.id.message);
                    ImageButton send_btn  = (ImageButton) dialogPlus.findViewById(R.id.send_btn);
                    send_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                            progressDialog.show();
                            Log.d("SELECTED","*"+resourse_id);

                            Log.d("SELECTED",selected_user+"*");

                            Log.d("ASAS",feedItemP.get(selected_user).getName());
                            String body = "{\"name\":\"nurik\", \"users\":[\""+feedItemP.get(selected_user).getUser_id()+"\"]}";

                            MainRestApi.post(getActivity(),"addChat",body,new JsonHttpResponseHandler(){
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                                    try {
                                        Log.d("RESPONS",response+"*");
                                        if(response.getBoolean("success")){

                                            String bodyP = "{\"chatId\":\"" + response.getString("body") + "\",  \"description\":\"\", \"contentType\":\"PICTURE\", \"resourceId\": \""+resourse_id+"\",\"postId\":\""+postId+"\"}";
                                            MainRestApi.post(getActivity(), "sendMessage", bodyP, new JsonHttpResponseHandler() {
                                                @Override
                                                public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {
                                                    try {
                                                        Log.d("RESPONS", response2 + "*");
                                                        if (response2.getBoolean("success")) {
                                                            if(message.getText().length()>0) {
                                                                String body = "{\"chatId\":\"" + response.getString("body") + "\",  \"description\":\"" + message.getText().toString() + "\", \"contentType\":\"TEXT\", \"resourceId\": null}";
                                                                MainRestApi.post(getActivity(), "sendMessage", body, new JsonHttpResponseHandler() {
                                                                    @Override
                                                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {
                                                                        try {
                                                                            Log.d("RESPONS", response2 + "*");
                                                                            if (response2.getBoolean("success")) {
                                                                                message.setText(null);
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }finally {
                                        progressDialog.dismiss();
                                        dialogPlus.dismiss();
                                    }

                                }
                            });
                        }
                    });

                    dialogPlus.show();
                }
            });

            viewHolder.like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (doubleclick[position]) {
                        item = feed_item.get(position);
                        AsyncHttpClient client = new AsyncHttpClient();
                        client.addHeader("auth", MainActivity.token);
                        RequestParams params = new RequestParams();
                        params.put("postId", item.getPost_id());

                        client.post(MainActivity.METHOD + "userLike", params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                /// / If the response is JSONObject instead of expected JSONArray
                                System.out.println(1212);
                                System.out.println(response);
                                try {
                                    if (response.getBoolean("success")) {
                                        viewHolder.circleBackground.setVisibility(View.VISIBLE);
                                        viewHolder.heartImageView.setVisibility(View.VISIBLE);

                                        viewHolder.circleBackground.setScaleY(0.1f);
                                        viewHolder.circleBackground.setScaleX(0.1f);
                                        viewHolder.circleBackground.setAlpha(1f);
                                        viewHolder.heartImageView.setScaleY(0.1f);
                                        viewHolder.heartImageView.setScaleX(0.1f);

                                        AnimatorSet animatorSet = new AnimatorSet();

                                        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(viewHolder.circleBackground, "scaleY", 0.1f, 1f);
                                        bgScaleYAnim.setDuration(200);
                                        bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(viewHolder.circleBackground, "scaleX", 0.1f, 1f);
                                        bgScaleXAnim.setDuration(200);
                                        bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(viewHolder.circleBackground, "alpha", 1f, 0f);
                                        bgAlphaAnim.setDuration(200);
                                        bgAlphaAnim.setStartDelay(150);
                                        bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

                                        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleY", 0.1f, 1f);
                                        imgScaleUpYAnim.setDuration(300);
                                        imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleX", 0.1f, 1f);
                                        imgScaleUpXAnim.setDuration(300);
                                        imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

                                        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleY", 1f, 0f);
                                        imgScaleDownYAnim.setDuration(300);
                                        imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
                                        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleX", 1f, 0f);
                                        imgScaleDownXAnim.setDuration(300);
                                        imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

                                        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
                                        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

                                        animatorSet.addListener(new AnimatorListenerAdapter() {
                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                viewHolder.circleBackground.setVisibility(View.GONE);
                                                viewHolder.heartImageView.setVisibility(View.GONE);
                                            }
                                        });
                                        animatorSet.start();
                                        if (response.getString("body").equals("LIKE")) {
                                            item.setLikeCount(item.getLikeCount() + 1);
                                            viewHolder.like.setImageResource(R.drawable.ic_like);
                                            String text =  res.getString(R.string.like, item.getLikeCount());
                                            viewHolder.likeCount.setText(text);
                                            item.setLiked(true);
                                        } else {
                                            item.setLikeCount(item.getLikeCount() - 1);
                                            viewHolder.like.setImageResource(R.drawable.ic_favorite);
                                            String text =  res.getString(R.string.like, item.getLikeCount());
                                            viewHolder.likeCount.setText(text);
                                            item.setLiked(false);
                                        }
                                        notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(activity, "Сервер не доступен", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    item = feed_item.get(position);
                    if(item.getContentType().equals("VIDEO") && viewHolder.contents_video.isPlaying()) {
                        viewHolder.is_mute.setVisibility(View.VISIBLE);
                        if (viewHolder.contents_video.ismute()){
                            viewHolder.is_mute.setImageResource(R.drawable.ic_volume_up);
                            viewHolder.contents_video.unmute();
                        }
                        else{
                            viewHolder.is_mute.setImageResource(R.drawable.ic_volume_off);
                            viewHolder.contents_video.mute();
                        }
                    }else {
                        item = feed_item.get(position);
                        if(item.getLabels().size()>0){
                        viewHolder.tagImageView.excuteTagsAnimation();
                        if(viewHolder.tagImageView.isHidenGroup()){
                            viewHolder.is_mark.setVisibility(View.GONE);
                        }else {
                            viewHolder.is_mark.setVisibility(View.VISIBLE);
                        }
                        }
                    }
                    doubleclick[position] = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doubleclick[position] = false;
                        }
                    }, 500);
                }
            });

            return convertView;
        }
    }

    private void findUsers(String search_text) {

        Log.d("FRAGMENTTT", search_text);

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "8");
        params.put("searchText", search_text);



        MainRestApi.get("findUsers",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    if(!USERID.equals(jsonObject.getString("id"))) {
                                        FeedItem item = new FeedItem();
                                        item.setUser_id(jsonObject.getString("id"));
                                        item.setLogin(jsonObject.has("login") ? jsonObject.getString("login") : " ");
                                        item.setName(jsonObject.has("name") ? jsonObject.getString("name") : " ");
                                        item.setSurname(jsonObject.has("surname") ? jsonObject.getString("surname") : " ");
                                        JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                        JSONObject avatar = jsonObject.has("avatar") ? jsonObject.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                        if (!avatar.has("headers")) {
                                            item.setAvatar_hasHeader(false);
                                        } else {
                                            item.setAvatar_hasHeader(true);
                                            JSONObject ava_headers = avatar.getJSONObject("headers");
                                            item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                            item.setAva_authorization(ava_headers.getString("Authorization"));
                                            item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                            item.setAva_Host(ava_headers.getString("Host"));
                                        }
                                        item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                        item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                        feedItemP.add(item);
                                    }
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    DPadapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });

    }



    //  DialogPlusAdapter
    public class DialogPlusAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public DialogPlusAdapter(Activity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name) TextView user_name;

            @BindView(R.id.avatar)
            CircleImageView avatar;
            @BindView(R.id.avatar_check)
            CircleImageView avatar_check;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment26_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);
            Log.d("SELECTED",selected_user+"*"+position);

            if(position==selected_user){
                viewHolder.avatar_check.setVisibility(View.VISIBLE);
            }else {
                viewHolder.avatar_check.setVisibility(View.GONE);
            }
//            viewHolder.user_name.setText(item.getName() + " " + item.getSurname());
            viewHolder.user_name.setText(item.getLogin());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    footer_edit_layout.setVisibility(View.VISIBLE);
                    selected_user = position;
                    Log.d("SELECTED",selected_user+"*");
                    DPadapter.notifyDataSetChanged();



                }
            });



            return convertView;
        }
    }

    @Override
    public void onPause() {
        // Save ListView state @ onPause
        Log.d("HAHAH", "saving listview state @ onPause" + page);
        state = listView.onSaveInstanceState();
        super.onPause();
    }




}
