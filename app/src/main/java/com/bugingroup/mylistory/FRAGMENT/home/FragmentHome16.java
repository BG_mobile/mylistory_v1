package com.bugingroup.mylistory.FRAGMENT.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile46;
import com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile50;
import com.bugingroup.mylistory.FRAGMENT.search.Fragment.SearchByTag;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch25;
import com.bugingroup.mylistory.ImagePicker.Views.TagImageView;
import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.SingleChat;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.Player;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.tag_library.DIRECTION;
import com.bugingroup.tag_library.TagViewGroup;
import com.bugingroup.tag_library.views.ITagView;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.GridHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

import static com.bugingroup.mylistory.MainActivity.dialogPlus;
import static com.bugingroup.mylistory.MainActivity.token;
import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome16 extends Fragment {

    @BindView(R.id.gridView)
    GridViewWithHeaderAndFooter gridView;

    ArrayList<TagGroupModel> labels;

    ImageButton btn_list;
    ImageButton btn_grid;
    ImageButton btn_with_me;
    @BindView(R.id.user_name)
    TextView user_name;
    ArrayList<FeedItem> feedItem;
    private GridViewAdapter adapter;
    private GridViewAdapter2 adapter2;
    ViewGroup myHeader;

    String userId;
    HashMap<String, String> header = new HashMap<String, String>();

    @BindString(R.string.unsubscribe)
    String unsubscribe_txt;

    @BindString(R.string.subscribe)
    String subscribe_txt;

    @BindString(R.string.edit)
    String edit_prof;

    Boolean followed = false;
    Button subscripe_btn;
    ImageView avatar_img;
    TextView name, followingCount, postCount, followersCount, info;
    String ID, NAME,LOGIN, AVA_URL = "no", RESOURCE_ID;
    LinearLayout followers, following;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    DialogPlusAdapter DPadapter;
    RelativeLayout footer_edit_layout;
    int selected_user=100;
    String resourse_id,postId;
    ArrayList<FeedItem> feedItemP;
 int textColor ;
    Drawable bg_button_subscribe,bg_button_subscribe_golden;
    boolean is_connect=false;
    String USERID;
    SharedPreferences prefsStart;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home16, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences prefs = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, getActivity().MODE_PRIVATE);
        int user_num = prefs.getInt("user_number", 1);

        switch (user_num) {
            case 1:
                prefsStart = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, getActivity().MODE_PRIVATE);
                Log.d("MAPshare1", "1111");
                System.out.println("1111" + user_num);
                break;
            case 2:
                prefsStart = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, getActivity().MODE_PRIVATE);
                Log.d("MAPshare2", "2222");
                System.out.println("2222" + user_num);
                break;
        }
        Bundle bundle = this.getArguments();
        USERID = prefsStart.getString("userId","");

        if (bundle != null) {
            userId = bundle.getString("userId");
        }
        Log.d("USER",userId);
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
        textColor = typedValue.data;



        int[] attrs = new int[] { R.attr.bg_button_subscribe /* index 0 */};
        TypedArray ta = getActivity().obtainStyledAttributes(attrs);
        bg_button_subscribe = ta.getDrawable(0 /* index */);
        ta.recycle();
        int[] attrs2 = new int[] { R.attr.bg_button_subscribe_golden /* index 0 */};
        TypedArray ta2 = getActivity().obtainStyledAttributes(attrs2);
         bg_button_subscribe_golden = ta2.getDrawable(0 /* index */);
        ta2.recycle();




        LayoutInflater myinflater = LayoutInflater.from(getActivity());

        this.gridView = (GridViewWithHeaderAndFooter) view.findViewById(R.id.gridView);
        gridView.setNumColumns(3);
        gridView.setHorizontalSpacing(4);
        gridView.setVerticalSpacing(4);

        myHeader = (ViewGroup) myinflater.inflate(R.layout.header_home16_view, null);

        feedItem = new ArrayList<FeedItem>();
        adapter = new GridViewAdapter(getActivity(), feedItem);
        adapter2 = new GridViewAdapter2(getActivity(), feedItem);
        name = (TextView) myHeader.findViewById(R.id.name);
        info = (TextView) myHeader.findViewById(R.id.info);
        followingCount = (TextView) myHeader.findViewById(R.id.followingCount);
        postCount = (TextView) myHeader.findViewById(R.id.postCount);
        followersCount = (TextView) myHeader.findViewById(R.id.followersCount);
        avatar_img = (ImageView) myHeader.findViewById(R.id.avatar);
        btn_list = (ImageButton) myHeader.findViewById(R.id.btn_list);
        btn_grid = (ImageButton) myHeader.findViewById(R.id.btn_grid);
        btn_with_me = (ImageButton) myHeader.findViewById(R.id.btn_with_me);
        followers = (LinearLayout) myHeader.findViewById(R.id.followers);
        following = (LinearLayout) myHeader.findViewById(R.id.following);
        subscripe_btn = (Button) myHeader.findViewById(R.id.subscripe_btn);

        btn_list.setOnClickListener(topMenuclickListener);
        btn_grid.setOnClickListener(topMenuclickListener);
        btn_with_me.setOnClickListener(topMenuclickListener);
        followers.setOnClickListener(topMenuclickListener);
        following.setOnClickListener(topMenuclickListener);
        subscripe_btn.setOnClickListener(topMenuclickListener);
        gridView.addHeaderView(myHeader);
        gridView.setAdapter(adapter2);
        btn_list.setBackground(getResources().getDrawable(R.color.transparent));
        btn_grid.setBackground(getResources().getDrawable(R.color.whiter));
        btn_with_me.setBackground(getResources().getDrawable(R.color.transparent));

        userProfile();

        feedItemP = new ArrayList<FeedItem>();
        DPadapter = new DialogPlusAdapter(getActivity(),feedItemP);
        findUsers("");

        return view;

    }

    @OnClick(R.id.menu)
    void menuClick() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.home17_menu_dialog);
        dialog.show();
        TextView block_user = (TextView) dialog.findViewById(R.id.block_user);
        block_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams params = new RequestParams();
                params.put("ignoredUser", userId);


                MainRestApi.post("ignoreUser", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                        Log.d("ignoreUser", response + "*");
                        try {
                            if (response.getBoolean("success")) {
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });

        TextView send_message = (TextView) dialog.findViewById(R.id.send_message);
        send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ASAS", userId);
                String body = "{\"name\":\"nurik\", \"users\":[\"" + userId + "\"]}";

                MainRestApi.post(getActivity(), "addChat", body, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            Log.d("RESPONS", response + "*");
                            if (response.getBoolean("success")) {
                                dialog.dismiss();
//                                startActivity(new Intent(getActivity(), SingleChat.class)
//                                        .putExtra("chatId", response.getString("body")));


                                startActivity(new Intent(getActivity(),SingleChat.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        .putExtra("chatId", response.getString("body"))
                                        .putExtra("userId",userId)
                                        .putExtra("ava_headers",  header)
                                        .putExtra("avatar_url",AVA_URL)
                                        .putExtra("name",LOGIN));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @OnClick(R.id.back)
    void backClick() {
        getActivity().onBackPressed();
    }

    private void userProfile() {
        RequestParams params = new RequestParams();
        params.put("userId", userId);

        MainRestApi.get("profile", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response.getBoolean("success")) {
                        JSONObject body = response.getJSONObject("body");
                        String surname = body.has("surname") ? body.getString("surname") : "";
                        String login = body.has("login") ? body.getString("login") : "";
                        String Name = body.has("name") ? body.getString("name") : "";
                        name.setText(surname + " " + Name);
                        user_name.setText(login);
                        info.setText(body.has("info") ? body.getString("info") : "");
                        postCount.setText(body.has("postCount") ? body.getString("postCount") : "0");
                        followersCount.setText(body.has("followersCount") ? body.getString("followersCount") : "0");
                        followingCount.setText(body.has("followingCount") ? body.getString("followingCount") : "0");
                        final JSONObject avatar = body.getJSONObject("avatar");
                        ID = body.getString("id");
                        NAME = Name;
                        LOGIN = login;
                        AVA_URL = avatar.has("url") ? avatar.getString("url") : "no";
                        RESOURCE_ID = avatar.has("resourceId") ? avatar.getString("resourceId") : "";


                        followed = body.has("followed") ? body.getBoolean("followed") : false;
                        if(USERID.equals(userId)) {
                            subscripe_btn.setText(edit_prof);
                        }else {
                            if (!followed) {
                                subscripe_btn.setText(subscribe_txt);
                                subscripe_btn.setBackground(bg_button_subscribe_golden);
                                subscripe_btn.setTextColor(getResources().getColor(R.color.white));
                            } else {
                                subscripe_btn.setText(unsubscribe_txt);
                                subscripe_btn.setBackground(bg_button_subscribe);
                                subscripe_btn.setTextColor(textColor);
                            }
                        }

                        if (!avatar.has("headers")) {
                            header.put("auth", MainActivity.token);
                        } else {
                            JSONObject ava_headers = avatar.getJSONObject("headers");
                            header.put("x-amz-content-sha256", ava_headers.getString("x-amz-content-sha256"));
                            header.put("Authorization", ava_headers.getString("Authorization"));
                            header.put("x-amz-date", ava_headers.getString("x-amz-date"));
                            header.put("Host", ava_headers.getString("Host"));
                        }

                        DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                                .extraForDownloader(header)
                                .showImageOnLoading(R.drawable.loading)
                                .cacheInMemory(true)
                                .cacheOnDisk(true)
                                .considerExifParams(true)
                                .build();
                        if (!AVA_URL.equals("no"))
                            ImageLoader.getInstance().displayImage(AVA_URL, avatar_img, ava_options, animateFirstListener);

                        userContents(ID);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void userContents(String ID) {
        RequestParams params = new RequestParams();
        params.put("userId", ID);
        params.put("start", "0");
        params.put("limit", "40");

        MainRestApi.get("userContents", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    feedItem = new ArrayList<FeedItem>();
                    if (response.getBoolean("success")) {
                        JSONObject body = response.getJSONObject("body");
                        if (body.has("list")) {
                            JSONArray list = body.getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject posts = (JSONObject) list.get(i);
                                JSONObject scren =new JSONObject("{\"screen\":{ }}");
                                JSONObject screenshot = posts.has("screen")?posts.getJSONObject("screen"):scren.getJSONObject("screen");

                                FeedItem item = new FeedItem();
                                if (!posts.has("description")) {
                                    item.setDescription("");
                                } else {
                                    item.setDescription(posts.getString("description"));
                                }
                                item.setPost_id(posts.getString("postId"));
                                if (!posts.has("headers")) {
                                    item.setHasHeader(false);
                                } else {
                                    JSONObject headers = posts.getJSONObject("headers");
                                    item.setX_amz_content_sha256(headers.getString("x-amz-content-sha256"));
                                    item.setAuthorization(headers.getString("Authorization"));
                                    item.setX_amz_date(headers.getString("x-amz-date"));
                                    item.setHost(headers.getString("Host"));
                                    item.setHasHeader(true);
                                }
                                item.setUrl(posts.getString("url"));
                                item.setContentType(posts.has("contentType") ? posts.getString("contentType") : "PICTURE");
                                item.setResourceId(posts.getString("resourceId"));
                                item.setCommentCount(posts.has("commentCount") ? posts.getInt("commentCount") : 0);
                                item.setLikeCount( posts.has("likeCount") ? posts.getInt("likeCount") : 0);
                                item.setLiked(posts.has("liked") && posts.getBoolean("liked"));
                                item.setScren_url(screenshot.has("url")?screenshot.getString("url"):"no");
                                item.setScren_resourceId(screenshot.has("resourceId")?screenshot.getString("resourceId"):"no");
                                if(!screenshot.has("headers")){
                                    item.setScren_hasHeader(false);
                                }else {
                                    JSONObject header = screenshot.getJSONObject("headers");
                                    item.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                    item.setScren_authorization(header.getString("Authorization"));
                                    item.setScren_x_amz_date(header.getString("x-amz-date"));
                                    item.setScren_Host(header.getString("Host"));
                                    item.setScren_hasHeader(true);
                                }
                                labels = new ArrayList<TagGroupModel>();
                                if(posts.has("labels")) {
                                    JSONArray lab_array = posts.getJSONArray("labels");
                                    for (int k = 0; k < lab_array.length();k++){
                                        JSONObject labObect = (JSONObject) lab_array.get(k);
                                        TagGroupModel model = new TagGroupModel();
                                        TagGroupModel.Tag tag1 = new TagGroupModel.Tag();
                                        tag1.setDirection(DIRECTION.RIGHT_CENTER.getValue());
                                        JSONObject user = labObect.getJSONObject("user");
                                        String n = user.has("name")?user.getString("name"):"";
                                        String m = user.has("surname")?user.getString("surname"):"";
                                        String login = user.has("login")?user.getString("login"):"";
                                        tag1.setName(login);

                                        model.getTags().add(tag1);
                                        model.setPercentY(Float.parseFloat(labObect.getString("y")));
                                        model.setPercentX(Float.parseFloat(labObect.getString("x")));
                                        model.setUser_id(labObect.getString("userId"));
                                        labels.add(model);
                                    }
                                }
                                item.setLabels(labels);
                                feedItem.add(item);

                            }
                            adapter = new GridViewAdapter(getActivity(), feedItem);
                            adapter2 = new GridViewAdapter2(getActivity(), feedItem);
                            gridView.setAdapter(adapter2);
                            is_connect = true;
                        }
                    } else {

                        gridView.setNumColumns(1);
                        gridView.setVerticalSpacing(0);
                        is_connect = false;
                        CloseProfileAdapter adapters = new CloseProfileAdapter(getActivity(), response.getString("errorDesc"));
                        gridView.setAdapter(adapters);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    View.OnClickListener topMenuclickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_list:
                    if(is_connect) {
                        gridView.setAdapter(adapter);
                        gridView.setNumColumns(1);
                        gridView.setVerticalSpacing(0);
                        btn_list.setBackground(getResources().getDrawable(R.color.whiter));
                        btn_grid.setBackground(getResources().getDrawable(R.color.transparent));
                        btn_with_me.setBackground(getResources().getDrawable(R.color.transparent));
                    }
                    break;
                case R.id.btn_grid:
                    if(is_connect) {
                        gridView.setAdapter(adapter2);
                        gridView.setNumColumns(3);
                        gridView.setHorizontalSpacing(4);
                        gridView.setVerticalSpacing(4);
                        btn_list.setBackground(getResources().getDrawable(R.color.transparent));
                        btn_with_me.setBackground(getResources().getDrawable(R.color.transparent));
                        btn_grid.setBackground(getResources().getDrawable(R.color.whiter));
                    }
                    break;
                case R.id.btn_with_me:
                    if(is_connect) {
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("userId", ID);
                        bundle1.putString("name", LOGIN);
                        Fragment fragment1 = new FragmentProfile46();
                        fragment1.setArguments(bundle1);
                        ((MainActivity) getActivity()).replaceFragment(fragment1);
                    }
                    break;
                case R.id.following:
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", ID);
                    Fragment fragment = new FragmentFollowing();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                    break;
                case R.id.followers:
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("userId", ID);
                    Fragment fragment2 = new FragmentFollowers();
                    fragment2.setArguments(bundle2);
                    ((MainActivity) getActivity()).replaceFragment(fragment2);
                    break;
                case R.id.subscripe_btn:
                    if(USERID.equals(userId)) {
                        ((MainActivity) getActivity()).replaceFragment(new FragmentProfile50());
                    }else {
                        final Dialog dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.profile62_clear_search_dialog);
                        Button cancle = (Button) dialog.findViewById(R.id.cancle);
                        cancle.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        Button ok = (Button) dialog.findViewById(R.id.ok);
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                Following();
                            }
                        });
                        dialog.show();
                    }
                    break;

            }
        }
    };


    public void Following(){

        RequestParams params = new RequestParams();
        params.put("favoriteId", ID);

        if (followed) {
            MainRestApi.post("unfollowing", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    Log.d("FOLLOWINGUNR", response + "*");
                    try {
                        if (response.getBoolean("success")) {
                            subscripe_btn.setText(subscribe_txt);
                            subscripe_btn.setBackground(bg_button_subscribe_golden);
                            subscripe_btn.setTextColor(getResources().getColor(R.color.white));
                            followed=false;
                        }else {
                            Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            MainRestApi.post("addFollower", params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            subscripe_btn.setText(unsubscribe_txt);
                            subscripe_btn.setBackground(bg_button_subscribe);
                            subscripe_btn.setTextColor(textColor);
                            followed=true;
                        }else {
                            Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }


    //  GridViewAdapter
    public class GridViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        boolean [] doubleclick ;

        // Animation Helpers
        private  final DecelerateInterpolator DECCELERATE_INTERPOLATOR
                = new DecelerateInterpolator();
        private  final AccelerateInterpolator ACCELERATE_INTERPOLATOR
                = new AccelerateInterpolator();
        public GridViewAdapter(Activity activity, ArrayList<FeedItem> feed_item) {
            this.feed_item = feed_item;
            this.activity = activity;
            doubleclick = new boolean[feed_item.size()];
            Arrays.fill(doubleclick, Boolean.FALSE);
        }

        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {

            @BindView(R.id.likeCount)
            TextView likeCount;

            @BindView(R.id.user_name)
            TextView userName;

            @BindView(R.id.name)
            TextView name;

            @BindView(R.id.comment)
            TextView comment;

            @BindView(R.id.btn_send)
            ImageButton btn_share;

            @BindView(R.id.btn_comment)
            ImageButton btn_comment;

            @BindView(R.id.description)
            TextView description;

            @BindView(R.id.show_menu)
            ImageButton show_menu;

            @BindView(R.id.to_other_profile)
            RelativeLayout to_other_profile;

            @BindView(R.id.tagImageView)
            TagImageView tagImageView;

            @BindView(R.id.is_mark)
            ImageView is_mark;

            @BindView(R.id.is_mute)
            ImageView is_mute;

            @BindView(R.id.contents_video)
            Player contents_video;

            @BindView(R.id.like_layout)
            RelativeLayout like_layout;

            @BindView(R.id.heart)
            ImageView heartImageView;

            @BindView(R.id.circleBg)
            View circleBackground;

            @BindView(R.id.progressBar)
            ProgressBar progressBar;


            private HashTagHelper mTextHashTagHelper;

            @BindView(R.id.avatar)
            ImageView avatar;
            @BindView(R.id.like)
            ImageView like;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            final ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.profile43_item_list, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);

            if(item.getLabels().size()>0) {
            viewHolder. tagImageView.setTagList(item.getLabels(),new TagViewGroup.OnTagGroupClickListenerr() {

                @Override
                public void onTagClick(TagViewGroup group, ITagView tag, int index) {
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", feedItem.get(position).getLabels().get(viewHolder.tagImageView.getTagGroupIndex(group)).getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }

            });
            viewHolder.is_mark.setVisibility(View.VISIBLE);
        }else {
            viewHolder.tagImageView.Remove();
            viewHolder.is_mark.setVisibility(View.GONE);
        }

            viewHolder.show_menu.setVisibility(View.GONE);

            if (item.getLiked()) {
                viewHolder.like.setImageResource(R.drawable.ic_like);
            } else {
                viewHolder.like.setImageResource(R.drawable.ic_favorite);
            }

            final Resources res = getResources();
            int count = item.getLikeCount();
            String text = count==0 ?"": res.getString(R.string.like, count);
            viewHolder.likeCount.setText(text);


            if(item.getCommentCount()==0)
                viewHolder.comment.setVisibility(View.GONE);
            else {
                viewHolder.comment.setVisibility(View.VISIBLE);
                int comment_count = item.getCommentCount();
                String comment_text = res.getString(R.string.comment, comment_count);
                viewHolder.comment.setText(comment_text);
            }

            viewHolder.userName.setText(LOGIN);
            viewHolder.name.setText(LOGIN+" : ");
            viewHolder.description.setText(item.getDescription());
            viewHolder.mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hash_tag), new HashTagHelper.OnHashTagClickListener() {
                @Override
                public void onHashTagClicked(String hashTag) {
                    Bundle bundle = new Bundle();
                    bundle.putString("hash_tag", hashTag);
                    Fragment fragment = new SearchByTag();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });
            viewHolder.mTextHashTagHelper.handle(viewHolder.description);


            Map<String, String> headers_content = new HashMap<String, String>();
            if(item.getHasHeader()){
                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers_content.put("x-amz-date", item.getX_amz_date());
                headers_content.put("Host", item.getHost());
                headers_content.put("Authorization", item.getAuthorization());
            }else {
                headers_content.put("auth", MainActivity.token);
            }

            if(item.getContentType().equals("PICTURE")){
                viewHolder.contents_video.setVisibility(View.GONE);
                viewHolder.tagImageView.setVisibility(View.VISIBLE);
                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_content)
                        .cacheInMemory(true)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();


                viewHolder.tagImageView.setImageUrl(item.getUrl(),header_options,viewHolder.progressBar);
              viewHolder.is_mute.setVisibility(View.GONE);
            }else {
                Map<String, String> headers_screen = new HashMap<String, String>();
                if (item.getScren_hasHeader()) {
                    headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                    headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                    headers_screen.put("Host", item.getScren_Host());
                    headers_screen.put("Authorization", item.getScren_authorization());
                } else {
                    headers_screen.put("auth", token);
                }


                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_screen)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                viewHolder.tagImageView.setImageUrl(item.getScren_url(),header_options,viewHolder.progressBar);

                ViewTreeObserver vto = viewHolder.tagImageView.getViewTreeObserver();
                vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    public boolean onPreDraw() {
                        viewHolder.tagImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                        viewHolder.contents_video.setVideoSize(viewHolder.tagImageView.getMeasuredWidth(),viewHolder.tagImageView.getMeasuredHeight());
                        return true;
                    }
                });
                viewHolder.is_mute.setImageResource(R.drawable.ic_volume_off);

                try {

                    Method setVideoURIMethod= viewHolder.contents_video.getClass().getMethod("setVideoURI", Uri.class, Map.class);
                    setVideoURIMethod.invoke(viewHolder.contents_video, Uri.parse(item.getUrl()),headers_content);
                    viewHolder.contents_video.start();


                } catch (NoSuchMethodException e) {
                    System.out.println(e);
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                viewHolder.contents_video.setVisibility(View.VISIBLE);
                viewHolder.tagImageView.setVisibility(View.INVISIBLE);
                viewHolder.progressBar.setVisibility(View.GONE);

            }
            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(header)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();
            if(!AVA_URL.equals("no"))
                ImageLoader.getInstance().displayImage(AVA_URL,viewHolder.avatar, ava_options, animateFirstListener);



            viewHolder.like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);

                    RequestParams params = new RequestParams();
                    params.put("postId", item.getPost_id());

                    MainRestApi.post("userLike",params,new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            /// / If the response is JSONObject instead of expected JSONArray
                            System.out.println(1212);
                            System.out.println(response);
                            try {
                                if(response.getBoolean("success")){
                                    if (response.getString("body").equals("LIKE")) {
                                        item.setLikeCount(item.getLikeCount() + 1);
                                        viewHolder.like.setImageResource(R.drawable.ic_like);
                                        String text =  res.getString(R.string.like, item.getLikeCount());
                                        viewHolder.likeCount.setText(text);
                                        item.setLiked(true);
                                    } else {
                                        item.setLikeCount(item.getLikeCount() - 1);
                                        viewHolder.like.setImageResource(R.drawable.ic_favorite);
                                        String text =  res.getString(R.string.like, item.getLikeCount());
                                        viewHolder.likeCount.setText(text);
                                        item.setLiked(false);
                                    }
                                    notifyDataSetChanged();
                                }else {
                                    Toast.makeText(activity, "Сервер не доступен", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                            // Pull out the first event on the public timeline
                            System.out.println(1313);
                            System.out.println(timeline);

                        }
                    });
                }
            });

            viewHolder.comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    startActivity(new Intent(activity,AllComment.class).putExtra("postId",item.getPost_id()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
            });

            viewHolder.btn_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    startActivity(new Intent(activity,AllComment.class).putExtra("postId",item.getPost_id()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
            });
            viewHolder.btn_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    resourse_id = item.getResourceId();
                    postId = item.getPost_id();
                    final GridHolder holder = new GridHolder(4);
                    dialogPlus = DialogPlus.newDialog(getActivity())
                            .setAdapter(DPadapter)
                            .setContentHolder(holder)
                            .setCancelable(true)
                            .setExpanded(false, 500)
                            .setHeader(R.layout.header_fragment26_view)
                            .setFooter(R.layout.footer_fragment26_view)
                            .setOnItemClickListener(new OnItemClickListener() {
                                @Override
                                public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                                }
                            })
                            .create();

                    final EditText search = (EditText) dialogPlus.findViewById(R.id.search);
                    search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                feedItemP.clear();
                                selected_user=100;
                                findUsers(v.getText().toString());
                                URL_CONSTANT.hideSoftKeyboard(getActivity());
                                return true;
                            }
                            return false;
                        }
                    });

                    Button cancelBtn = (Button) dialogPlus.findViewById(R.id.cancelBtn);
                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogPlus.dismiss();
                        }
                    });

                    footer_edit_layout = (RelativeLayout) dialogPlus.findViewById(R.id.edit_layout);
                    final EditText message  = (EditText) dialogPlus.findViewById(R.id.message);
                    ImageButton send_btn  = (ImageButton) dialogPlus.findViewById(R.id.send_btn);
                    send_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                            progressDialog.show();
                            Log.d("SELECTED","*"+resourse_id);

                            Log.d("SELECTED",selected_user+"*");

                            Log.d("ASAS",feedItemP.get(selected_user).getName());
                            String body = "{\"name\":\"nurik\", \"users\":[\""+feedItemP.get(selected_user).getUser_id()+"\"]}";

                            MainRestApi.post(getActivity(),"addChat",body,new JsonHttpResponseHandler(){
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                                    try {
                                        Log.d("RESPONS",response+"*");
                                        if(response.getBoolean("success")){

                                            String bodyP = "{\"chatId\":\"" + response.getString("body") + "\",  \"description\":\"\", \"contentType\":\"PICTURE\", \"resourceId\": \""+resourse_id+"\",\"postId\":\""+postId+"\"}";
                                            MainRestApi.post(getActivity(), "sendMessage", bodyP, new JsonHttpResponseHandler() {
                                                @Override
                                                public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {
                                                    try {
                                                        Log.d("RESPONS", response2 + "*");
                                                        if (response2.getBoolean("success")) {
                                                            if(message.getText().length()>0) {
                                                                String body = "{\"chatId\":\"" + response.getString("body") + "\",  \"description\":\"" + message.getText().toString() + "\", \"contentType\":\"TEXT\", \"resourceId\": null}";
                                                                MainRestApi.post(getActivity(), "sendMessage", body, new JsonHttpResponseHandler() {
                                                                    @Override
                                                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {
                                                                        try {
                                                                            Log.d("RESPONS", response2 + "*");
                                                                            if (response2.getBoolean("success")) {
                                                                                message.setText(null);
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }finally {
                                        progressDialog.dismiss();
                                        dialogPlus.dismiss();
                                    }

                                }
                            });
                        }
                    });

                    dialogPlus.show();
                }
            });

            viewHolder.like_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(doubleclick[position]){
                        item = feed_item.get(position);

                        RequestParams params = new RequestParams();
                        params.put("postId", item.getPost_id());

                        MainRestApi.post("userLike",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                /// / If the response is JSONObject instead of expected JSONArray
                                System.out.println(1212);
                                System.out.println(response+"**");
                                try {
                                    if(response.getBoolean("success")){
                                        viewHolder.circleBackground.setVisibility(View.VISIBLE);
                                        viewHolder.heartImageView.setVisibility(View.VISIBLE);

                                        viewHolder.circleBackground.setScaleY(0.1f);
                                        viewHolder.circleBackground.setScaleX(0.1f);
                                        viewHolder.circleBackground.setAlpha(1f);
                                        viewHolder.heartImageView.setScaleY(0.1f);
                                        viewHolder.heartImageView.setScaleX(0.1f);

                                        AnimatorSet animatorSet = new AnimatorSet();

                                        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(viewHolder.circleBackground, "scaleY", 0.1f, 1f);
                                        bgScaleYAnim.setDuration(200);
                                        bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(viewHolder.circleBackground, "scaleX", 0.1f, 1f);
                                        bgScaleXAnim.setDuration(200);
                                        bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(viewHolder.circleBackground, "alpha", 1f, 0f);
                                        bgAlphaAnim.setDuration(200);
                                        bgAlphaAnim.setStartDelay(150);
                                        bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

                                        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleY", 0.1f, 1f);
                                        imgScaleUpYAnim.setDuration(300);
                                        imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleX", 0.1f, 1f);
                                        imgScaleUpXAnim.setDuration(300);
                                        imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

                                        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleY", 1f, 0f);
                                        imgScaleDownYAnim.setDuration(300);
                                        imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
                                        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(viewHolder.heartImageView, "scaleX", 1f, 0f);
                                        imgScaleDownXAnim.setDuration(300);
                                        imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

                                        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
                                        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

                                        animatorSet.addListener(new AnimatorListenerAdapter() {
                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                viewHolder.circleBackground.setVisibility(View.GONE);
                                                viewHolder.heartImageView.setVisibility(View.GONE);
                                            }
                                        });
                                        animatorSet.start();
                                        if(response.getString("body").equals("LIKE")) {
                                            item.setLikeCount(item.getLikeCount() + 1);
                                            viewHolder.like.setImageResource(R.drawable.ic_like);
                                            String text = res.getString(R.string.like, item.getLikeCount());
                                            viewHolder.likeCount.setText(text);
                                            item.setLiked(true);
                                        } else {
                                            item.setLikeCount(item.getLikeCount() - 1);
                                            viewHolder.like.setImageResource(R.drawable.ic_favorite);
                                            String text =  res.getString(R.string.like, item.getLikeCount());
                                            viewHolder.likeCount.setText(text);
                                            item.setLiked(false);
                                        }
                                        notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(activity, "Сервер не доступен", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                                // Pull out the first event on the public timeline
                                System.out.println(1313);
                                System.out.println(timeline);

                            }
                        });
                    }
                    item = feed_item.get(position);
                    if(item.getContentType().equals("VIDEO") && viewHolder.contents_video.isPlaying()) {
                        viewHolder.is_mute.setVisibility(View.VISIBLE);
                        if (viewHolder.contents_video.ismute()){
                            viewHolder.is_mute.setImageResource(R.drawable.ic_volume_up);
                            viewHolder.contents_video.unmute();
                        }
                        else{
                            viewHolder.is_mute.setImageResource(R.drawable.ic_volume_off);
                            viewHolder.contents_video.mute();
                        }
                    }else {
                        item = feed_item.get(position);
                        if(item.getLabels().size()>0){
                            viewHolder.tagImageView.excuteTagsAnimation();
                            if(viewHolder.tagImageView.isHidenGroup()){
                                viewHolder.is_mark.setVisibility(View.GONE);
                            }else {
                                viewHolder.is_mark.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                    doubleclick[position] = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doubleclick[position] = false;
                        }
                    }, 500);
                }
            });


            return convertView;
        }


    }




    //  GridViewAdapter
    public class GridViewAdapter2 extends BaseAdapter {

        ArrayList<FeedItem>  feed_item;
        FeedItem item;
        Activity activity;
        int count;
        int size;
        int n=100,k=100;

        public GridViewAdapter2(Activity activity, ArrayList<FeedItem> feed_item) {
            this.feed_item = feed_item;
            this.activity = activity;
            size= feed_item.size();
            count =size;
            n = size;
            k = size;
            if(size%3==2) {
                n = size;
                k = size;
                count = size + 1;
            } if(size%3==1) {
                n = size;
                k = size+1;
                count = size + 2;
            }
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {

            @BindView(R.id.imageView)
            ImageView imageView;

            @BindView(R.id.ic_video)
            ImageView ic_video;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home16_item_grid, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(position==n || position==k) {
                viewHolder.ic_video.setVisibility(View.GONE);
                viewHolder.imageView.setVisibility(View.GONE);
            }else {
                viewHolder.imageView.setVisibility(View.VISIBLE);
                item = feed_item.get(position);
                if (item.getContentType().equals("PICTURE")) {
                    viewHolder.ic_video.setVisibility(View.GONE);
                    Map<String, String> headers_content = new HashMap<String, String>();
                    if (item.getHasHeader()) {
                        headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                        headers_content.put("x-amz-date", item.getX_amz_date());
                        headers_content.put("Host", item.getHost());
                        headers_content.put("Authorization", item.getAuthorization());
                    } else {
                        headers_content.put("auth", MainActivity.token);
                    }
                    DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                            .extraForDownloader(headers_content)
                            .showImageOnLoading(R.drawable.loading)
                            .cacheInMemory(true)
                            .cacheOnDisk(true)
                            .considerExifParams(true)
                            .build();

                    ImageLoader.getInstance().displayImage(item.getUrl(), viewHolder.imageView, header_options, animateFirstListener);
                } else {
                    viewHolder.ic_video.setVisibility(View.VISIBLE);
                    Map<String, String> headers_screen = new HashMap<String, String>();
                    if (item.getScren_hasHeader()) {
                        headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                        headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                        headers_screen.put("Host", item.getScren_Host());
                        headers_screen.put("Authorization", item.getScren_authorization());
                    } else {
                        headers_screen.put("auth", MainActivity.token);
                    }
                    DisplayImageOptions screen_options = new DisplayImageOptions.Builder()
                            .extraForDownloader(headers_screen)
                            .showImageOnLoading(R.drawable.loading)
                            .cacheInMemory(true)
                            .cacheOnDisk(true)
                            .considerExifParams(true)
                            .build();

                    ImageLoader.getInstance().displayImage(item.getScren_url(), viewHolder.imageView, screen_options, animateFirstListener);

                }
            }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(position==n || position==k){

                    }else {
                        item = feed_item.get(position);
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", item.getPost_id());
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }

                }
            });

            return convertView;
        }


    }


    private void findUsers(String search_text) {

        final String userId = prefsStart.getString("userId","");

        Log.d("FRAGMENTTT", search_text);

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "8");
        params.put("searchText", search_text);



        MainRestApi.get("findUsers",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    if(!userId.equals(jsonObject.getString("id"))) {
                                        FeedItem item = new FeedItem();
                                        item.setUser_id(jsonObject.getString("id"));
                                        item.setLogin(jsonObject.has("login") ? jsonObject.getString("login") : " ");
                                        item.setName(jsonObject.has("name") ? jsonObject.getString("name") : " ");
                                        item.setSurname(jsonObject.has("surname") ? jsonObject.getString("surname") : " ");
                                        JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                        JSONObject avatar = jsonObject.has("avatar") ? jsonObject.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                        if (!avatar.has("headers")) {
                                            item.setAvatar_hasHeader(false);
                                        } else {
                                            item.setAvatar_hasHeader(true);
                                            JSONObject ava_headers = avatar.getJSONObject("headers");
                                            item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                            item.setAva_authorization(ava_headers.getString("Authorization"));
                                            item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                            item.setAva_Host(ava_headers.getString("Host"));
                                        }
                                        item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                        item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                        feedItemP.add(item);
                                    }
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    DPadapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });

    }



    //  DialogPlusAdapter
    public class DialogPlusAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public DialogPlusAdapter(Activity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name) TextView user_name;

            @BindView(R.id.avatar)
            CircleImageView avatar;
            @BindView(R.id.avatar_check)
            CircleImageView avatar_check;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final DialogPlusAdapter.ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment26_item, parent, false);
                viewHolder = new DialogPlusAdapter.ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (DialogPlusAdapter.ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);
            Log.d("SELECTED",selected_user+"*"+position);

            if(position==selected_user){
                viewHolder.avatar_check.setVisibility(View.VISIBLE);
            }else {
                viewHolder.avatar_check.setVisibility(View.GONE);
            }
//            viewHolder.user_name.setText(item.getName() + " " + item.getSurname());
            viewHolder.user_name.setText(item.getLogin());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    footer_edit_layout.setVisibility(View.VISIBLE);
                    selected_user = position;
                    Log.d("SELECTED",selected_user+"*");
                    DPadapter.notifyDataSetChanged();



                }
            });



            return convertView;
        }
    }



    //  GridViewAdapter
    public class CloseProfileAdapter extends BaseAdapter {

        String text;
        FeedItem item;
        Activity activity;

        public CloseProfileAdapter(Activity activity, String text) {
            this.text = text;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {

            @BindView(R.id.closed)
            TextView closed;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.closer_item_grid, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.closed.setText(text);

            return convertView;
        }


    }



}


