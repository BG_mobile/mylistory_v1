package com.bugingroup.mylistory.FRAGMENT.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FriendsFB;
import com.bugingroup.mylistory.FriendsVK;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;
import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome18 extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.listView)
    ListView listView;

    ArrayList<FeedItem> feedItem,postsItem;

    private ListViewAdapter adapter;
    ViewGroup myHeader;


    LinearLayout friend_vk,friend_fb;
    String following , subscribe;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    int textColor;

    String USERID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home18, container, false);
        ButterKnife.bind(this,view);

        USERID = prefsMain.getString("userId","");

        LayoutInflater myinflater = LayoutInflater.from(getActivity());
        myHeader = (ViewGroup)myinflater.inflate(R.layout.header_home18_view, null);
        friend_vk = (LinearLayout)myHeader.findViewById(R.id.friend_vk);
        friend_fb = (LinearLayout)myHeader.findViewById(R.id.friend_fb);
        feedItem = new ArrayList<FeedItem>();

        listView.addHeaderView(myHeader);


        following = getResources().getString(R.string.unsubscribe);
        subscribe = getResources().getString(R.string.subscribe);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
        textColor = typedValue.data;

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        JsonPopularUsers();
                                    }
                                }
        );

      friend_vk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),FriendsVK.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            }
        });
        friend_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),FriendsFB.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            }
        });
        return view;

    }



    @OnClick (R.id.back)
    public void onClick(View v){
        getActivity().onBackPressed();
    }

    private void JsonPopularUsers() {
        swipeRefreshLayout.setRefreshing(true);
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit","40");

        MainRestApi.get("popularUsers",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                Log.d("popularUsers",response+"");
                try {
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        JSONArray list = body.getJSONArray("list");
                        for (int i=0;i<list.length();i++){
                            JSONObject user = (JSONObject) list.get(i);
                            if(!USERID.equals(user.getString("id"))) {
                                FeedItem item = new FeedItem();
                                item.setUser_id(user.getString("id"));
                                item.setSurname(user.has("surname") ? user.getString("surname") : "");
                                item.setLogin(user.has("login") ? user.getString("login") : "");
                                item.setName(user.has("name") ? user.getString("name") : "");
                                item.setFollowed(!user.has("followed") || user.getBoolean("followed"));
                                JSONObject jsonObject = new JSONObject("{\"avatar\":{ }}");
                                JSONObject avatar = user.has("avatar") ? user.getJSONObject("avatar") : jsonObject.getJSONObject("avatar");
                                if (!avatar.has("headers")) {
                                    item.setAvatar_hasHeader(false);
                                } else {
                                    item.setAvatar_hasHeader(true);
                                    JSONObject ava_headers = avatar.getJSONObject("headers");
                                    item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                    item.setAva_authorization(ava_headers.getString("Authorization"));
                                    item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                    item.setAva_Host(ava_headers.getString("Host"));
                                }
                                item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                item.setResourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");

                                JSONObject lastContent = new JSONObject("{\"lastContents\":[ ]}");

                                JSONArray lastContents = user.has("lastContents") ? user.getJSONArray("lastContents") : lastContent.getJSONArray("lastContents");
                                postsItem = new ArrayList<FeedItem>();
                                for (int j = 0; j < lastContents.length(); j++) {
                                    FeedItem postsitem = new FeedItem();
                                    JSONObject contents = (JSONObject) lastContents.get(j);

                                    if (!contents.has("headers")) {
                                        postsitem.setHasHeader(false);
                                    } else {
                                        JSONObject header = contents.getJSONObject("headers");
                                        postsitem.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                        postsitem.setAuthorization(header.getString("Authorization"));
                                        postsitem.setX_amz_date(header.getString("x-amz-date"));
                                        postsitem.setHost(header.getString("Host"));
                                        postsitem.setHasHeader(true);
                                    }
                                    postsitem.setUrl(contents.has("url") ? contents.getString("url") : "no");
                                    postsitem.setResourceId(contents.has("resourceId") ? contents.getString("resourceId") : "no");
                                    postsItem.add(postsitem);
                                }
                                item.setPostsItem(postsItem);

                                feedItem.add(item);
                            }
                        }
                        adapter = new ListViewAdapter (feedItem);
                        listView.setAdapter(adapter);
                        swipeRefreshLayout.setRefreshing(false);

                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});

    }

    @Override
    public void onRefresh() {
        JsonPopularUsers();
    }


    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
        ArrayList<FeedItem> feedItem;
        FeedItem item;

        public ListViewAdapter(ArrayList<FeedItem> feedItem) {
            this.feedItem = feedItem;
        }

        class ViewHolder {

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.to_user)
            LinearLayout to_user;

            @BindView(R.id.name)
            TextView name;
            @BindView(R.id.close_btn)
            ImageButton close_btn;
            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;

            @BindView(R.id.content_img1) ImageView imageView1;
            @BindView(R.id.content_img2) ImageView imageView2;
            @BindView(R.id.content_img3) ImageView imageView3;


            final int[] imageview ={R.id.content_img1,R.id.content_img2,R.id.content_img3};

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home18_item_list, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            item = feedItem.get(position);

//            viewHolder.name.setText(item.getSurname()+" "+item.getName());
            viewHolder.name.setText(item.getLogin());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if(item.getAvatar_hasHeader()){
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            }else {
                ava_headers.put("auth", MainActivity.token);
            }
            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();
            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);

            ArrayList<FeedItem> postsItem = item.getPostsItem();
            if(postsItem.size()==0){
                viewHolder.imageView1.setVisibility(View.GONE);
                viewHolder.imageView2.setVisibility(View.GONE);
                viewHolder.imageView3.setVisibility(View.GONE);
            }else {
                viewHolder.imageView1.setVisibility(View.VISIBLE);
                viewHolder.imageView2.setVisibility(View.VISIBLE);
                viewHolder.imageView3.setVisibility(View.VISIBLE);
            }
            for (int i=0;i<postsItem.size();i++){
                FeedItem item2 = postsItem.get(i);
                Map<String, String> headers_content = new HashMap<String, String>();
                if (item2.getHasHeader()) {
                    headers_content.put("x-amz-content-sha256", item2.getX_amz_content_sha256());
                    headers_content.put("x-amz-date", item2.getX_amz_date());
                    headers_content.put("Host", item2.getHost());
                    headers_content.put("Authorization", item2.getAuthorization());
                } else {
                    headers_content.put("auth", token);
                }

                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_content)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                ImageView img = (ImageView) convertView.findViewById(viewHolder.imageview[i]);
                ImageLoader.getInstance().displayImage(item2.getUrl(), img, header_options, animateFirstListener);

            }

            viewHolder.close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    feedItem.remove(position);
                    notifyDataSetChanged();
                }
            });

            int[] attrs = new int[] { R.attr.bg_button_subscribe /* index 0 */};
            TypedArray ta = getActivity().obtainStyledAttributes(attrs);
            final Drawable bg_button_subscribe = ta.getDrawable(0 /* index */);
            ta.recycle();
            int[] attrs2 = new int[] { R.attr.bg_button_subscribe_golden /* index 0 */};
            TypedArray ta2 = getActivity().obtainStyledAttributes(attrs2);
            final Drawable bg_button_subscribe_golden = ta2.getDrawable(0 /* index */);
            ta2.recycle();

            if(!item.getFollowed()){
                viewHolder.subscripe_btn.setText(subscribe);
                viewHolder.subscripe_btn.setBackground(bg_button_subscribe_golden);
                viewHolder.subscripe_btn.setTextColor(getResources().getColor(R.color.white));
            }else {
                viewHolder.subscripe_btn.setText(following);
                viewHolder.subscripe_btn.setBackground(bg_button_subscribe);
                viewHolder.subscripe_btn.setTextColor(textColor);
            }

            viewHolder.subscripe_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item = feedItem.get(position);

                    RequestParams params = new RequestParams();
                    params.put("favoriteId", item.getUser_id());

                    if(item.getFollowed()){
                        MainRestApi.post("unfollowing",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                Log.d("FOLLOWINGUNR",response+"*");
                                try {
                                    if(response.getBoolean("success")){
                                        viewHolder.subscripe_btn.setText(subscribe);
                                        viewHolder.subscripe_btn.setBackground(bg_button_subscribe_golden);
                                        item.setFollowed(false);
                                        notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }else {
                        MainRestApi.post("addFollower",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                Log.d("REsponse:",response+"*");
                                try {
                                    if(response.getBoolean("success")){
                                        viewHolder.subscripe_btn.setText(following);
                                        viewHolder.subscripe_btn.setBackground(bg_button_subscribe);
                                        viewHolder.subscripe_btn.setTextColor(textColor);
                                        item.setFollowed(true);
                                        notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }

                }
            });


            viewHolder.to_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feedItem.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });



            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return feedItem.size();
        }
    }



}