package com.bugingroup.mylistory.FRAGMENT.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.SingleChat;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome19 extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.listView)
     ListView listView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ListViewAdapter adapter;

    ArrayList<FeedItem> feedItem;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home19, container, false);
        ButterKnife.bind(this,view);
        return  view;
    }
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        chatList();
                                    }
                                }
        );


    }

    @OnClick(R.id.back)
    void BackClick(){
        getActivity().onBackPressed();
    }
    @OnClick(R.id.add)
    void AddClick(){
        ((MainActivity) getActivity()).replaceFragment(new FragmentHome22());
    }




    View.OnClickListener headerClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            ((MainActivity) getActivity()).replaceFragment(new FragmentHome20());
        }};


    private void chatList() {
        swipeRefreshLayout.setRefreshing(true);


        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "40");


        MainRestApi.get("chatList",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("chatList",response+"*");
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                   JSONArray users = jsonObject.getJSONArray("users");
                                    for (int u=0;u<users.length();u++){
                                        JSONObject user = (JSONObject) users.get(u);
                                        if(!user.getBoolean("current")){
                                            item.setChatId(jsonObject.getString("id"));
                                            item.setUser_id(user.getString("id"));
                                            item.setLogin(user.has("login")?user.getString("login"):"");
                                            item.setName(user.has("name")?user.getString("name"):"");
                                            item.setSurname(user.has("surname")?user.getString("surname"):"");
                                            JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                            JSONObject avatar = user.has("avatar") ? user.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                            if (!avatar.has("headers")) {
                                                item.setAvatar_hasHeader(false);
                                            } else {
                                                item.setAvatar_hasHeader(true);
                                                JSONObject ava_headers = avatar.getJSONObject("headers");
                                                item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                                item.setAva_authorization(ava_headers.getString("Authorization"));
                                                item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                                item.setAva_Host(ava_headers.getString("Host"));
                                            }
                                            item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                            item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");

                                            if(jsonObject.has("lastMessage")) {
                                                JSONObject lastMessage = jsonObject.getJSONObject("lastMessage");
                                                JSONObject content = lastMessage.getJSONObject("content");
                                                if(!content.getString("contentType").equals("TEXT")){
                                                    item.setCreateDate(0);
                                                    item.setContentType("");
                                                    item.setDescription("");
                                                }else {
                                                    item.setCreateDate(lastMessage.getLong("createDate"));
                                                    item.setContentType(content.getString("contentType"));
                                                    item.setDescription(content.has("description")?content.getString("description"):" ");
                                                }
                                            }else {
                                                item.setCreateDate(0);
                                                item.setContentType("");
                                                item.setDescription("");
                                            }

                                            feedItem.add(item);
                                        }
                                    }

                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    adapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(adapter);
                    swipeRefreshLayout.setRefreshing(false);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
            }

        });

    }

    @Override
    public void onRefresh() {
        chatList();

    }

    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;

            @BindView(R.id.create_date)
            TextView create_date;

            @BindView(R.id.last_message)
            TextView last_message;

            @BindView(R.id.avatar)
            CircleImageView avatar;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home19_item_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);


            viewHolder.user_name.setText(item.getLogin());
//            viewHolder.user_name.setText(item.getName() + " " + item.getSurname());
            viewHolder.last_message.setText(item.getDescription());
            viewHolder.create_date.setText(URL_CONSTANT.getTimeAgo(item.getCreateDate(), getActivity()));

            final HashMap<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            final DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Log.d("AAAAAAA",item.getChatId());
                    startActivity(new Intent(getActivity(),SingleChat.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            .putExtra("chatId", item.getChatId())
                            .putExtra("userId",item.getUser_id())
                            .putExtra("ava_headers",  ava_headers)
                            .putExtra("avatar_url",item.getAvatar_url())
//                            .putExtra("name",item.getName() + " " + item.getSurname()));
                            .putExtra("name",item.getLogin()));
                }
            });
            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.chat_menu_dialog);
                    dialog.show();
                    TextView delete = (TextView) dialog.findViewById(R.id.delete);
                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            item = feed_item.get(position);
                            RequestParams params = new RequestParams();
                            params.put("chatId", item.getChatId());


                            MainRestApi.post("deleteChat",params,new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                    Log.d("deleteChat",response+"*");
                                    try {
                                        if(response.getBoolean("success")){
                                            feed_item.remove(position);
                                            notifyDataSetChanged();
                                        } else {
                                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                    }
                                        dialog.dismiss();

                                } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }});
                        }
                    });
                    return false;
                }
            });
            return convertView;
        }
    }

}
