package com.bugingroup.mylistory.FRAGMENT.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome20 extends Fragment {

    private ListView listView;
    private ListViewAdapter adapter;
    ViewGroup myHeader;
    ImageButton add,back;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home19, container, false);

        LayoutInflater myinflater = LayoutInflater.from(getActivity());
        this.listView = (ListView) view.findViewById(R.id.listView);
        myHeader = (ViewGroup)myinflater.inflate(R.layout.header_home19_view, null);
        adapter = new ListViewAdapter (5);
        listView.addHeaderView(myHeader);
        listView.setAdapter(adapter);
        add  = (ImageButton) view.findViewById(R.id.add);
        back  = (ImageButton) view.findViewById(R.id.back);
        add.setOnClickListener(OnImageButtonclickListener);
        back.setOnClickListener(OnImageButtonclickListener);
        return view;

    }


    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.add:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentHome22());
                    break;
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
            }
        }
    };


    //  ListViewAdapter
    private class ListViewAdapter extends BaseAdapter {

        private int  count;

        public ListViewAdapter(int count) {
            this.count = count;
        }

        class ViewHolder {

        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home19_item_list, viewGroup, false);
                viewHolder = new ViewHolder();
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).replaceFragment(new FragmentHome21());
                }
            });

            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return count;
        }
    }


}