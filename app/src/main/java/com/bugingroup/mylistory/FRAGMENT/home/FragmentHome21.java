package com.bugingroup.mylistory.FRAGMENT.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome21 extends Fragment {

    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.listView)
     ListView listView;
    private ListViewAdapter adapter;
    ImageButton add,back;
    String chatId;
    ArrayList<FeedItem> feedItem;
    private Handler handler;
    private Runnable runable;
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home21, container, false);
        ButterKnife.bind(this,view);
        return  view;
    }
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            chatId = bundle.getString("chatId");
        }
        messageList(chatId);
        handler = new Handler();
        runable = new Runnable() {

            @Override
            public void run() {

                messageList(chatId);

                Log.d("AAA","FDF");
                //also call the same runnable
                handler.postDelayed(this, 10000);
            }
        };
        handler.postDelayed(runable,10000);

    }


    @OnClick(R.id.back)
    void BackClick(){
        getActivity().onBackPressed();
        handler.removeCallbacks(runable);
    }




    @OnClick(R.id.ic_send)
    void SendClick(){

        String body = "{\"chatId\":\""+chatId+"\",  \"description\":\""+message.getText().toString()+"\", \"contentType\":\"TEXT\", \"resourceId\": null}";

        MainRestApi.post(getActivity(),"sendMessage",body,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d("RESPONS",response+"*");
                    if(response.getBoolean("success")){
                        message.setText(null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void messageList(String chatId) {


        RequestParams params = new RequestParams();
        params.put("chatId", chatId);



        MainRestApi.get("messageList",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("messageList",response+"*");
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                    item.setChatId(jsonObject.getString("id"));
                                    item.setFrom(jsonObject.getString("from"));
                                    JSONObject content = jsonObject.getJSONObject("content");
                                    item.setContentType(content.getString("contentType"));
                                    item.setDescription(content.getString("description"));
                                    feedItem.add(item);
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    adapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
            }

        });

    }


    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.message_m)
            TextView message_m;

            @BindView(R.id.message_y)
            TextView message_y;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home21_item_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);
            viewHolder.message_m.setText(item.getDescription());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).replaceFragment(new FragmentHome21());
                }
            });

            return convertView;
        }


    }


}