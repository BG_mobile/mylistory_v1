package com.bugingroup.mylistory.FRAGMENT.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.SingleChat;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;
import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentHome22 extends Fragment {

    @BindView(R.id.listView)
     ListView listView;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private ListViewAdapter adapter;
    ArrayList<FeedItem> feedItem;

    int check_position;
    String userId;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home22, container, false);
        ButterKnife.bind(this,view);
        return  view;
    }
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userId = prefsMain.getString("userId","");

        findUsers("");
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    findUsers(search.getText().toString());
                    return true;
                }
                return false;
            }
        });

    }

    @OnClick(R.id.back)
    void BackClick(){
        getActivity().onBackPressed();
    }

    @OnClick(R.id.next)
    void NextClick(){
        Log.d("ASAS",feedItem.get(check_position).getName());
        String body = "{\"name\":\"nurik\", \"users\":[\""+feedItem.get(check_position).getUser_id()+"\"]}";

        MainRestApi.post(getActivity(),"addChat",body,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d("RESPONS",response+"*");
                    if(response.getBoolean("success")){
//                        startActivity(new Intent(getActivity(),SingleChat.class)
//                                .putExtra("chatId", response.getString("body")));

                        HashMap<String, String> ava_headers = new HashMap<String, String>();
                        if (feedItem.get(check_position).getAvatar_hasHeader()) {
                            ava_headers.put("x-amz-content-sha256", feedItem.get(check_position).getAva_x_amz_content_sha256());
                            ava_headers.put("x-amz-date", feedItem.get(check_position).getAva_x_amz_date());
                            ava_headers.put("Host", feedItem.get(check_position).getAva_Host());
                            ava_headers.put("Authorization", feedItem.get(check_position).getAva_authorization());
                        } else {
                            ava_headers.put("auth", token);
                        }


                        startActivity(new Intent(getActivity(),SingleChat.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                .putExtra("chatId", response.getString("body"))
                                .putExtra("userId",feedItem.get(check_position).getUser_id())
                                .putExtra("ava_headers",  ava_headers)
                                .putExtra("avatar_url",feedItem.get(check_position).getAvatar_url())
//                                .putExtra("name",feedItem.get(check_position).getName()));
                                .putExtra("name",feedItem.get(check_position).getLogin()));




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    private void findUsers(String search_text) {
        progressBar.setVisibility(View.VISIBLE);
        final String userId = prefsMain.getString("userId","");

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "40");
        if(!search_text.equals("null"))
            params.put("searchText", search_text);



        MainRestApi.get("findUsers",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    if(!userId.equals(jsonObject.getString("id"))) {
                                        FeedItem item = new FeedItem();
                                        item.setUser_id(jsonObject.getString("id"));
                                        item.setLogin(jsonObject.has("login") ? jsonObject.getString("login") : " ");
                                        item.setName(jsonObject.has("name") ? jsonObject.getString("name") : " ");
                                        item.setSurname(jsonObject.has("surname") ? jsonObject.getString("surname") : " ");
                                        JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                        JSONObject avatar = jsonObject.has("avatar") ? jsonObject.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                        if (!avatar.has("headers")) {
                                            item.setAvatar_hasHeader(false);
                                        } else {
                                            item.setAvatar_hasHeader(true);
                                            JSONObject ava_headers = avatar.getJSONObject("headers");
                                            item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                            item.setAva_authorization(ava_headers.getString("Authorization"));
                                            item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                            item.setAva_Host(ava_headers.getString("Host"));
                                        }
                                        item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                        item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                        feedItem.add(item);
                                    }
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    adapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progressBar.setVisibility(View.GONE);
                System.out.println(errorResponse);
            }

        });

    }

    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.checkBox)
            CheckBox checkBox;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home22_item_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);

            if(position==check_position){
                viewHolder.checkBox.setChecked(true);
            }else {
                viewHolder.checkBox.setChecked(false);
            }

            viewHolder.user_name.setText(item.getLogin());
//            viewHolder.user_name.setText(item.getName() + " " + item.getSurname());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    check_position = position;
                    adapter.notifyDataSetChanged();
                }
            });
            return convertView;
        }
    }

}
