package com.bugingroup.mylistory.FRAGMENT.like.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome16;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch25;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 04.11.2016.
 */
public class LikeTabFragment1 extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.listView)
    ListView listView;


    private ListViewAdapter adapter;
    ArrayList<FeedItem> feedItem,postsItem;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    public LikeTabFragment1() {

    }
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        ButterKnife.bind(this,view);
        return  view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        likedPosts();
                                    }
                                }
        );


    }

    private void likedPosts() {
        Log.d("Fragment1111","OKOK22");
        swipeRefreshLayout.setRefreshing(true);

        MainRestApi.get("likedPosts",null,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("likedPosts",response+"*");
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                    JSONObject user = jsonObject.getJSONObject("user");
                                    item.setUser_id(user.getString("id"));
                                    item.setLogin(user.has("login")?user.getString("login"):" ");
                                    item.setName(user.has("name")?user.getString("name"):" ");
                                    item.setSurname(user.has("surname")?user.getString("surname"):" ");
                                    JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                    JSONObject avatar = user.has("avatar") ? user.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                    if (!avatar.has("headers")) {
                                        item.setAvatar_hasHeader(false);
                                    } else {
                                        item.setAvatar_hasHeader(true);
                                        JSONObject ava_headers = avatar.getJSONObject("headers");
                                        item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                        item.setAva_authorization(ava_headers.getString("Authorization"));
                                        item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                        item.setAva_Host(ava_headers.getString("Host"));
                                    }
                                    item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                    item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                    JSONArray JAposts = jsonObject.getJSONArray("posts");

                                    postsItem = new ArrayList<FeedItem>();
                                    for (int j = 0; j < JAposts.length(); j++) {
                                        JSONObject posts = (JSONObject) JAposts.get(j);
                                         FeedItem postsitem = new FeedItem();
                                        postsitem.setPost_id(posts.getString("postId"));
                                        postsitem.setLikeDate(posts.getLong("likeDate"));
                                        JSONArray JScontents = posts.getJSONArray("contents");
                                        JSONObject contents = (JSONObject) JScontents.get(0);
                                        JSONObject scren =new JSONObject("{\"screenshot\":{ }}");
                                        JSONObject screenshot = contents.has("screenshot")?contents.getJSONObject("screenshot"):scren.getJSONObject("screenshot");

                                        if (!contents.has("headers")) {
                                            postsitem.setHasHeader(false);
                                        } else {
                                            JSONObject header = contents.getJSONObject("headers");
                                            postsitem.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                            postsitem.setAuthorization(header.getString("Authorization"));
                                            postsitem.setX_amz_date(header.getString("x-amz-date"));
                                            postsitem.setHost(header.getString("Host"));
                                            postsitem.setHasHeader(true);
                                        }
                                        postsitem.setContentType(contents.has("contentType") ? contents.getString("contentType") : "PICTURE");

                                        postsitem.setUrl(contents.has("url") ? contents.getString("url") : "no");
                                        postsitem.setResourceId(contents.has("resourceId") ? contents.getString("resourceId") : "no");
                                        postsitem.setScren_url(screenshot.has("url")?screenshot.getString("url"):"no");
                                        postsitem.setScren_resourceId(screenshot.has("resourceId")?screenshot.getString("resourceId"):"no");
                                        if(!screenshot.has("headers")){
                                            postsitem.setScren_hasHeader(false);
                                        }else {
                                            JSONObject header = screenshot.getJSONObject("headers");
                                            postsitem.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                            postsitem.setScren_authorization(header.getString("Authorization"));
                                            postsitem.setScren_x_amz_date(header.getString("x-amz-date"));
                                            postsitem.setScren_Host(header.getString("Host"));
                                            postsitem.setScren_hasHeader(true);
                                        }
                                        postsItem.add(postsitem);
                                        if(j==9){
                                            break;
                                        }
                                    }
                                    item.setPostsItem(postsItem);
                                    feedItem.add(item);
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorCode")+response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                   adapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(adapter);
                    swipeRefreshLayout.setRefreshing(false);
                   } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println("NNNNNNNN"+errorResponse);
            }

        });

    }

    @Override
    public void onRefresh() {
        likedPosts();
    }

    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;
        int []count;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
            count = new int[feedItem.size()];
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.name)
            TextView user_name;

             @BindView(R.id.likedate)
            TextView likedate;

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.to_other_profile)
            RelativeLayout to_other_profile;

            @BindView(R.id.imageView1) ImageView imageView1;
            @BindView(R.id.imageView2) ImageView imageView2;
            @BindView(R.id.imageView3) ImageView imageView3;
            @BindView(R.id.imageView4) ImageView imageView4;
            @BindView(R.id.imageView5) ImageView imageView5;
            @BindView(R.id.imageView6) ImageView imageView6;
            @BindView(R.id.imageView7) ImageView imageView7;
            @BindView(R.id.imageView8) ImageView imageView8;
            @BindView(R.id.imageView9) ImageView imageView9;
            @BindView(R.id.imageView10) ImageView imageView10;

            @BindView(R.id.ic_video1) ImageView ic_video1;
            @BindView(R.id.ic_video2) ImageView ic_video2;
            @BindView(R.id.ic_video3) ImageView ic_video3;
            @BindView(R.id.ic_video4) ImageView ic_video4;
            @BindView(R.id.ic_video5) ImageView ic_video5;
            @BindView(R.id.ic_video6) ImageView ic_video6;
            @BindView(R.id.ic_video7) ImageView ic_video7;
            @BindView(R.id.ic_video8) ImageView ic_video8;
            @BindView(R.id.ic_video9) ImageView ic_video9;
            @BindView(R.id.ic_video10) ImageView ic_video10;


            final int[] imageview ={R.id.imageView1,R.id.imageView2,R.id.imageView3,R.id.imageView4,R.id.imageView5,R.id.imageView6,R.id.imageView7,R.id.imageView8,R.id.imageView9,R.id.imageView10};

            final int[] ic_video ={R.id.ic_video1,R.id.ic_video2,R.id.ic_video3,R.id.ic_video4,R.id.ic_video5,R.id.ic_video6,R.id.ic_video7,R.id.ic_video8,R.id.ic_video9,R.id.ic_video10};

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment_tab1_like_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);
            viewHolder.user_name.setText(item.getLogin());
//            viewHolder.user_name.setText(item.getName()+" "+item.getSurname());


            ArrayList<FeedItem> postsItem = item.getPostsItem();
            Long [] likeDate= new Long[postsItem.size()];
            count[position] = postsItem.size();
            Log.d("AAAASSS",count+"**");
            for (int i=0;i<postsItem.size();i++){
                FeedItem item2 = postsItem.get(i);
                likeDate [i] = item2.getLikeDate();

                if(item2.getContentType().equals("PICTURE")) {
                    ImageView video = (ImageView) convertView.findViewById(viewHolder.ic_video[i]);
                    video.setVisibility(View.GONE);
                    Map<String, String> headers_content = new HashMap<String, String>();
                    if (item2.getHasHeader()) {
                        headers_content.put("x-amz-content-sha256", item2.getX_amz_content_sha256());
                        headers_content.put("x-amz-date", item2.getX_amz_date());
                        headers_content.put("Host", item2.getHost());
                        headers_content.put("Authorization", item2.getAuthorization());
                    } else {
                        headers_content.put("auth", token);
                    }

                    DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                            .extraForDownloader(headers_content)
                            .showImageOnLoading(R.drawable.loading)
                            .cacheInMemory(true)
                            .cacheOnDisk(true)
                            .considerExifParams(true)
                            .build();


                    ImageView img = (ImageView) convertView.findViewById(viewHolder.imageview[i]);
                   ImageLoader.getInstance().displayImage(item2.getUrl(), img, header_options, animateFirstListener);
                }else {
                    ImageView video = (ImageView) convertView.findViewById(viewHolder.ic_video[i]);
                    video.setVisibility(View.VISIBLE);
                    Map<String, String> headers_screen = new HashMap<String, String>();
                    if (item2.getScren_hasHeader()) {
                        headers_screen.put("x-amz-content-sha256", item2.getScren_x_amz_content_sha256());
                        headers_screen.put("x-amz-date", item2.getScren_x_amz_date());
                        headers_screen.put("Host", item2.getScren_Host());
                        headers_screen.put("Authorization", item2.getScren_authorization());
                    } else {
                        headers_screen.put("auth", token);
                    }

                    DisplayImageOptions screen_options = new DisplayImageOptions.Builder()
                            .extraForDownloader(headers_screen)
                            .showImageOnLoading(R.drawable.loading)
                            .cacheInMemory(true)
                            .cacheOnDisk(true)
                            .considerExifParams(true)
                            .build();


                    ImageView img = (ImageView) convertView.findViewById(viewHolder.imageview[i]);
                    ImageLoader.getInstance().displayImage(item2.getScren_url(), img, screen_options, animateFirstListener);
                }

            }
            List b = Arrays.asList(likeDate);
            String text = getResources().getString(R.string.likedate, URL_CONSTANT.getTimeAgo((long)Collections.max(b),getActivity()));
            viewHolder.likedate.setText(text);



            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);

            viewHolder.imageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=1) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(0).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=2) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(1).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=3) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(2).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=4) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(3).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=5) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(4).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=6) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(5).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=7) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(6).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }
                }
            });

              viewHolder.imageView8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[position]>=8) {
                        item = feed_item.get(position);
                        String postId = item.getPostsItem().get(7).getPost_id();
                        Bundle bundle = new Bundle();
                        Log.d("DDD123", item.getPost_id() + "***");
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                    }

                }
            });


            viewHolder.to_other_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });
            return convertView;
        }

    }



}
