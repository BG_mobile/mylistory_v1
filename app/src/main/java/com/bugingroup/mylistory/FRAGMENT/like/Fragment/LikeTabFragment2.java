package com.bugingroup.mylistory.FRAGMENT.like.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome16;
import com.bugingroup.mylistory.FRAGMENT.like.FragmentLike42;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch25;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 04.11.2016.
 */
public class LikeTabFragment2 extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.listView)
    ListView listView;
    TextView request_count;
    private ListViewAdapter adapter;
    ViewGroup myHeader;
    ArrayList<FeedItem> feedItem;
    ArrayList<FeedItem> feedItem_requests;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    public LikeTabFragment2() {

    }
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        ButterKnife.bind(this,view);
        return  view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LayoutInflater myinflater = LayoutInflater.from(getActivity());
        myHeader = (ViewGroup)myinflater.inflate(R.layout.header_like_tab2, null);
        request_count = (TextView) myHeader.findViewById(R.id.request_count);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        myLikedPosts();
                                    }
                                }
        );

    }
    View.OnClickListener headerClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            ((MainActivity) getActivity()).replaceFragment(new FragmentLike42(feedItem_requests));

        }};

    private void myLikedPosts() {
        Log.d("Fragment2222", "OKOK22");

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "10");

        MainRestApi.get("myLikedPosts", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("myLikedPosts", response+"*");
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    JSONObject user = jsonObject.getJSONObject("user");
                                    JSONArray JAposts = jsonObject.getJSONArray("posts");
                                    for (int j = 0; j < JAposts.length(); j++) {
                                        FeedItem item = new FeedItem();
                                        item.setUser_id(user.getString("id"));
                                        item.setLogin(user.has("login")?user.getString("login"):" ");
                                        item.setName(user.has("name")?user.getString("name"):" ");
                                        item.setSurname(user.has("surname")?user.getString("surname"):" ");
                                        JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                        JSONObject avatar = user.has("avatar") ? user.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                        if (!avatar.has("headers")) {
                                            item.setAvatar_hasHeader(false);
                                        } else {
                                            item.setAvatar_hasHeader(true);
                                            JSONObject ava_headers = avatar.getJSONObject("headers");
                                            item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                            item.setAva_authorization(ava_headers.getString("Authorization"));
                                            item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                            item.setAva_Host(ava_headers.getString("Host"));
                                        }
                                        item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                        item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                        JSONObject posts = (JSONObject) JAposts.get(j);

                                        item.setPost_id(posts.getString("postId"));
                                        item.setLikeDate(posts.getLong("likeDate"));
                                        JSONArray JScontents = posts.getJSONArray("contents");
                                        JSONObject contents = (JSONObject) JScontents.get(0);
                                        JSONObject scren =new JSONObject("{\"screenshot\":{ }}");
                                        JSONObject screenshot = contents.has("screenshot")?contents.getJSONObject("screenshot"):scren.getJSONObject("screenshot");

                                        if (!contents.has("headers")) {
                                            item.setHasHeader(false);
                                        } else {
                                            JSONObject header = contents.getJSONObject("headers");
                                            item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                            item.setAuthorization(header.getString("Authorization"));
                                            item.setX_amz_date(header.getString("x-amz-date"));
                                            item.setHost(header.getString("Host"));
                                            item.setHasHeader(true);
                                        }
                                        item.setContentType(contents.has("contentType") ? contents.getString("contentType") : "PICTURE");
                                        item.setUrl(contents.has("url") ? contents.getString("url") : "no");
                                        item.setResourceId(contents.has("resourceId") ? contents.getString("resourceId") : "no");
                                        item.setScren_url(screenshot.has("url")?screenshot.getString("url"):"no");
                                        item.setScren_resourceId(screenshot.has("resourceId")?screenshot.getString("resourceId"):"no");
                                        if(!screenshot.has("headers")){
                                            item.setScren_hasHeader(false);
                                        }else {
                                            JSONObject header = screenshot.getJSONObject("headers");
                                            item.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                            item.setScren_authorization(header.getString("Authorization"));
                                            item.setScren_x_amz_date(header.getString("x-amz-date"));
                                            item.setScren_Host(header.getString("Host"));
                                            item.setScren_hasHeader(true);
                                        }
                                        feedItem.add(item);
                                    }
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorCode"), Toast.LENGTH_SHORT).show();
                    }

                    adapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(adapter);
                    swipeRefreshLayout.setRefreshing(false);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });
        followerRequests();

    }
    private void followerRequests() {

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "10");

        MainRestApi.get("followingRequests", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("followingRequests",response+"*");
                feedItem_requests = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        if(body.has("list")) {
                            JSONArray list = body.getJSONArray("list");
                            if(list.length()>0) {
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject posts = (JSONObject) list.get(i);
                                    JSONObject follower = posts.getJSONObject("follower");
                                    FeedItem item = new FeedItem();

                                    item.setRequest_id(posts.has("id") ? posts.getString("id") : "0");
                                    item.setUser_id(follower.has("id") ? follower.getString("id") : "0");
                                    item.setLogin(follower.has("login") ? follower.getString("login") : "");
                                    item.setName(follower.has("name") ? follower.getString("name") : "");
                                    item.setSurname(follower.has("surname") ? follower.getString("surname") : "");
                                    JSONObject jsonObject = new JSONObject("{\"avatar\":{ }}");
                                    JSONObject avatar = follower.has("avatar") ? follower.getJSONObject("avatar") : jsonObject.getJSONObject("avatar");
                                    if (!avatar.has("headers")) {
                                        item.setAvatar_hasHeader(false);
                                    } else {
                                        JSONObject ava_headers = avatar.getJSONObject("headers");
                                        item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                        item.setAva_authorization(ava_headers.getString("Authorization"));
                                        item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                        item.setAva_Host(ava_headers.getString("Host"));
                                        item.setAvatar_hasHeader(true);
                                    }

                                    item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                    item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                    feedItem_requests.add(item);
                                }
                                listView.addHeaderView(myHeader);
                                myHeader.setVisibility(View.VISIBLE);
                                myHeader.setOnClickListener(headerClickListener);
                                request_count.setText(String.valueOf(list.length()));
                            }else {
                                myHeader.setVisibility(View.GONE);
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onRefresh() {
        myLikedPosts();
    }


    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;

            @BindView(R.id.likedate)
            TextView likedate;

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.imageView)
            ImageView imageView;


            @BindView(R.id.ic_video)
            ImageView ic_video;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment_tab2_like_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            item = feed_item.get(position);
            viewHolder.user_name.setText(item.getLogin());
//            viewHolder.user_name.setText(item.getName()+" "+item.getSurname());


            if(item.getContentType().equals("PICTURE")) {
                viewHolder.ic_video.setVisibility(View.GONE);
                Map<String, String> headers_content = new HashMap<String, String>();
                if (item.getHasHeader()) {
                    headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                    headers_content.put("x-amz-date", item.getX_amz_date());
                    headers_content.put("Host", item.getHost());
                    headers_content.put("Authorization", item.getAuthorization());
                } else {
                    headers_content.put("auth", token);
                }

                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_content)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                ImageLoader.getInstance().displayImage(item.getUrl(), viewHolder.imageView, header_options, animateFirstListener);
            }else {
                viewHolder.ic_video.setVisibility(View.VISIBLE);
                Map<String, String> headers_screen = new HashMap<String, String>();
                if (item.getScren_hasHeader()) {
                    headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                    headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                    headers_screen.put("Host", item.getScren_Host());
                    headers_screen.put("Authorization", item.getScren_authorization());
                } else {
                    headers_screen.put("auth", token);
                }

                DisplayImageOptions screen_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_screen)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                ImageLoader.getInstance().displayImage(item.getScren_url(), viewHolder.imageView, screen_options, animateFirstListener);
            }

            String text = getResources().getString(R.string.likedate, URL_CONSTANT.getTimeAgo(item.getLikeDate(),getActivity()));
            viewHolder.likedate.setText(text);



            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);



            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    String postId = item.getPost_id();
                    Bundle bundle = new Bundle();
                    Log.d("DDD123",item.getPost_id()+"***");
                    bundle.putString("postId", postId);
                    Fragment fragment = new FragmentSearch25();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });

            return convertView;
        }

    }

}
