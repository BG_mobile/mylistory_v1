package com.bugingroup.mylistory.FRAGMENT.like;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.bugingroup.mylistory.FRAGMENT.like.Fragment.LikeTabFragment1;
import com.bugingroup.mylistory.FRAGMENT.like.Fragment.LikeTabFragment2;
import com.bugingroup.mylistory.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 11.11.2016.
 */
@SuppressLint("ValidFragment")
public class FragmentLike extends Fragment {

    @BindView(R.id.tabs)
    PagerSlidingTabStrip tabsStrip;
    private LinearLayout mTabsLinearLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;
    PagerAdapter adapter ;
    int  color_theme,color_theme2;
    public FragmentLike() {
    }
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_like, container, false);

        ButterKnife.bind(this,view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
        color_theme = typedValue.data;

        TypedValue typedValue2 = new TypedValue();
        Resources.Theme theme2 = getActivity().getTheme();
        theme2.resolveAttribute(R.attr.colorText, typedValue2, true);
        color_theme2 = typedValue2.data;


        setUpTabStrip();
        adapter = new PagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        tabsStrip.setViewPager(viewPager);

        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position){
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/light.ttf");
                for(int i=0; i < mTabsLinearLayout.getChildCount(); i++){
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    tv.setTypeface(tf);
                    if(i == position){
                        tv.setTextColor(color_theme2);
                    } else {
                        tv.setTextColor(color_theme);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }


    public void setUpTabStrip(){
        mTabsLinearLayout = ((LinearLayout)tabsStrip.getChildAt(0));
        for(int i=0; i < mTabsLinearLayout.getChildCount(); i++){
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
            if(i == 0){
                tv.setTextColor(color_theme2);
            } else {
                tv.setTextColor(color_theme);
            }
        }
    }


    public class PagerAdapter extends FragmentStatePagerAdapter {

        private final String[] TITLES = {"Подписчики","Вы"};


        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new LikeTabFragment1();
                case 1:
                default:
                    return new LikeTabFragment2();
            }

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }


    }
}

