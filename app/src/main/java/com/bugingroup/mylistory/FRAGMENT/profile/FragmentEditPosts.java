package com.bugingroup.mylistory.FRAGMENT.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentEditPosts extends Fragment {


    @BindView(R.id.user_name)
    TextView userName;


    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.contents_image)
    ImageView contents_image;


    @BindView(R.id.avatar)
    ImageView avatar;



    String postId;
    ArrayList<FeedItem> feedItem;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_posts, container, false);
        ButterKnife.bind(this,view);
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            postId = bundle.getString("postId");
        }
        Log.d("DDDD1",postId);
        PostGetServer(postId);
        Log.d("DDDD2",postId);


        return view;
    }

    private void PostGetServer(String postId) {


        Log.d("DDDD3",postId);

        RequestParams params = new RequestParams();
        params.put("postId", postId);

        MainRestApi.get("post",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        JSONObject scren =new JSONObject("{\"screenshot\":{ }}");
                        JSONObject posts = response.getJSONObject("body");
                        JSONArray contents = posts.getJSONArray("contents");
                        JSONObject avatar = posts.getJSONObject("avatar");
                        for (int j = 0; j < contents.length(); j++) {
                            JSONObject content = (JSONObject) contents.get(j);
                            JSONObject screenshot = content.has("screenshot")?content.getJSONObject("screenshot"):scren.getJSONObject("screenshot");
                            FeedItem item = new FeedItem();
                            item.setUser_id(posts.getString("userId"));
                            item.setPost_id(posts.getString("postId"));
                            item.setLikeCount( posts.has("likeCount") ? posts.getInt("likeCount") : 0);
                            item.setCommentCount( posts.has("commentCount") ? posts.getInt("commentCount") : 0);
                            item.setLogin(posts.has("login")?posts.getString("login"):"");
                            item.setName(posts.has("userName")?posts.getString("userName"):"");
                            item.setLiked(posts.getBoolean("liked"));
                            item.setCreateDate(posts.getLong("createDate"));
                            if (!posts.has("description")) {
                                item.setDescription("");
                            } else {
                                item.setDescription(posts.getString("description"));
                            }
                            if(!avatar.has("headers")){
                                item.setAvatar_hasHeader(false);
                            }else {
                                item.setAvatar_hasHeader(true);
                                JSONObject ava_headers = avatar.getJSONObject("headers");
                                item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                item.setAva_authorization(ava_headers.getString("Authorization"));
                                item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                item.setAva_Host(ava_headers.getString("Host"));
                            }
                            if(!content.has("headers")){
                                item.setHasHeader(false);
                            }else {
                                JSONObject header = content.getJSONObject("headers");
                                item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                item.setAuthorization(header.getString("Authorization"));
                                item.setX_amz_date(header.getString("x-amz-date"));
                                item.setHost(header.getString("Host"));
                                item.setHasHeader(true);
                            }
                            item.setUrl(content.getString("url"));
                            item.setAvatar_url(avatar.getString("url"));
                            item.setContentType(content.has("contentType")?content.getString("contentType"):"PICTURE");
                            item.setResourceId(content.getString("resourceId"));
                            item.setAva_resourceId(avatar.getString("resourceId"));
                            Log.d("DDDD0","RRRRR");
                            item.setScren_resourceId(screenshot.has("resourceId")?screenshot.getString("resourceId"):"no");
                            item.setScren_url(screenshot.has("url")?screenshot.getString("url"):"no");

                            if(!screenshot.has("headers")){
                                item.setScren_hasHeader(false);
                            }else {
                                JSONObject header = screenshot.getJSONObject("headers");
                                item.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                item.setScren_authorization(header.getString("Authorization"));
                                item.setScren_x_amz_date(header.getString("x-amz-date"));
                                item.setScren_Host(header.getString("Host"));
                                item.setScren_hasHeader(true);
                            }


                            feedItem.add(item);
                        }

                    }

                    SetContent(feedItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }});
    }

    private void SetContent(final ArrayList<FeedItem> feedItemm) {
        final FeedItem item = feedItemm.get(0);


//        userName.setText(item.getName());
        userName.setText(item.getLogin());
        description.setText(item.getDescription());



        if(item.getContentType().equals("PICTURE")){

            Map<String, String> headers_content = new HashMap<String, String>();
            if(item.getHasHeader()){
                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers_content.put("x-amz-date", item.getX_amz_date());
                headers_content.put("Host", item.getHost());
                headers_content.put("Authorization", item.getAuthorization());
            }else {
                headers_content.put("auth", MainActivity.token);
            }

            contents_image.setVisibility(View.VISIBLE);
            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_content)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();


            ImageLoader.getInstance().displayImage(item.getUrl(), contents_image, header_options);

        }else {

            Map<String, String> headers_screen = new HashMap<String, String>();
            if(item.getHasHeader()){
                headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                headers_screen.put("Host", item.getScren_Host());
                headers_screen.put("Authorization", item.getScren_authorization());
            }else {
                headers_screen.put("auth", MainActivity.token);
            }

            contents_image.setVisibility(View.VISIBLE);
            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_screen)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();


            ImageLoader.getInstance().displayImage(item.getScren_url(), contents_image, header_options);

        }




        Map<String, String> ava_headers = new HashMap<String, String>();
        if(item.getAvatar_hasHeader()){
            ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
            ava_headers.put("x-amz-date", item.getAva_x_amz_date());
            ava_headers.put("Host", item.getAva_Host());
            ava_headers.put("Authorization", item.getAva_authorization());
        }else {
            ava_headers.put("auth", MainActivity.token);
        }
        DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                .extraForDownloader(ava_headers)
                .showImageOnLoading(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader.getInstance().displayImage(item.getAvatar_url(),avatar, ava_options);

    }

    @OnClick(R.id.back)
    void backClik(){
        getActivity().onBackPressed();
    }

    @OnClick(R.id.done)
    void backDone(){


        RequestParams params = new RequestParams();
        params.put("postId", postId);
        params.put("description", description.getText().toString());

        Log.d("postId",postId );
        Log.d("description",description.getText().toString() );
        MainRestApi.post("updatePost",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                Log.d("updatePost postId",response+"**");
                getActivity().onBackPressed();

            }});
    }




}

