package com.bugingroup.mylistory.FRAGMENT.profile;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentIgnoredUser extends Fragment {


    @BindView(R.id.listView)
    ListView listView;

    private ListViewAdapter adapter;
    ArrayList<FeedItem> feedItem;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ignored_user, container, false);
        ButterKnife.bind(this,view);
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit","40");

        MainRestApi.get("ignoredUserList",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    feedItem = new ArrayList<FeedItem>();
                    if(response.getBoolean("success")){
                        Log.d("ignoredUserList",response+"*");
                        JSONObject body = response.getJSONObject("body");
                        if(body.has("list")) {
                            JSONArray list = body.getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject posts = (JSONObject) list.get(i);
                                FeedItem item = new FeedItem();
                                item.setUser_id(posts.has("id")?posts.getString("id"):"0");
                                item.setLogin(posts.has("login")?posts.getString("login"):"");
                                item.setName(posts.has("name")?posts.getString("name"):"");
                                item.setSurname(posts.has("surname")?posts.getString("surname"):"");
                                JSONObject jsonObject =new JSONObject("{\"avatar\":{ }}");
                                JSONObject avatar =posts.has("avatar")?posts.getJSONObject("avatar"):jsonObject.getJSONObject("avatar");
                                if (!avatar.has("headers")) {
                                    item.setAvatar_hasHeader(false);
                                } else {
                                    JSONObject headers = avatar.getJSONObject("headers");
                                    item.setAva_x_amz_content_sha256(headers.getString("x-amz-content-sha256"));
                                    item.setAva_authorization(headers.getString("Authorization"));
                                    item.setAva_x_amz_date(headers.getString("x-amz-date"));
                                    item.setAva_Host(headers.getString("Host"));
                                    item.setAvatar_hasHeader(true);
                                }

                                item.setAvatar_url(avatar.has("url")?avatar.getString("url"):"no");
                                item.setResourceId(avatar.has("resourceId")?avatar.getString("resourceId"):"no");
                                feedItem.add(item);

                            }
                            adapter = new ListViewAdapter(getActivity(), feedItem);
                            listView.setAdapter(adapter);
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});
        return view;
    }

    @OnClick(R.id.back)
    void BackClick(){
        getActivity().onBackPressed();
    }

    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }
        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.btn_false)
            ImageButton btn_false;



            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_ignored_user, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            item = feed_item.get(position);

//            viewHolder.user_name.setText(item.getName()+" "+item.getSurname());
            viewHolder.user_name.setText(item.getLogin());



            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.ava)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);


            viewHolder.btn_false.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    RequestParams params = new RequestParams();
                    params.put("ignoredUser", item.getUser_id());


                    MainRestApi.post("unignoreUser",params,new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                            Log.d("unignoreUser",response+"*");
                            try {
                                if(response.getBoolean("success")){
                                    feed_item.remove(position);
                                    notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }});

                }
            });



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            return convertView;
        }

    }
}