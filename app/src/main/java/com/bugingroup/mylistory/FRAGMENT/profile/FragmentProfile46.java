package com.bugingroup.mylistory.FRAGMENT.profile;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch25;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile46 extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.gridView)
    GridView gridView;
    @BindView(R.id.title_name)
    TextView title_name;
    private GridViewAdapter adapter;
    ArrayList<FeedItem> feedItem;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    String userId,userName;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile46, container, false);
        ButterKnife.bind(this,view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            userId = bundle.getString("userId");
            userName = bundle.getString("name");
            title_name.setText(userName+" ");
        }
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        findWithMe();
                                    }
                                }
        );


        return view;
    }

    private void findWithMe() {
        swipeRefreshLayout.setRefreshing(true);
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit","40");
        params.put("userId",userId);

        MainRestApi.get("findWithMe",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    feedItem = new ArrayList<FeedItem>();
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        if(body.has("list")) {
                            JSONArray list = body.getJSONArray("list");
                            if (list.length()!=0) {
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject posts = (JSONObject) list.get(i);
                                    if (posts.has("contents")) {
                                        JSONArray contents = posts.getJSONArray("contents");
                                        JSONObject content = (JSONObject) contents.get(0);
                                        FeedItem item = new FeedItem();
                                        item.setPost_id(posts.getString("postId"));
                                        if (!content.has("headers")) {
                                            item.setHasHeader(false);
                                        } else {
                                            JSONObject headers = content.getJSONObject("headers");
                                            item.setX_amz_content_sha256(headers.getString("x-amz-content-sha256"));
                                            item.setAuthorization(headers.getString("Authorization"));
                                            item.setX_amz_date(headers.getString("x-amz-date"));
                                            item.setHost(headers.getString("Host"));
                                            item.setHasHeader(true);
                                        }
                                        item.setUrl(content.getString("url"));
                                        item.setContentType(content.has("contentType") ? content.getString("contentType") : "PICTURE");
                                        item.setResourceId(content.getString("resourceId"));

                                        feedItem.add(item);

                                    }
                                }
                                adapter = new GridViewAdapter(getActivity(), feedItem);
                                gridView.setAdapter(adapter);
                                swipeRefreshLayout.setRefreshing(false);
                            }else {
                                swipeRefreshLayout.setRefreshing(false);
                            }

                        }else {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});

    }

    @Override
    public void onRefresh() {
        findWithMe();
    }


//  GridViewAdapter

    public class GridViewAdapter extends BaseAdapter {

        ArrayList<FeedItem>  feed_item;
        FeedItem item;
        Activity activity;

        public GridViewAdapter(Activity activity, ArrayList<FeedItem> feed_item) {
            this.feed_item = feed_item;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {

            @BindView(R.id.imageView)
            ImageView imageView;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home16_item_grid, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.imageView.setVisibility(View.VISIBLE);

            item = feed_item.get(position);

            Map<String, String> headers_content = new HashMap<String, String>();
            if(item.getHasHeader()){
                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers_content.put("x-amz-date", item.getX_amz_date());
                headers_content.put("Host", item.getHost());
                headers_content.put("Authorization", item.getAuthorization());
            }else {
                headers_content.put("auth", MainActivity.token);
            }
            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_content)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            if(item.getContentType().equals("PICTURE")) {
                ImageLoader.getInstance().displayImage(item.getUrl(), viewHolder.imageView, header_options, animateFirstListener);
            }else {
                viewHolder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_play));
            }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    Log.d("DDD123",item.getPost_id()+"***");
                    bundle.putString("postId", item.getPost_id());
                    Fragment fragment = new FragmentSearch25();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);

                }
            });

            return convertView;
        }
    }
}