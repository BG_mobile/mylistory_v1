package com.bugingroup.mylistory.FRAGMENT.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bugingroup.mylistory.ActivityTheme;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.start.SettingAvatar2;
import com.bugingroup.mylistory.start.Start2;
import com.bugingroup.mylistory.start.TakePhotoForAvatar;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
//import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sevenheaven.iosswitch.ShSwitchView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile47 extends Fragment {

    @BindView(R.id.switch_view2)
    ShSwitchView switch_view2;

    @BindView(R.id.close_account)
    ShSwitchView close_account;
    @BindView(R.id.invite_facebook) LinearLayout invite_facebook;
    @BindView(R.id.invite_vk) LinearLayout invite_vk;
    @BindView(R.id.setting_header) LinearLayout setting_header;
    @BindView(R.id.setting_theme) LinearLayout setting_theme;
    @BindView(R.id.edit_profile) LinearLayout edit_profile;
    @BindView(R.id.edit_password) LinearLayout edit_password;
    @BindView(R.id.ignored_user) LinearLayout ignored_user;
    @BindView(R.id.like_pub) LinearLayout like_pub;
    @BindView(R.id.linked_account) LinearLayout linked_account;
    @BindView(R.id.select_language) LinearLayout select_language;
    @BindView(R.id.data_phone) LinearLayout data_phone;
    @BindView(R.id.setting_push_notification) LinearLayout setting_push_notification;
    @BindView(R.id.help_center) LinearLayout help_center;
    @BindView(R.id.report_problem) LinearLayout report_problem;
    @BindView(R.id.advertising) LinearLayout advertising;
    @BindView(R.id.blog) LinearLayout blog;
    @BindView(R.id.privacy_policy) LinearLayout privacy_policy;
    @BindView(R.id.conditions) LinearLayout conditions;
    @BindView(R.id.open_source_gallery) LinearLayout open_source_gallery;
    @BindView(R.id.clear_search_history) LinearLayout clear_search_history;
    @BindView(R.id.add_account) LinearLayout add_account;
    @BindView(R.id.exit) LinearLayout exit;

    // SETTING
    static boolean closedProfile = false;
    static String likeNotification="ON" ,commentNotification="ON", locale="ru_RU";



    SharedPreferences preferences;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile47, container, false);
        ButterKnife.bind(this,view);

        loadSettings();
         preferences = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, Context.MODE_PRIVATE);
        if(preferences.getBoolean("save_image",false)){
            switch_view2.setOn(true);
        }

        invite_facebook.setOnClickListener(onSettingClickListener);
        invite_vk.setOnClickListener(onSettingClickListener);
        setting_header.setOnClickListener(onSettingClickListener);
        setting_theme.setOnClickListener(onSettingClickListener);
        edit_profile.setOnClickListener(onSettingClickListener);
        edit_password.setOnClickListener(onSettingClickListener);
        ignored_user.setOnClickListener(onSettingClickListener);
        like_pub.setOnClickListener(onSettingClickListener);
        linked_account.setOnClickListener(onSettingClickListener);
        select_language.setOnClickListener(onSettingClickListener);
        data_phone.setOnClickListener(onSettingClickListener);
        setting_push_notification.setOnClickListener(onSettingClickListener);
        help_center.setOnClickListener(onSettingClickListener);
        report_problem.setOnClickListener(onSettingClickListener);
        advertising.setOnClickListener(onSettingClickListener);
        blog.setOnClickListener(onSettingClickListener);
        privacy_policy.setOnClickListener(onSettingClickListener);
        conditions.setOnClickListener(onSettingClickListener);
        open_source_gallery.setOnClickListener(onSettingClickListener);
        clear_search_history.setOnClickListener(onSettingClickListener);
        add_account.setOnClickListener(onSettingClickListener);
        exit.setOnClickListener(onSettingClickListener);

        close_account.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
            @Override
            public void onSwitchStateChange(boolean isOn) {
                String body;
                if(isOn){
                    body = " {\"closedProfile\":\"true\", \"likeNotification\":\""+likeNotification+"\", \"commentNotification\":\""+commentNotification+"\", \"locale\":\""+locale+"\"} ";
                }else {
                    body = " {\"closedProfile\":\"false\", \"likeNotification\":\""+likeNotification+"\", \"commentNotification\":\""+commentNotification+"\", \"locale\":\""+locale+"\"} ";
                }
                MainRestApi.post(getActivity(),"saveSettings",body,new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            Log.d("ASASAS",response+"***");
                            if(response.getBoolean("success")){

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        switch_view2.setOnSwitchStateChangeListener(new ShSwitchView.OnSwitchStateChangeListener() {
            @Override
            public void onSwitchStateChange(boolean isOn) {
                SharedPreferences.Editor editor = preferences.edit();

                if (isOn) {
                    editor.putBoolean("save_image",true);
                } else {
                    editor.putBoolean("save_image",false);
                }
                editor.commit();
            }
        });

        return view;
    }

    private void loadSettings() {

        MainRestApi.get("loadSettings",null,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    Log.d("DDDDD",response+"**");
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        likeNotification = body.has("likeNotification")?body.getString("likeNotification"):"ON";
                        commentNotification = body.has("commentNotification")?body.getString("commentNotification"):"ON";
                        locale = body.getString("locale");
                        Log.d("SSSSSSEEEEEE",likeNotification+","+commentNotification+","+locale);
                       if(body.getBoolean("closedProfile")){
                           closedProfile = true;
                           close_account.setOn(true);
                       }else {
                           closedProfile = false;
                           close_account.setOn(false);
                       }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});
    }

    @OnClick(R.id.back)
    void BackClick(){
        getActivity().onBackPressed();
    }


    //  Setting Menu click
    View.OnClickListener onSettingClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.invite_facebook:
                    break;
                case R.id.invite_vk:
                    break;
                case R.id.setting_header:
//                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile55());

                    final Dialog dialog_ava = new Dialog(getActivity());
                    dialog_ava.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog_ava.setContentView(R.layout.setting_avatar_dialog);
                    TextView take_photo = (TextView) dialog_ava.findViewById(R.id.take_photo);
                    TextView from_gallerey = (TextView) dialog_ava.findViewById(R.id.from_gallerey);

                    take_photo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_ava.dismiss();
                            startActivity(new Intent(getActivity(),TakePhotoForAvatar.class));
                        }
                    });

                    from_gallerey.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_ava.dismiss();
                            startActivity(new Intent(getActivity(),SettingAvatar2.class));

                        }
                    });

                    Button cancle_ava = (Button) dialog_ava.findViewById(R.id.cancle);
                    cancle_ava.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_ava.dismiss();
                        }
                    });

                    dialog_ava.show();



                    break;
                case R.id.setting_theme:
                    startActivity(new Intent(getActivity(),ActivityTheme.class).putExtra("change",true));
                    break;
                case R.id.edit_profile:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile50());
                    break;
                case R.id.edit_password:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile54());
                    break;
                case R.id.ignored_user:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentIgnoredUser());
                    break;
                case R.id.like_pub:
                   ((MainActivity) getActivity()).replaceFragment(new FragmentProfile56());
                    break;
                case R.id.linked_account:
                   ((MainActivity) getActivity()).replaceFragment(new FragmentProfile57());
                    break;
                case R.id.select_language:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile58());
                    break;
                case R.id.data_phone:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile61());
                    break;
                case R.id.setting_push_notification:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile60());
                    break;
                case R.id.help_center:
                    break;
                case R.id.report_problem:
                    final Dialog dialogproblem = new Dialog(getActivity());
                    dialogproblem.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogproblem.setContentView(R.layout.dialog_report_problem);
                    dialogproblem.show();
                    final LinearLayout button_layout = (LinearLayout) dialogproblem.findViewById(R.id.button_layout);
                    final LinearLayout edit_layout = (LinearLayout) dialogproblem.findViewById(R.id.edit_layout);
                    button_layout.setVisibility(View.VISIBLE);
                    edit_layout.setVisibility(View.GONE);
                    TextView cancle_tv = (TextView) dialogproblem.findViewById(R.id.cancle);
                    cancle_tv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogproblem.dismiss();

                        }
                    });

                    TextView complaint = (TextView) dialogproblem.findViewById(R.id.complaint);
                    complaint.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            edit_layout.setVisibility(View.VISIBLE);
                            button_layout.setVisibility(View.GONE);
                        }
                    });

                    TextView report = (TextView) dialogproblem.findViewById(R.id.report);
                    report.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            edit_layout.setVisibility(View.VISIBLE);
                            button_layout.setVisibility(View.GONE);
                        }
                    });

                    Button cancle_btn = (Button) dialogproblem.findViewById(R.id.cancle_btn);
                    cancle_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogproblem.dismiss();

                        }
                    });

                    final EditText editText = (EditText) dialogproblem.findViewById(R.id.editText);
                    editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if(hasFocus){
                                editText.setHint("");
                            }
                        }
                    });
                    Button ok_btn = (Button) dialogproblem.findViewById(R.id.ok);
                    ok_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogproblem.dismiss();

                        }
                    });



                    break;
                case R.id.advertising:
                    break;
                case R.id.blog:

                    break;
                case R.id.privacy_policy:
                    break;
                case R.id.conditions:
                    break;
                case R.id.open_source_gallery:
                    break;
                case R.id.clear_search_history:
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.profile62_clear_search_dialog);
                    Button cancle = (Button) dialog.findViewById(R.id.cancle);
                    cancle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            getActivity().onBackPressed();
                        }
                    });
                    Button ok = (Button) dialog.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            getActivity().onBackPressed();
                        }
                    });
                    dialog.show();
                    break;
                case R.id.add_account:
                    SharedPreferences prefs2 = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, getActivity().MODE_PRIVATE);
                    if(!prefs2.contains("token")){
                        startActivity(new Intent(getActivity(),Start2.class));
                    }else {

                    }
                    break;
                case R.id.exit:
                    final Dialog dialogExit = new Dialog(getActivity());
                    dialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogExit.setContentView(R.layout.profile62_exit_dialog);
                    Button exitcancle = (Button) dialogExit.findViewById(R.id.cancle);
                    exitcancle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogExit.dismiss();
                        }
                    });
                    Button exitok = (Button) dialogExit.findViewById(R.id.ok);
                    exitok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogExit.dismiss();
                            AccessToken accessToken = AccessToken.getCurrentAccessToken();
                            if (accessToken != null) {
                                LoginManager.getInstance().logOut();
                            }
                            SharedPreferences preferences = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.commit();
                            Log.d("ASAS","ASAS");
                            SharedPreferences preferences2 = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor2 = preferences2.edit();
                            editor2.clear();
                            editor2.commit();

//                            String reg_id = FirebaseInstanceId.getInstance().getToken();
                            RequestParams params = new RequestParams();
//                            params.put("registrationId", reg_id);
                            MainRestApi.post("removeDevice",params,new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                    Log.d("DOMAIN_ISRRR",response+"*");
                                    try {
                                        if(response.getBoolean("success")){
                                            Log.d("DOMAIN_ISEEEE","success*");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                    super.onFailure(statusCode, headers, responseString, throwable);
                                    Log.d("DOMAIN_ISEEEE",responseString+"*");
                                }
                            });
                            startActivity(new Intent(getActivity(), Start2.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                        }
                    });
                    dialogExit.show();

                    break;
            }
        }
    };


}

