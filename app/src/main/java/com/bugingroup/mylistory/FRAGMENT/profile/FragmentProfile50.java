package com.bugingroup.mylistory.FRAGMENT.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yixia.camera.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.util.TextUtils;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile50 extends Fragment {

    @BindView(R.id.user_name)
    EditText user_name;

    @BindView(R.id.nick)
    EditText nick;

    @BindView(R.id.siteUrl)
    EditText siteUrl;

    @BindView(R.id.info)
    EditText info;

    ImageButton back;
    TextView done;
    private Spinner gender;
    LinearLayout email_address,phone_number;

   static String ID,SURNAME,NAME,INFO,NICK,SITEURL,PHONE,EMAIL,SEX;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile50, container, false);
        ButterKnife.bind(this,view);
        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);

        gender = (Spinner) view.findViewById(R.id.gender);
        SpinnerAdapter adapter = new SpinnerAdapter(getResources().getStringArray(R.array.genderArray));
        gender.setAdapter(adapter);

        email_address = (LinearLayout) view.findViewById(R.id.email_address);
        email_address.setOnClickListener(onSettingClickListener);
        phone_number = (LinearLayout) view.findViewById(R.id.phone_number);
        phone_number.setOnClickListener(onSettingClickListener);
        ProfileGetserver();
        return view;
    }


    private void ProfileGetserver() {
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit","40");

        MainRestApi.get("profile",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        ID = body.has("id")?body.getString("id"):"";
                        SURNAME = body.has("surname")?body.getString("surname"):"";
                        NAME = body.has("name")?body.getString("name"):"";
                        INFO = body.has("info")?body.getString("info"):"";
                        NICK = body.has("login")?body.getString("login"):"";
                        SITEURL = body.has("siteUrl")?body.getString("siteUrl"):"";
                        PHONE = body.has("phone")?body.getString("phone"):"";
                        EMAIL = body.has("email")?body.getString("email"):"";
                        SEX = body.has("sex")?body.getString("sex"):"";
                        TextTOTextView();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});


    }

    private void TextTOTextView() {
        user_name.setText(SURNAME+" "+NAME);
        nick.setText(NICK);
        info.setText(INFO);
        siteUrl.setText(SITEURL);
    }


    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            switch (v.getId()){
                case R.id.back:
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                    getActivity().onBackPressed();
                    break;
                case R.id.done:

                    if(isValid(nick.getText().toString())){
                        checkLogin();
                    }else {
                        Toast.makeText(getActivity(), "В именах пользователей можно использовать только буквы, цифры, символы подчеркивания и точко", Toast.LENGTH_SHORT).show();
                    }

                    break;
            }
        }
    };

    public void checkLogin(){

        String n = nick.getText().toString();
        if(!TextUtils.isEmpty(nick.getText().toString())) {
            Log.d("NNNNNNNN",nick.getText().toString()+"*"+NICK);
            if(nick.getText().toString().equals(NICK)){
                String Name = "";
                String Surname = "";
                if(!TextUtils.isEmpty(user_name.getText().toString()) && !user_name.getText().toString().equals(" ")) {
                    String[] splited = user_name.getText().toString().split("\\s+");
                    Surname = splited[0];
                    if (splited.length > 1) {
                        Name = splited[1];
                    }
                }

                String body = "{\"id\":\""+ID+"\"";
                if(!Name.equals(NAME))
                    body = body+",\"name\":\""+Name+"\"";
                if(!Surname.equals(SURNAME))
                    body = body+",\"surname\":\""+Surname+"\"";
                if(PHONE.length()>0)
                    body = body+",\"phone\":\""+PHONE+"\"";
                if(EMAIL.length()>0)
                    body = body+",\"email\":\""+EMAIL+"\"";

                if(!nick.getText().toString().equals(NICK))
                    body = body+",\"login\":\""+nick.getText().toString()+"\"";
                if(!info.getText().toString().equals(INFO))
                    body = body+",\"info\":\""+info.getText().toString()+"\"";
                if(!siteUrl.getText().toString().equals(SITEURL))
                    body = body+",\"siteUrl\":\""+siteUrl.getText().toString()+"\"";
                body = body+",\"sex\":\""+SEX+"\"";
                body = body+"}\n";

                MainRestApi.post(getActivity(),"updateUser",body,new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                        getActivity().onBackPressed();
                    }});
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }else {
                RestApi.get("loginRegistration/"+n,null,new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {

                        Log.d("RRr",response+"**");
                        try {
                            if(response.getBoolean("success") ){
                                if(!response.getBoolean("body")) {
                                    String[] splited = user_name.getText().toString().split("\\s+");
                                    String Name = "";
                                    String Surname = "";
                                    Surname = splited[0];
                                    if(splited.length>1) {
                                        Name = splited[1];
                                    }

                                    String body = "{\"id\":\""+ID+"\"";
                                    if(!Name.equals(NAME))
                                        body = body+",\"name\":\""+Name+"\"";
                                    if(!Surname.equals(SURNAME))
                                        body = body+",\"surname\":\""+Surname+"\"";
                                    if(PHONE.length()>0)
                                        body = body+",\"phone\":\""+PHONE+"\"";
                                    if(EMAIL.length()>0)
                                        body = body+",\"email\":\""+EMAIL+"\"";

                                    if(!nick.getText().toString().equals(NICK))
                                        body = body+",\"login\":\""+nick.getText().toString()+"\"";
                                    if(!info.getText().toString().equals(INFO))
                                        body = body+",\"info\":\""+info.getText().toString()+"\"";
                                    if(!siteUrl.getText().toString().equals(SITEURL))
                                        body = body+",\"siteUrl\":\""+siteUrl.getText().toString()+"\"";
                                    body = body+",\"sex\":\""+SEX+"\"";
                                    body = body+"}\n";

                                    MainRestApi.post(getActivity(),"updateUser",body,new JsonHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                            getActivity().onBackPressed();
                                        }});
                                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                                }else {
                                    Toast.makeText(getActivity(), getString(R.string.login_is_already_registered), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(getActivity(), "Сервер не доступен", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }});
            }




        }else {
            Toast.makeText(getActivity(), getString(R.string.enter_a_login), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValid(CharSequence s) {
        Pattern sPattern = Pattern.compile("^[a-zA-Z0-9_]*$");
        return sPattern.matcher(s).matches();
    }

    //  Setting Menu click
    View.OnClickListener onSettingClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.email_address:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile51());
                    break;
                case R.id.phone_number:
                    ((MainActivity) getActivity()).replaceFragment(new FragmentProfile52());
                    break;

            }
        }
    };


    //  SpinnerAdapter
    private class SpinnerAdapter extends BaseAdapter {

        private String[]  array;

        public SpinnerAdapter(String[] array) {
            this.array = array;
        }

        class ViewHolder {
            TextView spinner_tv;
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final SpinnerAdapter.ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.view_spinner_item, viewGroup, false);
                viewHolder = new SpinnerAdapter.ViewHolder();
                convertView.setTag(viewHolder);
                viewHolder.spinner_tv = (TextView) convertView.findViewById(R.id.spinner_tv);
                viewHolder.spinner_tv.setText(array[position]);

            } else {
                viewHolder = (SpinnerAdapter.ViewHolder) convertView.getTag();
            }



            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return array.length;
        }
    }

}

