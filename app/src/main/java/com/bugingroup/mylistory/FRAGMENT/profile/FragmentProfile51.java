package com.bugingroup.mylistory.FRAGMENT.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile51 extends Fragment {

    private EditText email_address;
    ImageButton back;
    TextView done;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile51, container, false);

        email_address = (EditText) view.findViewById(R.id.email_address);
        email_address.setText(FragmentProfile50.EMAIL);
        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);

        return view;
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.done:
                    if(isValidEmail(email_address.getText().toString())) {
                        getActivity().onBackPressed();
                    }else {
                        Toast.makeText(getActivity(), getString(R.string.enter_a_valid_email_address), Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}

