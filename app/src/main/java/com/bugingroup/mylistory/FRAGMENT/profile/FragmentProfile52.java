package com.bugingroup.mylistory.FRAGMENT.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile52 extends Fragment {
    private EditText phone_number;
    ImageButton back;
    TextView done;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile52, container, false);

        phone_number = (EditText) view.findViewById(R.id.phone_number);
        phone_number.setText(FragmentProfile50.PHONE);
        phone_number.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==3 && before ==0){
                    phone_number.append(" ");
                }if(s.length()==7 && before ==0){
                    phone_number.append(" ");
                }if(s.length()==10 && before ==0){
                    phone_number.append(" ");
                }
            }
        });
        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);

        return view;
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.done:
                    String PhoneNo = phone_number.getText().toString();
                    String Regex = "[^\\d]";
                    String PhoneDigits = PhoneNo.replaceAll(Regex, "");
                    if(PhoneDigits.length()==10) {
                        getActivity().onBackPressed();
                    }else {
                        Toast.makeText(getActivity(), getString(R.string.enter_a_valid_phone_number), Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

}

