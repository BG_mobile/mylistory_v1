package com.bugingroup.mylistory.FRAGMENT.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.start.Start2;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile54 extends Fragment {

    @BindView(R.id.old_password)
    EditText old_password;
    @BindView(R.id.new_password)
    EditText new_password;
    @BindView(R.id.new_password2)
    EditText new_password2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile54, container, false);
        ButterKnife.bind(this,view);



        return view;
    }


    @OnClick(R.id.back)
    void BackClick(){
        getActivity().onBackPressed();
    }
    @OnClick(R.id.done)
    void DoneClick(){
    if(!TextUtils.isEmpty(old_password.getText().toString()) && !TextUtils.isEmpty(new_password.getText().toString())  && !TextUtils.isEmpty(new_password2.getText().toString()) ){
        if(TextUtils.equals(new_password.getText().toString(),new_password2.getText().toString())){
            if(new_password.getText().toString().length()>=6){
                RequestParams params = new RequestParams();
                params.put("oldPassword", old_password.getText().toString());
                params.put("newPassword",new_password.getText().toString());

                Log.d("oldPassword", old_password.getText().toString());
                Log.d("newPassword",new_password.getText().toString());

                MainRestApi.post("changePassword",params,new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                        Log.d("CHANGE PASS",response+"**");
                        try {
                            if(response.getBoolean("success")){

                                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                                if (accessToken != null) {
                                    LoginManager.getInstance().logOut();
                                }
                                SharedPreferences preferences = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.commit();
                                startActivity(new Intent(getActivity(), Start2.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                            }else {
                                Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }});
                    } else {
            Toast.makeText(getActivity(), getString(R.string.passwords_must_contain_at_least_6_characters), Toast.LENGTH_SHORT).show();
        }
        }else {
            Toast.makeText(getActivity(), "Подтверждение пароля не соответствует оригиналу66", Toast.LENGTH_SHORT).show();
        }
    }else {
        Toast.makeText(getActivity(), "Пожалуйста, введите Ваш пароль66", Toast.LENGTH_SHORT).show();
    }
    }

    public boolean isMatchPassword(EditText et1,EditText et2){
        return et1.getText().toString().equals(et2.getText().toString());
    }


}

