package com.bugingroup.mylistory.FRAGMENT.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.AWS.ExampleMethod;
import com.bugingroup.mylistory.AWS.auth.AWS4SignerBase;
import com.bugingroup.mylistory.AWS.util.BinaryUtils;
import com.bugingroup.mylistory.ImagePicker.commons.models.Session;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.start.SettingAvatar;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile55 extends Fragment {

    ImageButton back;
    TextView done;

    @BindView(R.id.avatar)
    ImageView avatar_iv;

    @BindView(R.id.avatar1)
    CircleImageView avatar_1;

    @BindView(R.id.avatar2)
    CircleImageView avatar_2;

    @BindView(R.id.avatar3)
    CircleImageView avatar_3;

    private Session mSession = Session.getInstance();

    ProgressDialog progress;
    Boolean setPhoto =false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile55, container, false);
        ButterKnife.bind(this,view);
        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);


        ProfileGetserver();

        return view;
    }

    @OnClick(R.id.avatar)
    public void onClick(View v){
        startActivityForResult(new Intent(getActivity(), SettingAvatar.class),704);
    }

    private void ProfileGetserver() {


        MainRestApi.get("profile",null,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        if(body.has("avatar")) {
                            final JSONObject avatar = body.getJSONObject("avatar");
                            Map<String, String> ava_headers = new HashMap<String, String>();
                            if (!avatar.has("headers")) {
                                ava_headers.put("auth", MainActivity.token);
                            } else {
                                JSONObject headers = avatar.getJSONObject("headers");
                                ava_headers.put("x-amz-content-sha256", headers.getString("x-amz-content-sha256"));
                                ava_headers.put("Authorization", headers.getString("Authorization"));
                                ava_headers.put("x-amz-date", headers.getString("x-amz-date"));
                                ava_headers.put("Host", headers.getString("Host"));
                            }
                            if( avatar.has("resourceId") && avatar.has("url")) {
                                DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                                        .extraForDownloader(ava_headers)
                                        .showImageOnLoading(R.drawable.loading)
                                        .cacheInMemory(true)
                                        .cacheOnDisk(true)
                                        .considerExifParams(true)
                                        .build();

                                ImageLoader.getInstance().displayImage(avatar.getString("url"), avatar_iv, ava_options);
                                ImageLoader.getInstance().displayImage(avatar.getString("url"), avatar_1, ava_options);
                                ImageLoader.getInstance().displayImage(avatar.getString("url"), avatar_2, ava_options);
                                ImageLoader.getInstance().displayImage(avatar.getString("url"), avatar_3, ava_options);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.done:
                    if(setPhoto) {
                        progress = ProgressDialog.show(getActivity(), "Загрузить...",
                                "", true);
                        InputStream iStream = null;
                        try {
                            iStream = getActivity().getContentResolver().openInputStream(Uri.fromFile(mSession.getFileToUpload()));
                            byte[] inputData = getBytes(iStream);
                            PostTOAWS(inputData);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 704 && resultCode == Activity.RESULT_OK) {
            setPhoto = true;
            Picasso.with(getActivity()).load(Uri.fromFile(mSession.getFileToUpload()))
                    .noFade()
                    .noPlaceholder()
                    .into(avatar_iv);
            Picasso.with(getActivity()).load(Uri.fromFile(mSession.getFileToUpload()))
                    .noFade()
                    .noPlaceholder()
                    .into(avatar_1);
            Picasso.with(getActivity()).load(Uri.fromFile(mSession.getFileToUpload()))
                    .noFade()
                    .noPlaceholder()
                    .into(avatar_2);
            Picasso.with(getActivity()).load(Uri.fromFile(mSession.getFileToUpload()))
                    .noFade()
                    .noPlaceholder()
                    .into(avatar_3);
        }
    }


    private void PostTOAWS(final byte[] inputData) {
        byte[] contentHash = AWS4SignerBase.hash(inputData);
        final String contentHashString = BinaryUtils.toHex(contentHash);


        String body = "{\"contentHashString\":"+contentHashString+"," +
                "\"contentLength\":"+inputData.length+"}";
        Log.d("ASASAS","FDFD"+body);


        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);

            StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
            client.post(getActivity(),URL_CONSTANT.AwsToken,entity,"application/json",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        Log.d("JSONOBJECT1:",response+"");
                        String authorization = response.getString("authorization");
                        final String resourceId = response.getString("resourceId");
                        final String x_amz_date = response.getString("x-amz-date");
                        final String Host = response.getString("Host");
                        ExampleMethod.test(resourceId,authorization,inputData,x_amz_date,Host);
                        UpdateAvatar(resourceId);
                    }else {
                        progress.dismiss();
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});




    }

    private void UpdateAvatar(final String resourceId) {
        String body = "{\"resourceId\":\""+resourceId+"\"}";

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);

            StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
            client.post(getActivity(), URL_CONSTANT.DOMAIN + "UpdateAvatar", entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        Log.d("JSONOBJECT2:", response + "");
                        if (response.getBoolean("success")) {
                            getActivity().onBackPressed();
                        } else {
                            Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


            progress.dismiss();

    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
}

