package com.bugingroup.mylistory.FRAGMENT.profile;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;

import com.bugingroup.mylistory.R;

import in.srain.cube.views.GridViewWithHeaderAndFooter;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile56 extends Fragment {

    ImageButton back;
    private GridViewWithHeaderAndFooter gridView;
    private GridViewAdapter adapter;
    ImageButton btn_list, btn_grid;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile56, container, false);

        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);

        gridView = (GridViewWithHeaderAndFooter) view.findViewById(R.id.gridView);
        gridView.setNumColumns(1);
        gridView.setVerticalSpacing(0);
        adapter = new GridViewAdapter(5);
        btn_list = (ImageButton) view.findViewById(R.id.btn_list);
        btn_grid = (ImageButton) view.findViewById(R.id.btn_grid);
        btn_list.setOnClickListener(OnImageButtonclickListener);
        btn_grid.setOnClickListener(OnImageButtonclickListener);
        gridView.setAdapter(adapter);
        btn_list.setBackground(getResources().getDrawable(R.color.whiter));
        btn_grid.setBackground(getResources().getDrawable(R.color.transparent));

        return view;
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.btn_list:
                    gridView.setNumColumns(1);
                    gridView.setVerticalSpacing(0);
                    btn_list.setBackground(getResources().getDrawable(R.color.whiter));
                    btn_grid.setBackground(getResources().getDrawable(R.color.transparent));
                    adapter.notifyDataSetChanged();
                    break;
                case R.id.btn_grid:
                    gridView.setNumColumns(3);
                    gridView.setHorizontalSpacing(4);
                    gridView.setVerticalSpacing(4);
                    btn_list.setBackground(getResources().getDrawable(R.color.transparent));
                    btn_grid.setBackground(getResources().getDrawable(R.color.whiter));
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };


    //  GridViewAdapter
    private class GridViewAdapter extends BaseAdapter {

        private int  count;

        public GridViewAdapter(int count) {
            this.count = count;
        }

        class ViewHolder {
            ImageButton my_menu;

        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final GridViewAdapter.ViewHolder viewHolder;

            if (convertView == null){
                if(gridView.getNumColumns()==1) {
                    LayoutInflater inflater = (LayoutInflater) getActivity()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.profile43_item_list, viewGroup, false);
                    viewHolder = new GridViewAdapter.ViewHolder();
                    viewHolder.my_menu = (ImageButton) convertView.findViewById(R.id.show_menu);
                    convertView.setTag(viewHolder);

                    viewHolder.my_menu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Dialog dialog = new Dialog(getActivity());
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.profile44_menu_dialog);
                            dialog.show();
                        }
                    });

                }else {
                    LayoutInflater inflater = (LayoutInflater) getActivity()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.home16_item_grid, viewGroup, false);
                    viewHolder = new GridViewAdapter.ViewHolder();
                    convertView.setTag(viewHolder);
                }

            } else {
                viewHolder = (GridViewAdapter.ViewHolder) convertView.getTag();
            }



            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return count;
        }
    }


}

