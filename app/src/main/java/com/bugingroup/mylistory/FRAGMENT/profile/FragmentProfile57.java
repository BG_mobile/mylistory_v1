package com.bugingroup.mylistory.FRAGMENT.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile57 extends Fragment {

    ImageButton back;
    TextView done;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile57, container, false);

        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);

        return view;
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.done:
                    getActivity().onBackPressed();
                    break;
            }
        }
    };

}

