package com.bugingroup.mylistory.FRAGMENT.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.start.Start1;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.closedProfile;
import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.commentNotification;
import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.likeNotification;
import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.locale;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile58 extends Fragment {

    private ListView listView;
    private ListViewAdapter adapter;
    private int selectedPosition = 0;
    ImageButton back;
    TextView done;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile58, container, false);
        SharedPreferences prefs = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, getActivity().MODE_PRIVATE);
        if(prefs.getString("lang","ru").equals("en")) selectedPosition=1;

        listView = (ListView) view.findViewById(R.id.listView);
        adapter = new ListViewAdapter(getResources().getStringArray(R.array.language),getResources().getStringArray(R.array.language_name));
        listView.setAdapter(adapter);
        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);

        return view;
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.done:
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.profile59_menu_dialog);
                    Button cancle = (Button) dialog.findViewById(R.id.cancle);
                    cancle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getActivity().onBackPressed();
                            dialog.dismiss();
                        }
                    });
                    Button ok = (Button) dialog.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences.Editor editor = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, getActivity().MODE_PRIVATE).edit();
                            dialog.dismiss();
                            String locale="ru_RU";
                            switch (selectedPosition){
                                case 0:
                                    locale="ru_RU";
                                    editor.putString("lang", "ru");
                                    break;
                                case 1:
                                    locale="en_EN";
                                    editor.putString("lang","en");
                                    break;
                            }
                            String body = " {\"closedProfile\":\""+closedProfile+"\", \"likeNotification\":\""+likeNotification+"\", \"commentNotification\":\""+commentNotification+"\", \"locale\":\""+locale+"\"} ";

                            MainRestApi.post(getActivity(),"saveSettings",body,new JsonHttpResponseHandler(){
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    try {
                                        Log.d("ASASAS",response+"***");
                                        if(response.getBoolean("success")){

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            editor.commit();
                            Intent intent = new Intent(getActivity(), Start1.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);

                        }
                    });
                    dialog.show();
                    break;
            }
        }
    };


    //  ListViewAdapter
    private class ListViewAdapter extends BaseAdapter {

        String[] language, language_name;


        public ListViewAdapter(String[] language, String[] language_name) {
            this.language = language;
            this.language_name = language_name;
        }

        class ViewHolder {
            TextView lang_tv,name_tv;
            CheckBox checkBox;

        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ListViewAdapter.ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.profile58_item_list, viewGroup, false);
                viewHolder = new ListViewAdapter.ViewHolder();
                convertView.setTag(viewHolder);

                viewHolder.lang_tv   = (TextView) convertView.findViewById(R.id.language);
                viewHolder.name_tv   = (TextView) convertView.findViewById(R.id.language_name);
                viewHolder.checkBox  = (CheckBox) convertView.findViewById(R.id.checkBox);
                viewHolder.checkBox.setTag(position); // This line is important.

                viewHolder.lang_tv.setText(language[position]);
                viewHolder.name_tv.setText(language_name[position]);

            } else {
                viewHolder = (ListViewAdapter.ViewHolder) convertView.getTag();
            }


            if (position == selectedPosition) {
                viewHolder.checkBox.setChecked(true);
            } else viewHolder.checkBox.setChecked(false);



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    notifyDataSetChanged();

                }
            });

            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return language.length;
        }
    }
}
