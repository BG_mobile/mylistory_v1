package com.bugingroup.mylistory.FRAGMENT.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindArray;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.closedProfile;
import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.commentNotification;
import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.likeNotification;
import static com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile47.locale;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentProfile60 extends Fragment {

    private ListView listView_like,listView_comment;
    private ListViewAdapter adapter;
    private ListViewAdapterComment adapter_comment;

    @BindArray(R.array.genderArray)
            String [] a;
    ImageButton back;
    TextView done;
     int like = 0,comment=0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile60, container, false);

        switch (likeNotification){
            case "OFF":like = 0; break;
            case "ON_FOLLOWED": like = 1;break;
            case "ON": like = 2;break;
        }
        switch (commentNotification){
            case "OFF":comment = 0; break;
            case "ON_FOLLOWED": comment = 1;break;
            case "ON": comment = 2;break;
        }

        listView_like = (ListView) view.findViewById(R.id.listView_like);
        listView_comment = (ListView) view.findViewById(R.id.listView_comment);
        adapter = new ListViewAdapter(getResources().getStringArray(R.array.like_array));
        adapter_comment = new ListViewAdapterComment(getResources().getStringArray(R.array.like_array));

        listView_like.setAdapter(adapter);
        listView_comment.setAdapter(adapter_comment);

        back  = (ImageButton) view.findViewById(R.id.back);
        back.setOnClickListener(OnImageButtonclickListener);
        done  = (TextView) view.findViewById(R.id.done);
        done.setOnClickListener(OnImageButtonclickListener);

        return view;
    }
    View.OnClickListener OnImageButtonclickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back:
                    getActivity().onBackPressed();
                    break;
                case R.id.done:
                    String likeNotification = "ON";
                    switch (like){
                        case 0:likeNotification = "OFF"; break;
                        case 1:likeNotification = "ON_FOLLOWED"; break;
                        case 2:likeNotification = "ON"; break;
                    }
                    String commentNotification = "ON";
                    switch (comment){
                        case 0:commentNotification = "OFF"; break;
                        case 1:commentNotification = "ON_FOLLOWED"; break;
                        case 2:commentNotification = "ON"; break;
                    }
                    String body = " {\"closedProfile\":\""+closedProfile+"\", \"likeNotification\":\""+likeNotification+"\", \"commentNotification\":\""+commentNotification+"\", \"locale\":\""+locale+"\"} ";

                    MainRestApi.post(getActivity(),"saveSettings",body,new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                Log.d("ASASAS",response+"***");
                                if(response.getBoolean("success")){

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    getActivity().onBackPressed();
                    break;
            }
        }
    };



    //  ListViewAdapter
    private class ListViewAdapter extends BaseAdapter {

        String[] like_array;
        int selectedPosition = like;

        public ListViewAdapter(String[] like_array ) {
            this.like_array = like_array;
        }

        class ViewHolder {
            TextView name;
            CheckBox checkBox;

        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ListViewAdapter.ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.profile60_item_list, viewGroup, false);
                viewHolder = new ListViewAdapter.ViewHolder();
                convertView.setTag(viewHolder);

                viewHolder.name   = (TextView) convertView.findViewById(R.id.name);
                viewHolder.checkBox  = (CheckBox) convertView.findViewById(R.id.checkBox);
                viewHolder.checkBox.setTag(position); // This line is important.

                viewHolder.name.setText(like_array[position]);

            } else {
                viewHolder = (ListViewAdapter.ViewHolder) convertView.getTag();
            }


            if (position == selectedPosition) {
                viewHolder.checkBox.setChecked(true);
            } else viewHolder.checkBox.setChecked(false);



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    like = position;
                    notifyDataSetChanged();

                }
            });

            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return like_array.length;
        }
    }


    //  ListViewAdapterComment
    private class ListViewAdapterComment extends BaseAdapter {

        String[] like_array;
        int selectedPosition = comment;

        public ListViewAdapterComment(String[] like_array ) {
            this.like_array = like_array;
        }

        class ViewHolder {
            TextView name;
            CheckBox checkBox;

        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ListViewAdapterComment.ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.profile60_item_list, viewGroup, false);
                viewHolder = new ListViewAdapterComment.ViewHolder();
                convertView.setTag(viewHolder);

                viewHolder.name   = (TextView) convertView.findViewById(R.id.name);
                viewHolder.checkBox  = (CheckBox) convertView.findViewById(R.id.checkBox);
                viewHolder.checkBox.setTag(position); // This line is important.

                viewHolder.name.setText(like_array[position]);

            } else {
                viewHolder = (ListViewAdapterComment.ViewHolder) convertView.getTag();
            }


            if (position == selectedPosition) {
                viewHolder.checkBox.setChecked(true);
            } else viewHolder.checkBox.setChecked(false);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    comment = position;
                    notifyDataSetChanged();

                }
            });

            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return like_array.length;
        }
    }
}

