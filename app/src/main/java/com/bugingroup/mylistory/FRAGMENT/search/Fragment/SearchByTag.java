package com.bugingroup.mylistory.FRAGMENT.search.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch25;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 29.01.2017.
 */

    public class SearchByTag extends Fragment {

    @BindView(R.id.gridView)
    GridView gridView;

    @BindView(R.id.tag_title)
    TextView tag_title;
    private GridViewAdapter adapter;
    ArrayList<FeedItem> feedItem;
    String hash_tag=" ";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_tag, container, false);
        ButterKnife.bind(this,view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            hash_tag=bundle.getString("hash_tag");
                    tag_title.setText("# "+hash_tag);
        }
        findContens(hash_tag);
        return  view;
    }

    private void findContens(String hash_tag) {
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "40");
        params.put("hashtags", hash_tag);
        MainRestApi.get("findContens",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject content = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                    item.setPost_id(content.getString("postId"));
                                    if(!content.has("headers")){
                                        item.setHasHeader(false);
                                    }else {
                                        JSONObject header = content.getJSONObject("headers");
                                        item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                        item.setAuthorization(header.getString("Authorization"));
                                        item.setX_amz_date(header.getString("x-amz-date"));
                                        item.setHost(header.getString("Host"));
                                        item.setHasHeader(true);
                                    }
                                    item.setUrl(content.has("url")?content.getString("url"):"no");
                                    item.setResourceId(content.has("resourceId")?content.getString("resourceId"):"no");
                                    feedItem.add(item);
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                    adapter = new GridViewAdapter(getActivity(),feedItem);
                    gridView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
            }
        });
    }

    @OnClick(R.id.back)
    void click(){
        getActivity().onBackPressed();
    }

    public class GridViewAdapter extends BaseAdapter {
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        ArrayList<FeedItem>  feed_item;
        FeedItem item;
        Activity activity;

        public GridViewAdapter(Activity activity, ArrayList<FeedItem> feed_item) {
            this.feed_item = feed_item;
            this.activity = activity;
        }
        @Override
        public int getCount() {
            return feed_item.size();
        }
        @Override
        public Object getItem(int location) {
            return null;
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        class ViewHolder {
            @BindView(R.id.imageView)
            ImageView imageView;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home16_item_grid, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.imageView.setVisibility(View.VISIBLE);
            item = feed_item.get(position);
            Map<String, String> headers_content = new HashMap<String, String>();
            if(item.getHasHeader()){
                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers_content.put("x-amz-date", item.getX_amz_date());
                headers_content.put("Host", item.getHost());
                headers_content.put("Authorization", item.getAuthorization());
            }else {
                headers_content.put("auth", MainActivity.token);
            }
            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_content)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();
            ImageLoader.getInstance().displayImage(item.getUrl(), viewHolder.imageView, header_options, animateFirstListener);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        item = feed_item.get(position);
                        String postId = item.getPost_id();
                        Bundle bundle = new Bundle();
                        bundle.putString("postId", postId);
                        Fragment fragment = new FragmentSearch25();
                        fragment.setArguments(bundle);
                        ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });
            return convertView;
        }


    }

}

