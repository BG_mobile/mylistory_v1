package com.bugingroup.mylistory.FRAGMENT.search.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome16;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.util.TextUtils;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 29.01.2017.
 */

    public class TabFragmentBest extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.listView)
    ListView listView;

    private ListViewAdapter adapter;
    ArrayList<FeedItem> feedItem;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    public TabFragmentBest() {

    }
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        ButterKnife.bind(this,view);
       return  view;
    }
    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        findUsers();
                                    }
                                }
        );



    }

    private void findUsers() {
        final String url;
        swipeRefreshLayout.setRefreshing(true);
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "100");
        if(!FragmentSearch.search_text.equals("null") && !TextUtils.isEmpty(FragmentSearch.search_text)) {
            url="findUsers";
            params.put("searchText", FragmentSearch.search_text);
        }else {
            url="popularUsers";
        }
        Log.d("FRAGMENTTT", FragmentSearch.search_text+"AAA");



        MainRestApi.get(url,params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                feedItem = new ArrayList<FeedItem>();
                Log.d("RESRESRESRES",response+"+++"+url);
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                    item.setUser_id(jsonObject.getString("id"));
                                    item.setLogin(jsonObject.has("login")?jsonObject.getString("login"):" ");
                                    item.setName(jsonObject.has("name")?jsonObject.getString("name"):" ");
                                    item.setSurname(jsonObject.has("surname")?jsonObject.getString("surname"):" ");
                                    JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                    JSONObject avatar = jsonObject.has("avatar") ? jsonObject.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                    if (!avatar.has("headers")) {
                                        item.setAvatar_hasHeader(false);
                                    } else {
                                        item.setAvatar_hasHeader(true);
                                        JSONObject ava_headers = avatar.getJSONObject("headers");
                                        item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                        item.setAva_authorization(ava_headers.getString("Authorization"));
                                        item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                        item.setAva_Host(ava_headers.getString("Host"));
                                    }
                                    item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                    item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                    feedItem.add(item);
                                }
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    adapter = new ListViewAdapter(getActivity(),feedItem);
                    listView.setAdapter(adapter);
                    swipeRefreshLayout.setRefreshing(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });

    }

    @Override
    public void onRefresh() {
        findUsers();
    }


    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name) TextView user_name;
            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;
            @BindView(R.id.avatar) CircleImageView avatar;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.user_list_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.subscripe_btn.setVisibility(View.GONE);
            item = feed_item.get(position);
//            viewHolder.user_name.setText(item.getName() + " " + item.getSurname());
            viewHolder.user_name.setText(item.getLogin());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });
            return convertView;
        }
    }

}

