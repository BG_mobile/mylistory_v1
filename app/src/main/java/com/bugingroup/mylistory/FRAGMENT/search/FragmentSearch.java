package com.bugingroup.mylistory.FRAGMENT.search;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.bugingroup.mylistory.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 11.11.2016.
 */
@SuppressLint("ValidFragment")
public class FragmentSearch extends Fragment {

    @BindView(R.id.tabs)
    PagerSlidingTabStrip tabsStrip;
    private LinearLayout mTabsLinearLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.search)
    EditText search;

    SectionPagerAdapter mPagerAdapter;

    public static String search_text="null";
    int  color_theme,color_theme2;
    public FragmentSearch() {
    }


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search28, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getActivity().getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
       color_theme = typedValue.data;

        TypedValue typedValue2 = new TypedValue();
        Resources.Theme theme2 = getActivity().getTheme();
        theme2.resolveAttribute(R.attr.colorText, typedValue2, true);
        color_theme2 = typedValue2.data;


        search_text="null";
        setUpTabStrip();
        mPagerAdapter = new SectionPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(mPagerAdapter);
        tabsStrip.setViewPager(viewPager);

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    int page = viewPager.getCurrentItem();
                    selectPage(page);
                    return true;
                }
                return false;
            }
        });


        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position){
                search_text = search.getText().toString();
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                search_text = search.getText().toString();
                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/light.ttf");
                for(int i=0; i < mTabsLinearLayout.getChildCount(); i++){
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    tv.setTypeface(tf);
                    if(i == position){
                        tv.setTextColor(color_theme2);
                    } else {
                        tv.setTextColor(color_theme);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @OnClick(R.id.sendButton)
    void sendClick(){
        int page = viewPager.getCurrentItem();
        selectPage(page);
    }


    void selectPage(int page){
        search_text = search.getText().toString();
        mPagerAdapter = new SectionPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setCurrentItem(page);
    }

    public void setUpTabStrip(){
        mTabsLinearLayout = ((LinearLayout)tabsStrip.getChildAt(0));
        for(int i=0; i < mTabsLinearLayout.getChildCount(); i++){
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
            if(i == 0){
                tv.setTextColor(color_theme2);
            } else {
                tv.setTextColor(color_theme);
            }
        }
    }

}
