package com.bugingroup.mylistory.FRAGMENT.search;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.tag_library.DIRECTION;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentSearch24 extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.gridView)
    GridView gridView;
    private GridViewAdapter adapter;
    static ArrayList<FeedItem> feedItem;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    Parcelable state;

    ArrayList<TagGroupModel> labels;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search24, container, false);
        ButterKnife.bind(this,view);
        gridView.setHorizontalSpacing(4);
        gridView.setVerticalSpacing(4);


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        findPopularPosts();
                                    }
                                }
        );

        return view;
    }
    @Override
    public void onPause() {
        // Save ListView state @ onPause
        state = gridView.onSaveInstanceState();
        super.onPause();
    }

    private void findPopularPosts() {
        swipeRefreshLayout.setRefreshing(true);

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit","60");

        MainRestApi.get("findPopularPosts",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    feedItem = new ArrayList<FeedItem>();
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        JSONObject scren =new JSONObject("{\"screenshot\":{ }}");
                        if(body.has("list")) {
                            JSONArray list = body.getJSONArray("list");

                            for (int i = 0; i < list.length(); i++) {
                                JSONObject posts = (JSONObject) list.get(i);
                                JSONObject contentsOJ = new JSONObject("{\"contents\":[ ]}");
                                JSONArray contents = posts.has("contents") ? posts.getJSONArray("contents") : contentsOJ.getJSONArray("contents");
                                JSONObject jsonObject = new JSONObject("{\"avatar\":{ }}");
                                JSONObject avatar = posts.has("avatar") ? posts.getJSONObject("avatar") : jsonObject.getJSONObject("avatar");
                                for (int j = 0; j < contents.length(); j++) {
                                    JSONObject content = (JSONObject) contents.get(j);
                                    JSONObject screenshot = content.has("screenshot") ? content.getJSONObject("screenshot") : scren.getJSONObject("screenshot");
                                    FeedItem item = new FeedItem();
                                    item.setUser_id(posts.getString("userId"));
                                    item.setPost_id(posts.getString("postId"));
                                    item.setLikeCount(posts.has("likeCount") ? posts.getInt("likeCount") : 0);
                                    item.setCommentCount(posts.has("commentCount") ? posts.getInt("commentCount") : 0);
                                    item.setLogin(posts.has("login") ? posts.getString("login") : "");
                                    item.setName(posts.has("userName") ? posts.getString("userName") : "");
                                    item.setLiked(posts.getBoolean("liked"));
                                    item.setCreateDate(posts.getLong("createDate"));
                                    item.setDescription(posts.has("description") ? posts.getString("description") : "");
                                    if (!avatar.has("headers")) {
                                        item.setAvatar_hasHeader(false);
                                    } else {
                                        item.setAvatar_hasHeader(true);
                                        JSONObject ava_headers = avatar.getJSONObject("headers");
                                        item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                        item.setAva_authorization(ava_headers.getString("Authorization"));
                                        item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                        item.setAva_Host(ava_headers.getString("Host"));
                                    }
                                    if (!content.has("headers")) {
                                        item.setHasHeader(false);
                                    } else {
                                        JSONObject header = content.getJSONObject("headers");
                                        item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                        item.setAuthorization(header.getString("Authorization"));
                                        item.setX_amz_date(header.getString("x-amz-date"));
                                        item.setHost(header.getString("Host"));
                                        item.setHasHeader(true);
                                    }
                                    item.setContentType(content.has("contentType") ? content.getString("contentType") : "PICTURE");
                                    item.setUrl(content.has("url") ? content.getString("url") : "no");
                                    item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                    item.setResourceId(content.has("resourceId") ? content.getString("resourceId") : "no");
                                    item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                    item.setScren_resourceId(screenshot.has("resourceId") ? screenshot.getString("resourceId") : "no");
                                    item.setScren_url(screenshot.has("url") ? screenshot.getString("url") : "no");

                                    if (!screenshot.has("headers")) {
                                        item.setScren_hasHeader(false);
                                    } else {
                                        JSONObject header = screenshot.getJSONObject("headers");
                                        item.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                        item.setScren_authorization(header.getString("Authorization"));
                                        item.setScren_x_amz_date(header.getString("x-amz-date"));
                                        item.setScren_Host(header.getString("Host"));
                                        item.setScren_hasHeader(true);
                                    }
                                    labels = new ArrayList<TagGroupModel>();
                                    if (content.has("labels")) {
                                        JSONArray lab_array = content.getJSONArray("labels");
                                        for (int k = 0; k < lab_array.length(); k++) {
                                            JSONObject labObect = (JSONObject) lab_array.get(k);
                                            TagGroupModel model = new TagGroupModel();
                                            TagGroupModel.Tag tag1 = new TagGroupModel.Tag();
                                            tag1.setDirection(DIRECTION.RIGHT_CENTER.getValue());
                                            JSONObject user = labObect.getJSONObject("user");
                                            String n = user.has("name") ? user.getString("name") : "";
                                            String m = user.has("surname") ? user.getString("surname") : "";
                                            String login = user.has("login") ? user.getString("login") : "";
                                            tag1.setName(login);

                                            model.getTags().add(tag1);
                                            model.setPercentY(Float.parseFloat(labObect.getString("y")));
                                            model.setPercentX(Float.parseFloat(labObect.getString("x")));
                                            model.setUser_id(labObect.getString("userId"));
                                            labels.add(model);
                                        }
                                    }
                                    item.setLabels(labels);
                                    feedItem.add(item);
                                }
                            }
                            adapter = new GridViewAdapter(getActivity(), feedItem);
                            gridView.setAdapter(adapter);
                            swipeRefreshLayout.setRefreshing(false);
                            if(state != null) {
                                gridView.onRestoreInstanceState(state);
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});

    }

    @OnClick(R.id.toSearch)
    void onSearchClick(View v) {
            ((MainActivity) getActivity()).replaceFragment(new FragmentSearch());
        }

    @Override
    public void onRefresh() {
        findPopularPosts();
    }

//  GridViewAdapter

public class GridViewAdapter extends BaseAdapter {

    ArrayList<FeedItem>  feed_item;
    FeedItem item;
    Activity activity;

    public GridViewAdapter(Activity activity, ArrayList<FeedItem> feed_item) {
        this.feed_item = feed_item;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return feed_item.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;

        @BindView(R.id.ic_video)
        ImageView ic_video;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.home16_item_grid, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        item = feed_item.get(position);

        viewHolder.imageView.setVisibility(View.VISIBLE);

        if(item.getContentType().equals("PICTURE")) {
            viewHolder.ic_video.setVisibility(View.GONE);

            Map<String, String> headers_content = new HashMap<String, String>();
            if(item.getHasHeader()){
                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers_content.put("x-amz-date", item.getX_amz_date());
                headers_content.put("Host", item.getHost());
                headers_content.put("Authorization", item.getAuthorization());
            }else {
                headers_content.put("auth", MainActivity.token);
            }
            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_content)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();
            ImageLoader.getInstance().displayImage(item.getUrl(), viewHolder.imageView, header_options, animateFirstListener);
        }else {
            viewHolder.ic_video.setVisibility(View.VISIBLE);
            Map<String, String> headers_screen = new HashMap<String, String>();
            if(item.getScren_hasHeader()){
                headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                headers_screen.put("Host", item.getScren_Host());
                headers_screen.put("Authorization", item.getScren_authorization());
            }else {
                headers_screen.put("auth", MainActivity.token);
            }
            DisplayImageOptions screen_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_screen)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getScren_url(), viewHolder.imageView, screen_options, animateFirstListener);

        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = feed_item.get(position);
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                Fragment fragment = new FragmentSearchOnList();
                fragment.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragment(fragment);

            }
        });

        return convertView;
    }
}
}
