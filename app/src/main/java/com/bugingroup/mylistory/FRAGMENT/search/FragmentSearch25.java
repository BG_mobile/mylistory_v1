package com.bugingroup.mylistory.FRAGMENT.search;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.FRAGMENT.profile.FragmentEditPosts;
import com.bugingroup.mylistory.LikedUsers;
import com.bugingroup.mylistory.FRAGMENT.home.AllComment;
import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome16;
import com.bugingroup.mylistory.FRAGMENT.search.Fragment.SearchByTag;
import com.bugingroup.mylistory.ImagePicker.Views.TagImageView;
import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.Player;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.tag_library.DIRECTION;
import com.bugingroup.tag_library.TagViewGroup;
import com.bugingroup.tag_library.views.ITagView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.GridHolder;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.dialogPlus;
import static com.bugingroup.mylistory.MainActivity.token;
import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class FragmentSearch25 extends Fragment {

    @BindView(R.id.likeCount)
    TextView likeCount;

    @BindView(R.id.user_name)
    TextView userName;

    @BindView(R.id.name)
    TextView name;

    @BindView(R.id.comment)
    TextView comment;

    @BindView(R.id.btn_comment)
    ImageButton btn_comment;

    @BindView(R.id.btn_share)
    ImageButton btn_share;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.to_other_profile)
    RelativeLayout to_other_profile;

    @BindView(R.id.tagImageView)
    TagImageView tagImageView;


    @BindView(R.id.is_mark)
    ImageView is_mark;

    @BindView(R.id.is_mute)
    ImageView is_mute;

    @BindView(R.id.contents_video)
    Player contents_video;

    @BindView(R.id.like_layout)
    RelativeLayout like_layout;

    @BindView(R.id.heart)
    ImageView heartImageView;

    @BindView(R.id.circleBg)
    View circleBackground;

    @BindView(R.id.avatar)
    ImageView avatar;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.like)
    ImageView like;
    ArrayList<TagGroupModel> labels;

    boolean is_photo = false;

    String postId, USERID, user_id;
    private HashTagHelper mTextHashTagHelper;
    ArrayList<FeedItem> feedItem;
    ArrayList<FeedItem> feedItemP;
    Boolean doubleclick = false;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    private final DecelerateInterpolator DECCELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();

    DialogPlusAdapter adapter;
    RelativeLayout footer_edit_layout;
    int selected_user = 100;
    String resourse_id;

    SharedPreferences prefsStart;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search25, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences prefs = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, getActivity().MODE_PRIVATE);
        int user_num = prefs.getInt("user_number", 1);

        switch (user_num) {
            case 1:
                prefsStart = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, getActivity().MODE_PRIVATE);
                Log.d("MAPshare1", "1111");
                System.out.println("1111" + user_num);
                break;
            case 2:
                prefsStart = getActivity().getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, getActivity().MODE_PRIVATE);
                Log.d("MAPshare2", "2222");
                System.out.println("2222" + user_num);
                break;
        }
        USERID = prefsStart.getString("userId", "");

        Bundle bundle = this.getArguments();
        feedItemP = new ArrayList<FeedItem>();
        adapter = new DialogPlusAdapter(getActivity(), feedItemP);
        if (bundle != null) {
            postId = bundle.getString("postId");
        }
        Log.d("postId", postId);
        PostGetServer(postId);
        Log.d("DDDD2", postId);

        return view;
    }

    private void PostGetServer(String postId) {


        Log.d("DDDD3", postId);

        RequestParams params = new RequestParams();
        params.put("postId", postId);

        MainRestApi.get("post", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                feedItem = new ArrayList<FeedItem>();
                try {
                    if (response.getBoolean("success")) {
                        JSONObject scren = new JSONObject("{\"screenshot\":{ }}");
                        JSONObject posts = response.getJSONObject("body");

                        JSONArray contents = posts.getJSONArray("contents");
                        JSONObject avatar = posts.getJSONObject("avatar");
                        for (int j = 0; j < contents.length(); j++) {
                            JSONObject content = (JSONObject) contents.get(j);
                            JSONObject screenshot = content.has("screenshot") ? content.getJSONObject("screenshot") : scren.getJSONObject("screenshot");
                            FeedItem item = new FeedItem();
                            item.setUser_id(posts.getString("userId"));
                            user_id = posts.getString("userId");
                            item.setPost_id(posts.getString("postId"));
                            item.setLikeCount(posts.has("likeCount") ? posts.getInt("likeCount") : 0);
                            item.setCommentCount(posts.has("commentCount") ? posts.getInt("commentCount") : 0);
                            item.setLogin(posts.has("login") ? posts.getString("login") : "");
                            item.setName(posts.has("userName") ? posts.getString("userName") : "");
                            item.setLiked(posts.has("liked") && posts.getBoolean("liked"));
                            item.setCreateDate(posts.getLong("createDate"));
                            item.setDescription(posts.has("description") ? posts.getString("description") : "");
                            if (!avatar.has("headers")) {
                                item.setAvatar_hasHeader(false);
                            } else {
                                item.setAvatar_hasHeader(true);
                                JSONObject ava_headers = avatar.getJSONObject("headers");
                                item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                item.setAva_authorization(ava_headers.getString("Authorization"));
                                item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                item.setAva_Host(ava_headers.getString("Host"));
                            }
                            if (!content.has("headers")) {
                                item.setHasHeader(false);
                            } else {
                                JSONObject header = content.getJSONObject("headers");
                                item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                item.setAuthorization(header.getString("Authorization"));
                                item.setX_amz_date(header.getString("x-amz-date"));
                                item.setHost(header.getString("Host"));
                                item.setHasHeader(true);
                            }
                            item.setUrl(content.has("url") ? content.getString("url") : "no");
                            item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                            item.setContentType(content.has("contentType") ? content.getString("contentType") : "PICTURE");
                            item.setResourceId(content.has("resourceId") ? content.getString("resourceId") : "no");
                            item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                            item.setScren_resourceId(screenshot.has("resourceId") ? screenshot.getString("resourceId") : "no");
                            item.setScren_url(screenshot.has("url") ? screenshot.getString("url") : "no");

                            if (!screenshot.has("headers")) {
                                item.setScren_hasHeader(false);
                            } else {
                                JSONObject header = screenshot.getJSONObject("headers");
                                item.setScren_x_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                item.setScren_authorization(header.getString("Authorization"));
                                item.setScren_x_amz_date(header.getString("x-amz-date"));
                                item.setScren_Host(header.getString("Host"));
                                item.setScren_hasHeader(true);
                            }
                            labels = new ArrayList<TagGroupModel>();
                            if (content.has("labels")) {
                                JSONArray lab_array = content.getJSONArray("labels");
                                for (int k = 0; k < lab_array.length(); k++) {
                                    JSONObject labObect = (JSONObject) lab_array.get(k);
                                    TagGroupModel model = new TagGroupModel();
                                    TagGroupModel.Tag tag1 = new TagGroupModel.Tag();
                                    tag1.setDirection(DIRECTION.RIGHT_CENTER.getValue());
                                    JSONObject user = labObect.getJSONObject("user");
                                    tag1.setName(user.has("login") ? user.getString("login") : " ");

                                    model.getTags().add(tag1);
                                    model.setPercentY(Float.parseFloat(labObect.getString("y")));
                                    model.setPercentX(Float.parseFloat(labObect.getString("x")));
                                    model.setUser_id(labObect.getString("userId"));
                                    labels.add(model);
                                }
                            }
                            item.setLabels(labels);
                            feedItem.add(item);
                        }

                    }

                    SetContent(feedItem);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    private TagViewGroup.OnTagGroupClickListenerr mTagGroupClickListener = new TagViewGroup.OnTagGroupClickListenerr() {

        @Override
        public void onTagClick(TagViewGroup group, ITagView tag, int index) {
            Bundle bundle = new Bundle();
            bundle.putString("userId", feedItem.get(0).getLabels().get(tagImageView.getTagGroupIndex(group)).getUser_id());
            Fragment fragment = new FragmentHome16();
            fragment.setArguments(bundle);
            ((MainActivity) getActivity()).replaceFragment(fragment);
        }

    };

    private void SetContent(final ArrayList<FeedItem> feedItemm) {
        final FeedItem item = feedItemm.get(0);

        if (item.getLabels().size() > 0) {
            tagImageView.setTagList(item.getLabels(), mTagGroupClickListener);
            is_mark.setVisibility(View.VISIBLE);
        } else {
            tagImageView.Remove();
            is_mark.setVisibility(View.GONE);
        }

        resourse_id = item.getResourceId();
        if (item.getLiked()) {
            like.setImageResource(R.drawable.ic_like);
        } else {
            like.setImageResource(R.drawable.ic_favorite);
        }
        final Resources res = getResources();
        int count = item.getLikeCount();
        String text = count == 0 ? "" : res.getString(R.string.like, count);
        likeCount.setText(text);

        likeCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("postId", item.getPost_id());
                Fragment fragment = new LikedUsers();
                fragment.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragment(fragment);

            }
        });

        if (item.getCommentCount() == 0)
            comment.setVisibility(View.GONE);
        else {
            comment.setVisibility(View.VISIBLE);
            int comment_count = item.getCommentCount();
            String comment_text = res.getString(R.string.comment, comment_count);
            comment.setText(comment_text);
        }

//        userName.setText(item.getName());
        userName.setText(item.getLogin());
        if (item.getDescription().equals("")) {
            name.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
        } else {
            name.setVisibility(View.VISIBLE);
            description.setVisibility(View.VISIBLE);
            name.setText(item.getLogin() + " : ");
//            name.setText(item.getName());
            description.setText(item.getDescription());
        }

        mTextHashTagHelper = HashTagHelper.Creator.create(getResources().getColor(R.color.hash_tag), new HashTagHelper.OnHashTagClickListener() {
            @Override
            public void onHashTagClicked(String hashTag) {
                Bundle bundle = new Bundle();
                bundle.putString("hash_tag", hashTag);
                Fragment fragment = new SearchByTag();
                fragment.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragment(fragment);
            }
        });
        mTextHashTagHelper.handle(description);

        Map<String, String> headers_content = new HashMap<String, String>();
        if (item.getHasHeader()) {
            headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
            headers_content.put("x-amz-date", item.getX_amz_date());
            headers_content.put("Host", item.getHost());
            headers_content.put("Authorization", item.getAuthorization());
        } else {
            headers_content.put("auth", MainActivity.token);
        }


        if (item.getContentType().equals("PICTURE")) {
            is_photo = true;
            contents_video.setVisibility(View.GONE);
            tagImageView.setVisibility(View.VISIBLE);
            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_content)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();


            tagImageView.setImageUrl(item.getUrl(), header_options, progressBar);
            is_mute.setVisibility(View.GONE);


        } else {
            is_photo = false;

            Map<String, String> headers_screen = new HashMap<String, String>();
            if (item.getScren_hasHeader()) {
                headers_screen.put("x-amz-content-sha256", item.getScren_x_amz_content_sha256());
                headers_screen.put("x-amz-date", item.getScren_x_amz_date());
                headers_screen.put("Host", item.getScren_Host());
                headers_screen.put("Authorization", item.getScren_authorization());
            } else {
                headers_screen.put("auth", token);
            }


            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers_screen)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            tagImageView.setImageUrl(item.getScren_url(), header_options, progressBar);

            ViewTreeObserver vto = tagImageView.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    tagImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                    contents_video.setVideoSize(tagImageView.getMeasuredWidth(), tagImageView.getMeasuredHeight());
                    Log.d("AAAAAA", "Height: " + tagImageView.getMeasuredHeight() + " Width: " + tagImageView.getMeasuredWidth());
                    return true;
                }
            });

            try {
                Method setVideoURIMethod = contents_video.getClass().getMethod("setVideoURI", Uri.class, Map.class);
                setVideoURIMethod.invoke(contents_video, Uri.parse(item.getUrl()), headers_content);
                contents_video.start();
                is_mute.setImageResource(R.drawable.ic_volume_off);
            } catch (NoSuchMethodException e) {
                System.out.println(e);
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            contents_video.setVisibility(View.VISIBLE);
            tagImageView.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.GONE);

        }


        Map<String, String> ava_headers = new HashMap<String, String>();
        if (item.getAvatar_hasHeader()) {
            ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
            ava_headers.put("x-amz-date", item.getAva_x_amz_date());
            ava_headers.put("Host", item.getAva_Host());
            ava_headers.put("Authorization", item.getAva_authorization());
        } else {
            ava_headers.put("auth", MainActivity.token);
        }
        DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                .extraForDownloader(ava_headers)
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.loading)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader.getInstance().displayImage(item.getAvatar_url(), avatar, ava_options, animateFirstListener);

        to_other_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("userId", item.getUser_id());
                Fragment fragment = new FragmentHome16();
                fragment.setArguments(bundle);
                ((MainActivity) getActivity()).replaceFragment(fragment);
            }
        });

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncHttpClient client = new AsyncHttpClient();
                client.addHeader("auth", MainActivity.token);
                RequestParams params = new RequestParams();
                params.put("postId", item.getPost_id());

                client.post(MainActivity.METHOD + "userLike", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        /// / If the response is JSONObject instead of expected JSONArray

                        try {
                            if (response.getBoolean("success")) {
                                if (response.getString("body").equals("LIKE")) {
                                    item.setLikeCount(item.getLikeCount() + 1);
                                    like.setImageResource(R.drawable.ic_like);
                                    String text = res.getString(R.string.like, item.getLikeCount());
                                    likeCount.setText(text);
                                    item.setLiked(true);
                                } else {
                                    item.setLikeCount(item.getLikeCount() - 1);
                                    like.setImageResource(R.drawable.ic_favorite);
                                    String text = res.getString(R.string.like, item.getLikeCount());
                                    likeCount.setText(text);
                                    item.setLiked(false);
                                }
                                SetContent(feedItem);
                            } else {
                                Toast.makeText(getActivity(), "Сервер не доступен", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                        // Pull out the first event on the public timeline

                    }
                });
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllComment.class).putExtra("postId", item.getPost_id()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            }
        });

        btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllComment.class).putExtra("postId", item.getPost_id()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            }
        });

        like_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (doubleclick) {
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.addHeader("auth", MainActivity.token);
                    RequestParams params = new RequestParams();
                    params.put("postId", item.getPost_id());

                    client.post(MainActivity.METHOD + "userLike", params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            /// / If the response is JSONObject instead of expected JSONArray
                            System.out.println(1212);
                            System.out.println(response);
                            try {
                                if (response.getBoolean("success")) {
                                    circleBackground.setVisibility(View.VISIBLE);
                                    heartImageView.setVisibility(View.VISIBLE);

                                    circleBackground.setScaleY(0.1f);
                                    circleBackground.setScaleX(0.1f);
                                    circleBackground.setAlpha(1f);
                                    heartImageView.setScaleY(0.1f);
                                    heartImageView.setScaleX(0.1f);

                                    AnimatorSet animatorSet = new AnimatorSet();

                                    ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(circleBackground, "scaleY", 0.1f, 1f);
                                    bgScaleYAnim.setDuration(200);
                                    bgScaleYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                    ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(circleBackground, "scaleX", 0.1f, 1f);
                                    bgScaleXAnim.setDuration(200);
                                    bgScaleXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                    ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(circleBackground, "alpha", 1f, 0f);
                                    bgAlphaAnim.setDuration(200);
                                    bgAlphaAnim.setStartDelay(150);
                                    bgAlphaAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

                                    ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(heartImageView, "scaleY", 0.1f, 1f);
                                    imgScaleUpYAnim.setDuration(300);
                                    imgScaleUpYAnim.setInterpolator(DECCELERATE_INTERPOLATOR);
                                    ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(heartImageView, "scaleX", 0.1f, 1f);
                                    imgScaleUpXAnim.setDuration(300);
                                    imgScaleUpXAnim.setInterpolator(DECCELERATE_INTERPOLATOR);

                                    ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(heartImageView, "scaleY", 1f, 0f);
                                    imgScaleDownYAnim.setDuration(300);
                                    imgScaleDownYAnim.setInterpolator(ACCELERATE_INTERPOLATOR);
                                    ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(heartImageView, "scaleX", 1f, 0f);
                                    imgScaleDownXAnim.setDuration(300);
                                    imgScaleDownXAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

                                    animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
                                    animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

                                    animatorSet.addListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            circleBackground.setVisibility(View.GONE);
                                            heartImageView.setVisibility(View.GONE);
                                        }
                                    });
                                    animatorSet.start();
                                    if (response.getString("body").equals("LIKE")) {
                                        item.setLikeCount(item.getLikeCount() + 1);
                                        like.setImageResource(R.drawable.ic_like);
                                        String text = res.getString(R.string.like, item.getLikeCount());
                                        likeCount.setText(text);
                                        item.setLiked(true);
                                    } else {
                                        item.setLikeCount(item.getLikeCount() - 1);
                                        like.setImageResource(R.drawable.ic_favorite);
                                        String text = res.getString(R.string.like, item.getLikeCount());
                                        likeCount.setText(text);
                                        item.setLiked(false);
                                    }
                                    SetContent(feedItem);
                                } else {
                                    Toast.makeText(getActivity(), "Сервер не доступен", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                            // Pull out the first event on the public timeline
                            System.out.println(1313);
                            System.out.println(timeline);

                        }
                    });
                }

                if (item.getContentType().equals("VIDEO") && contents_video.isPlaying()) {
                    if (contents_video.ismute()) {
                        is_mute.setImageResource(R.drawable.ic_volume_up);
                        contents_video.unmute();
                    } else {
                        is_mute.setImageResource(R.drawable.ic_volume_off);
                        contents_video.mute();
                    }
                    is_mute.setVisibility(View.VISIBLE);
                } else {
                    if (item.getLabels().size() > 0) {
                        tagImageView.excuteTagsAnimation();
                        if (tagImageView.isHidenGroup()) {
                            is_mark.setVisibility(View.GONE);
                        } else {
                            is_mark.setVisibility(View.VISIBLE);
                        }
                    }
                }
                doubleclick = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleclick = false;
                    }
                }, 500);
            }
        });
        findUsers("");

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final GridHolder holder = new GridHolder(4);
                dialogPlus = DialogPlus.newDialog(getActivity())
                        .setAdapter(adapter)
                        .setContentHolder(holder)
                        .setCancelable(true)
                        .setExpanded(false, 500)
                        .setHeader(R.layout.header_fragment26_view)
                        .setFooter(R.layout.footer_fragment26_view)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                            }
                        })
                        .create();

                final EditText search = (EditText) dialogPlus.findViewById(R.id.search);
                search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            feedItemP.clear();
                            selected_user = 100;
                            findUsers(v.getText().toString());
                            URL_CONSTANT.hideSoftKeyboard(getActivity());
                            return true;
                        }
                        return false;
                    }
                });
                Button cancelBtn = (Button) dialogPlus.findViewById(R.id.cancelBtn);
                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogPlus.dismiss();
                    }
                });

                footer_edit_layout = (RelativeLayout) dialogPlus.findViewById(R.id.edit_layout);
                final EditText message = (EditText) dialogPlus.findViewById(R.id.message);
                ImageButton send_btn = (ImageButton) dialogPlus.findViewById(R.id.send_btn);
                send_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                        progressDialog.show();
                        Log.d("SELECTED", "*" + resourse_id);

                        Log.d("SELECTED", selected_user + "*");

                        Log.d("ASAS", feedItemP.get(selected_user).getName());
                        String body = "{\"name\":\"nurik\", \"users\":[\"" + feedItemP.get(selected_user).getUser_id() + "\"]}";

                        MainRestApi.post(getActivity(), "addChat", body, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                                try {
                                    Log.d("RESPONS", response + "*");
                                    if (response.getBoolean("success")) {

                                        String bodyP = "{\"chatId\":\"" + response.getString("body") + "\",  \"description\":\"\", \"contentType\":\"PICTURE\", \"resourceId\": \"" + resourse_id + "\",\"postId\":\"" + postId + "\"}";
                                        MainRestApi.post(getActivity(), "sendMessage", bodyP, new JsonHttpResponseHandler() {
                                            @Override
                                            public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {
                                                try {
                                                    Log.d("RESPONS", response2 + "*");
                                                    if (response2.getBoolean("success")) {
                                                        if (message.getText().length() > 0) {
                                                            String body = "{\"chatId\":\"" + response.getString("body") + "\",  \"description\":\"" + message.getText().toString() + "\", \"contentType\":\"TEXT\", \"resourceId\": null}";
                                                            MainRestApi.post(getActivity(), "sendMessage", body, new JsonHttpResponseHandler() {
                                                                @Override
                                                                public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {
                                                                    try {
                                                                        Log.d("RESPONS", response2 + "*");
                                                                        if (response2.getBoolean("success")) {
                                                                            message.setText(null);
                                                                        }
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } finally {
                                    progressDialog.dismiss();
                                    dialogPlus.dismiss();
                                }

                            }
                        });
                    }
                });

                dialogPlus.show();
            }
        });

    }

    @OnClick(R.id.back)
    void backClik() {
        if (dialogPlus.isShowing()) {
            dialogPlus.dismiss();
        } else {
            getActivity().onBackPressed();
        }

    }


    @OnClick(R.id.show_menu)
    void menuClick() {
        if (!USERID.equals(user_id)) {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.home17_menu_dialog);
            dialog.show();

            TextView save = (TextView) dialog.findViewById(R.id.save);
            if(!is_photo){
               save.setVisibility(View.GONE);
            }else {
                save.setVisibility(View.VISIBLE);
            }
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    URL_CONSTANT.SaveImage(getActivity(),tagImageView.getImageBitmap());
                    dialog.dismiss();
                }
            });
        } else {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.profile44_menu_dialog);
            dialog.show();
            TextView save = (TextView) dialog.findViewById(R.id.save);
            if(!is_photo){
                save.setVisibility(View.GONE);
            }else {
                save.setVisibility(View.VISIBLE);
            }
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    URL_CONSTANT.SaveImage(getActivity(),tagImageView.getImageBitmap());
                    dialog.dismiss();
                }
            });

            TextView delete = (TextView) dialog.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RequestParams params = new RequestParams();
                    params.put("postId", postId);

                    MainRestApi.post("deletePost", params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                            Log.d("CHANGE PASS", response + "**");
                            try {
                                if (response.getBoolean("success")) {
                                    getActivity().onBackPressed();
                                } else {
                                    Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                }
                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });
            TextView edit = (TextView) dialog.findViewById(R.id.edit);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putString("postId", postId);
                    Fragment fragment = new FragmentEditPosts();
                    fragment.setArguments(bundle);
                    ((MainActivity) getActivity()).replaceFragment(fragment);
                }
            });
        }
    }

    private void findUsers(String search_text) {

        Log.d("FRAGMENTTT", search_text);

        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "8");
        params.put("searchText", search_text);


        MainRestApi.get("findUsers", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response.getBoolean("success")) {
                        if (response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if (body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    if (!USERID.equals(jsonObject.getString("id"))) {
                                        FeedItem item = new FeedItem();
                                        item.setUser_id(jsonObject.getString("id"));
                                        item.setLogin(jsonObject.has("login") ? jsonObject.getString("login") : " ");
                                        item.setName(jsonObject.has("name") ? jsonObject.getString("name") : " ");
                                        item.setSurname(jsonObject.has("surname") ? jsonObject.getString("surname") : " ");
                                        JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                        JSONObject avatar = jsonObject.has("avatar") ? jsonObject.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                        if (!avatar.has("headers")) {
                                            item.setAvatar_hasHeader(false);
                                        } else {
                                            item.setAvatar_hasHeader(true);
                                            JSONObject ava_headers = avatar.getJSONObject("headers");
                                            item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                            item.setAva_authorization(ava_headers.getString("Authorization"));
                                            item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                            item.setAva_Host(ava_headers.getString("Host"));
                                        }
                                        item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                        item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                        feedItemP.add(item);
                                    }
                                }
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });

    }

    //  DialogPlusAdapter
    public class DialogPlusAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public DialogPlusAdapter(Activity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;

            @BindView(R.id.avatar)
            CircleImageView avatar;
            @BindView(R.id.avatar_check)
            CircleImageView avatar_check;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment26_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);
            Log.d("SELECTED", selected_user + "*" + position);

            if (position == selected_user) {
                viewHolder.avatar_check.setVisibility(View.VISIBLE);
            } else {
                viewHolder.avatar_check.setVisibility(View.GONE);
            }
            viewHolder.user_name.setText(item.getLogin());
//            viewHolder.user_name.setText(item.getName() + " " + item.getSurname());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .cacheInMemory(true)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    footer_edit_layout.setVisibility(View.VISIBLE);
                    selected_user = position;
                    Log.d("SELECTED", selected_user + "*");
                    adapter.notifyDataSetChanged();


                }
            });


            return convertView;
        }
    }
//
//
//    private Bitmap addWaterMark(Bitmap src) {
//        int w = src.getWidth();
//        int h = src.getHeight();
//        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());
//        Canvas canvas = new Canvas(result);
//        canvas.drawBitmap(src, 0, 0, null);
//
//        Bitmap waterMark = BitmapFactory.decodeResource(getResources(), R.drawable.watermark);
//        canvas.drawBitmap(waterMark, w-110, h-80, null);
//
//        return result;
//    }
//
//
//    private void SaveImage(Bitmap finalBitmap) {
//
//        Bitmap bm = addWaterMark(finalBitmap);
//        String root = Environment.getExternalStorageDirectory().toString();
//        File myDir = new File(root + "/Pictures/mylistory/");
//        myDir.mkdirs();
//        long tsLong = System.currentTimeMillis()/1000;
//
//        String fname = tsLong +".jpg";
//        File file = new File (myDir, fname);
//        if (file.exists ()) file.delete ();
//        try {
//            FileOutputStream out = new FileOutputStream(file);
//            bm.compress(Bitmap.CompressFormat.JPEG, 100, out);
//            out.flush();
//            out.close();
//            Toast.makeText(getActivity(), "Downloaded success", Toast.LENGTH_SHORT).show();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}

