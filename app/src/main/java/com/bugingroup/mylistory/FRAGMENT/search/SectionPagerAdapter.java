package com.bugingroup.mylistory.FRAGMENT.search;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bugingroup.mylistory.FRAGMENT.search.Fragment.TabFragmentBest;
import com.bugingroup.mylistory.FRAGMENT.search.Fragment.TabFragmentPerson;
import com.bugingroup.mylistory.FRAGMENT.search.Fragment.TabFragmentTag;

/**
 * Created by Nurik on 30.01.2017.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {"Лучшее","Люди", "Метки"};

    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }


    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TabFragmentBest();
            case 1:
                return new TabFragmentPerson();
            case 2:
            default:
                return  new TabFragmentTag();

        }
    }
}