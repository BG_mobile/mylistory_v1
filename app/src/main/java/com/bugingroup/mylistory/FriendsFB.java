package com.bugingroup.mylistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by Nurik on 03.03.2017.
 */

public class FriendsFB extends Activity {


    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.info)
    TextView info;

    LoginButton login_button;
    CallbackManager callbackManager;
    GraphRequest request=null;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_fb);
        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();
        login_button = (LoginButton) findViewById(R.id.login_button);
        // If using in a fragment
        login_button.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends"));

        login_button.registerCallback(callbackManager, callback);

        info.setVisibility(View.GONE);

    }
    FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            String token = loginResult.getAccessToken().getToken();
            info.setVisibility(View.VISIBLE);
            login_button.setVisibility(View.GONE);
            Log.d("ASASASA_FACEBOOK",token);
            RequestParams params = new RequestParams();
            params.put("accessToken", token);
            AsyncHttpClient client = new AsyncHttpClient();
            client.get("http://mylistory.com:8080/reg-server/services/FriendsVK",params,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    System.out.println(response+"**"+statusCode);


                    try {
                        if(response.getBoolean("success")){

                        }else {
                            Toast.makeText(FriendsFB.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    System.out.println(responseString+"**"+statusCode);

                }
            });
        }

        @Override
        public void onCancel() {
            request.executeAsync().cancel(false);
        }

        @Override
        public void onError(FacebookException error) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==704 && resultCode==RESULT_OK){
            startActivity(new Intent(FriendsFB.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(request!=null)
            request.executeAsync().cancel(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(request!=null)
            request.executeAsync().cancel(true);
    }

    @OnClick(R.id.back)
    void BackClick(){
            finish();
    }


    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {
        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
        ArrayList<FeedItem> feedItem;
        FeedItem item;

        public ListViewAdapter(ArrayList<FeedItem> feedItem) {
            this.feedItem = feedItem;
        }

        class ViewHolder {

            @BindView(R.id.avatar)
            CircleImageView avatar;
            @BindView(R.id.name)
            TextView name;
            @BindView(R.id.close_btn)
            ImageButton close_btn;
            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;

            @BindView(R.id.content_img1)
            ImageView imageView1;
            @BindView(R.id.content_img2) ImageView imageView2;
            @BindView(R.id.content_img3) ImageView imageView3;


            final int[] imageview ={R.id.content_img1,R.id.content_img2,R.id.content_img3};

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public String getItem(int i) {
            return null;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home18_item_list, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            item = feedItem.get(position);

            viewHolder.name.setText(item.getSurname()+" "+item.getName());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if(item.getAvatar_hasHeader()){
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            }else {
                ava_headers.put("auth", MainActivity.token);
            }
            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();
            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);

            ArrayList<FeedItem> postsItem = item.getPostsItem();
            if(postsItem.size()==0){
                viewHolder.imageView1.setVisibility(View.GONE);
                viewHolder.imageView2.setVisibility(View.GONE);
                viewHolder.imageView3.setVisibility(View.GONE);
            }else {
                viewHolder.imageView1.setVisibility(View.VISIBLE);
                viewHolder.imageView2.setVisibility(View.VISIBLE);
                viewHolder.imageView3.setVisibility(View.VISIBLE);
            }
            for (int i=0;i<postsItem.size();i++){
                FeedItem item2 = postsItem.get(i);
                Map<String, String> headers_content = new HashMap<String, String>();
                if (item2.getHasHeader()) {
                    headers_content.put("x-amz-content-sha256", item2.getX_amz_content_sha256());
                    headers_content.put("x-amz-date", item2.getX_amz_date());
                    headers_content.put("Host", item2.getHost());
                    headers_content.put("Authorization", item2.getAuthorization());
                } else {
                    headers_content.put("auth", token);
                }

                DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                        .extraForDownloader(headers_content)
                        .showImageOnLoading(R.drawable.loading)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .build();

                ImageView img = (ImageView) convertView.findViewById(viewHolder.imageview[i]);
                ImageLoader.getInstance().displayImage(item2.getUrl(), img, header_options, animateFirstListener);

            }

            viewHolder.close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    feedItem.remove(position);
                    notifyDataSetChanged();
                }
            });

//            if(!item.getFollowed()){
//                viewHolder.subscripe_btn.setText(subscribe);
//                viewHolder.subscripe_btn.setBackground(getResources().getDrawable(R.drawable.bg_button_subscribe_golden));
//                viewHolder.subscripe_btn.setTextColor(getResources().getColor(R.color.white));
//            }else {
//                viewHolder.subscripe_btn.setText(following);
//                viewHolder.subscripe_btn.setBackground(getResources().getDrawable(R.drawable.bg_button_subscribe));
//                viewHolder.subscripe_btn.setTextColor(getResources().getColor(R.color.golden));
//            }

            viewHolder.subscripe_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item = feedItem.get(position);

                    RequestParams params = new RequestParams();
                    params.put("favoriteId", item.getUser_id());

                    if(item.getFollowed()){
                        MainRestApi.post("unfollowing",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                Log.d("FOLLOWINGUNR",response+"*");
                                try {
                                    if(response.getBoolean("success")){
                                        notifyDataSetChanged();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }else {
                        MainRestApi.post("addFollower",params,new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                try {
                                    if(response.getBoolean("success")){
                                        notifyDataSetChanged();
                                    }else {
                                        Toast.makeText(FriendsFB.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }

                }
            });




            return convertView;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getCount() {
            return feedItem.size();
        }
    }



}
