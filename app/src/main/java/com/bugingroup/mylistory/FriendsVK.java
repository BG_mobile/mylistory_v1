package com.bugingroup.mylistory;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 27.01.2017.
 */
public class FriendsVK extends Activity{

    @BindView(R.id.webView)
    WebView web;

    @BindView(R.id.listView)
    ListView listView;

    private UserListAdapter adapter;
    ArrayList<FeedItem> feed_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vk_auth);
        ButterKnife.bind(this);
        web.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

        web.loadUrl("https://oauth.vk.com/authorize?client_id=5731417&client_secret=HPjaWdZ5rnBFlpQRuEdw&redirect_uri=http://mylistory.com:8080/reg-server/services/FriendsVK&response_type=code&display=mobile");
        web.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if(url.contains("http://mylistory.com:8080")){
                    AsyncHttpClient client = new AsyncHttpClient();
                    Log.d("ASASAS1","BABABA");
                    client.get(url,   new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.d("ASASAS33333", response+"/");
                            feed_list = new ArrayList<FeedItem>();
                            try {
                                if(response.getBoolean("success")){
                                    JSONArray items = response.getJSONArray("items");
                                    for (int i=0;i<items.length();i++){
                                        JSONObject user = (JSONObject) items.get(i);
                                        FeedItem item = new FeedItem();

                                        item.setUser_id(user.getString("userId"));
                                        item.setSurname(user.getString("last_name"));
                                        item.setName(user.getString("first_name"));
                                        item.setAvatar_url(user.getString("photo"));
                                        feed_list.add(item);
                                    }
                                    web.setVisibility(View.GONE);
                                    listView.setVisibility(View.VISIBLE);
                                    adapter = new UserListAdapter (FriendsVK.this,feed_list);
                                    listView.setAdapter(adapter);
                                }else {
                                    Toast.makeText(FriendsVK.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }else {

                    view.loadUrl(url);
                }
                Log.d("ASASAS3", url);
                return true;
            }

        });
    }


    public class UserListAdapter extends BaseAdapter {
        private Activity activity;
        ArrayList<FeedItem> feed_list;
        FeedItem item;
        UserListAdapter(Activity activity, ArrayList<FeedItem> feed_list) {
            this.activity = activity;
            this.feed_list = feed_list;
        }


        @Override
        public int getCount() {
            return feed_list.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            @BindView(R.id.user_name)
            TextView user_name;

            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;

            @BindView(R.id.avatar)
            CircleImageView avatar;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.user_list_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            item = feed_list.get(position);

            viewHolder.user_name.setText(item.getName()+" "+item.getSurname());

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar);

            viewHolder.subscripe_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_list.get(position);

                    RequestParams params = new RequestParams();
                    params.put("favoriteId", item.getUser_id());


                    MainRestApi.post("addFollower",params,new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                            try {
                                if(response.getBoolean("success")){

                                }else {
                                    Toast.makeText(FriendsVK.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }});


                }
            });
            return convertView;
        }


    }

}