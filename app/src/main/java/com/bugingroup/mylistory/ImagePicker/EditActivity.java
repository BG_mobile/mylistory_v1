package com.bugingroup.mylistory.ImagePicker;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bugingroup.mylistory.ImagePicker.commons.ui.CropImageView;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bugingroup.mylistory.ImagePicker.commons.utils.FileUtils.getLocalDir;

/**
 * Created by Nurik on 06.03.2017.
 */

public class EditActivity  extends AppCompatActivity {



    @BindView(R.id.seekbar)
    SeekBar seekbar;
    @BindView(R.id.seekbar_color)  SeekBar seekbarColor;
    @BindView(R.id.seekbar_rotate)  SeekBar seekbar_rotate;
    @BindView(R.id.label_color)
    TextView label_color;
    @BindView(R.id.label) TextView label;
    @BindView(R.id.label_rotate) TextView label_rotate;

    @BindView(R.id.mPreview)
    CropImageView mEffectPreview;

    @BindView(R.id.edit)
    TextView edit;

    @BindView(R.id.filter)
    TextView filter;

    @BindView(R.id.filter_layout)
    LinearLayout filter_layout;

    @BindView(R.id.edit_layout)
    LinearLayout edit_layout;

    @BindView(R.id.seekBar_layout)
    RelativeLayout seekBar_layout;

    @BindView(R.id.rotate_layout)
    RelativeLayout rotate_layout;

    @BindView(R.id.color_layout)
    LinearLayout color_layout;

    @BindView(R.id.button1)
    ImageButton button1;

    @BindView(R.id.mEffectChooserRecyclerView)
    RecyclerView mEffectChooserRecyclerView;

    //    private Filter mCurrentFilter = null;

    static float contrast = 1;
    static float brightness = 0;
    static float temperature = 0;

    Boolean is_contrast=false;
    Boolean is_brightness=false;
    Boolean is_temperature=false;
    static Bitmap changed_bitmap;

    int AA = 127,RR=0,GG=0,BB=0;

    File file;
    String path;

    int color_theme,color_theme2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_edit_image);
        ButterKnife.bind(this);

     //   button1.setBackgroundTintList(ColorStateList.valueOf(0xFF4CAF50));

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
        color_theme = typedValue.data;

        TypedValue typedValue2 = new TypedValue();
        Resources.Theme theme2 = getTheme();
        theme2.resolveAttribute(R.attr.colorText, typedValue2, true);
        color_theme2 = typedValue2.data;


        edit.setTextColor(color_theme2);
        filter.setTextColor(color_theme);

        filter_layout.setVisibility(View.GONE);
        edit_layout.setVisibility(View.VISIBLE);
        seekBar_layout.setVisibility(View.GONE);
        color_layout.setVisibility(View.GONE);
        rotate_layout.setVisibility(View.GONE);

        mEffectChooserRecyclerView.setHasFixedSize(true);
        mEffectChooserRecyclerView.setItemAnimator(new DefaultItemAnimator());
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mEffectChooserRecyclerView.setLayoutManager(mLayoutManager);


        path = getIntent().getStringExtra("path");
        file = new File(path);
        mEffectPreview.setImageURI(Uri.fromFile(file));
        Bitmap ret  = ((BitmapDrawable) mEffectPreview.getDrawable()).getBitmap();
        changed_bitmap = changeBitmapContrastBrightness(ret, contrast, brightness,temperature);

        seekbar.setMax(200);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {


                if(is_contrast){
                    contrast = (float) progress / 100;
                }
                if(is_brightness){
                    brightness = progress - 100;
                }
                if(is_temperature){
                    temperature = (float) (progress - 100)/4/200;
                }

                Log.d("CBCBCB",contrast+"//"+brightness+"//"+temperature);
                mEffectPreview.setImageBitmap(changeBitmapContrastBrightness(changed_bitmap, contrast, brightness,temperature));
                label.setText((progress - 100)+" ");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        seekbarColor.setMax(255);
        seekbarColor.setProgress(127);
        seekbarColor.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                AA = progress;
                label_color.setText(Math.round(progress/2.55)+" ");
                ColorFilter(AA,RR,GG,BB);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });



        seekbar_rotate.setMax(200);
        seekbar_rotate.setProgress(100);
        seekbar_rotate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                float angle = (progress - 100);
                label_rotate.setText(angle+" ");
                mEffectPreview.setImageBitmap(rotateImage(changed_bitmap,angle/10));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }



    @OnClick(R.id.filter)
    void FilterClick() {
        createImageFromBitmap(mEffectPreview.getImageBitmap());
//        ByteArrayOutputStream bs = new ByteArrayOutputStream();
//        mEffectPreview.getImageBitmap().compress(Bitmap.CompressFormat.PNG, 100, bs);
//        startActivity(new Intent(EditActivity.this, FilterActivity.class).putExtra("path", bs.toByteArray()));
    }

    @OnClick(R.id.back)
    void onClickback() {
        onBackPressed();
    }

    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
            Intent i = new Intent(EditActivity.this, FilterActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
            finish();

        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }
    public void FilterClick(View v) {
        switch (v.getId()){
            case R.id.button1:
                rotate_layout.setVisibility(View.VISIBLE);
                seekbar.setProgress(100);
                break;

            case R.id.button2:
                seekBar_layout.setVisibility(View.VISIBLE);
                is_brightness=true;
                is_contrast=false;
                is_temperature=false;
                seekbar.setProgress(100);
                break;

            case R.id.button3:
                seekBar_layout.setVisibility(View.VISIBLE);
                is_brightness=false;
                is_contrast=true;
                is_temperature=false;
                seekbar.setProgress(100);
                break;

            case R.id.button5:
                seekBar_layout.setVisibility(View.VISIBLE);
                is_temperature=true;
                is_brightness=false;
                is_contrast=false;
                seekbar.setProgress(100);
                break;

            case R.id.button6:
                color_layout.setVisibility(View.VISIBLE);
                break;

        }

    }

    @OnClick(R.id.ok)
    void okClick(){
        contrast = 1;
        brightness = 0;
        temperature=0;
        changed_bitmap = ((BitmapDrawable)mEffectPreview.getDrawable()).getBitmap();
        seekBar_layout.setVisibility(View.GONE);
    }
    @OnClick(R.id.close)
    void closeClick(){
        contrast = 1;
        brightness = 0;
        temperature=0;
        mEffectPreview.setImageBitmap(changed_bitmap);
        seekBar_layout.setVisibility(View.GONE);
    }


    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness, float temperature) {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, temperature, brightness,
                        0, contrast, 0, temperature/2, brightness,
                        0, 0, contrast, temperature/4, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);
        return ret;
    }

    public void Changecolor(View v) {
        if(seekbarColor.getVisibility()==View.GONE)
            seekbarColor.setVisibility(View.VISIBLE);
        switch (v.getId()) {

            case R.id.color1:
                AA=0;
                RR=0;
                GG=0;
                BB=0;
                break;
            case R.id.color2:
                AA = 127;
                RR=199;
                GG=194;
                BB=46;
                break;
            case R.id.color3:
                AA = 127;
                RR=199;
                GG=139;
                BB=46;
                break;
            case R.id.color4:
                AA = 127;
                RR=199;
                GG=46;
                BB=46;
                break;
            case R.id.color5:
                AA = 127;
                RR=196;
                GG=65;
                BB=126;
                break;
            case R.id.color6:
                AA = 127;
                RR=133;
                GG=46;
                BB=199;
                break;
            case R.id.color7:
                AA = 127;
                RR=46;
                GG=60;
                BB=199;
                break;
            case R.id.color8:
                AA = 127;
                RR=46;
                GG=171;
                BB=199;
                break;
            case R.id.color9:
                AA = 127;
                RR=46;
                GG=199;
                BB=60;

                break;
        }

        ColorFilter(AA,RR,GG,BB);

    }

    public void ColorFilter(int A,int R, int G, int B){

        Bitmap bm = Bitmap.createBitmap(changed_bitmap, 0, 0,
               changed_bitmap.getWidth(), changed_bitmap.getHeight());
        Paint paint = new Paint();
        ColorFilter filter = new PorterDuffColorFilter(Color.argb(A, R, G, B), PorterDuff.Mode.ADD);
        paint.setColorFilter(filter);

        Canvas canvas = new Canvas(bm);
        canvas.drawBitmap(bm, 0, 0, paint);
        mEffectPreview.setImageBitmap(bm);
    }

    @OnClick(R.id.ok_color)
    void ok_colorClick(){
        changed_bitmap  = ((BitmapDrawable) mEffectPreview.getDrawable()).getBitmap();
        mEffectPreview.setImageBitmap(changed_bitmap);

        color_layout.setVisibility(View.GONE);
    }
    @OnClick(R.id.close_color)
    void close_colorClick(){
        mEffectPreview.setImageBitmap(changed_bitmap);
        color_layout.setVisibility(View.GONE);
    }

    public static Bitmap rotateImage(Bitmap src,float degree)
    {

        // create new matrix
        Matrix matrix = new Matrix();
        // setup rotation degree
        matrix.postRotate(degree);
        Bitmap bmp = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
        return bmp;
    }

    public static Bitmap flip(Bitmap src, int type) {
        Matrix matrix = new Matrix();

        if(type == 0) {
//            Direction.VERTICAL
            matrix.preScale(1.0f, -1.0f);
        }
        else if(type == 1) {
//            Direction.HORIZONTAL
            matrix.preScale(-1.0f, 1.0f);
        } else if(type == 2) {
            matrix.postRotate(90);
        }else {
            return src;
        }

        return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
    }

    @OnClick(R.id.ok_rotate)
    void ok_rotateClick(){
        changed_bitmap = mEffectPreview.getCroppedImage();
        mEffectPreview.setImageBitmap(changed_bitmap);
        rotate_layout.setVisibility(View.GONE);
    }
    @OnClick(R.id.close_rotate)
    void close_rotateClick(){
        mEffectPreview.setImageBitmap(rotateImage(changed_bitmap,0));
        rotate_layout.setVisibility(View.GONE);
    }

    @OnClick(R.id.rotate1)
    void onRotate1Click(){
        final Bitmap bitmap = mEffectPreview.getImageBitmap();
        mEffectPreview.setImageBitmap(flip(bitmap,0));
    }

    @OnClick(R.id.rotate2)
    void onRotate2Click(){
        final Bitmap bitmap = mEffectPreview.getImageBitmap();
        mEffectPreview.setImageBitmap(flip(bitmap,2));
    }
    @OnClick(R.id.rotate3)
    void onRotate3Click(){
        final Bitmap bitmap = mEffectPreview.getImageBitmap();
        mEffectPreview.setImageBitmap(flip(bitmap,1));
    }


    @OnClick(R.id.next_btn)
    void next_click(){
       saveBitmap(mEffectPreview.getCroppedImage(),
                getLocalDir().getAbsolutePath() + "/" + FilterActivity.new_file_name);

    }


    private File saveBitmap(Bitmap bitmap, String path) {
        File file = null;
        if (bitmap != null) {
            file = new File(path);
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            startActivity(new Intent(EditActivity.this, UploadPhotot.class).putExtra("path",path).putExtra("content_type","PICTURE").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                            outputStream.flush();
                            outputStream.close();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }




}
