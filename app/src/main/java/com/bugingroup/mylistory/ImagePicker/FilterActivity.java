package com.bugingroup.mylistory.ImagePicker;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bugingroup.mylibrary.GPUImage;
import com.bugingroup.mylibrary.GPUImageFilter;
import com.bugingroup.mylibrary.GPUImageView;
import com.bugingroup.mylistory.PhotoVideo.GPUImageFilterTools;
import com.bugingroup.mylistory.PhotoVideo.GPUImageFilterTools.FilterAdjuster;
import com.bugingroup.mylistory.PhotoVideo.ImageEffectAdapter;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.Utils;

import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;

import static com.bugingroup.mylistory.PhotoVideo.GPUImageFilterTools.ArrayFilterList;


/**
 * Created by Nurik on 03.02.2017.
 */

public class FilterActivity extends AppCompatActivity  {

    /**   *   FILTERS  *   **/
    @BindView(R.id.mPreview) GPUImageView mEffectPreview;
    @BindView(R.id.hListView) HListView hListView;
    GPUImageFilter mCurrentFilter = null;

    GPUImageFilterTools.GPUImageFilterList filters;
    private FilterAdjuster mFilterAdjuster;

    @BindView(R.id.edit)
    TextView edit;

    @BindView(R.id.filter)
    TextView filter;
    int color_theme;
    int color_theme2;

    Bitmap bitmap;

    int last_position=0;
    public static String new_file_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.filter_activity);
        ButterKnife.bind(this);

        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(R.attr.colorMainIcon, typedValue, true);
        color_theme = typedValue.data;

        TypedValue typedValue2 = new TypedValue();
        Resources.Theme theme2 = getTheme();
        theme2.resolveAttribute(R.attr.colorText, typedValue2, true);
        color_theme2 = typedValue2.data;


      //  new_file_name = FileUtils.getNewFileName();
        try {
            bitmap =   BitmapFactory.decodeStream(openFileInput("myImage"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        filter.setTextColor(color_theme2);

        edit.setTextColor(color_theme);

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mEffectPreview.getLayoutParams();
        lp.height = getResources().getDisplayMetrics().widthPixels;
        mEffectPreview.setLayoutParams(lp);
        mEffectPreview.setScaleType(GPUImage.ScaleType.CENTER_CROP);

        mEffectPreview.setImage(bitmap);

        mEffectPreview.setOnTouchListener(setOnTouchListener());
        filters = ArrayFilterList(this);

        final ImageEffectAdapter effectAdapter = new ImageEffectAdapter(this,filters,bitmap);
        hListView.setAdapter(effectAdapter);


        hListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position!=0) {
                    switchFilterTo(filters.filters.get(position));
                    mEffectPreview.requestRender();
                }else {
                    mEffectPreview.setFilter(new GPUImageFilter(GPUImageFilter.NO_FILTER_VERTEX_SHADER, GPUImageFilter. NO_FILTER_FRAGMENT_SHADER));
                    mEffectPreview.requestRender();
                }
                if(last_position<position)
                    hListView.smoothScrollToPosition(position+1);
                else if(last_position>position)
                    hListView.smoothScrollToPosition(position-1);
                last_position=position;
            }
        });



    }
    private void switchFilterTo(final GPUImageFilter filter) {

            if (mCurrentFilter == null
                    || (filter != null && !mCurrentFilter.getClass().equals(filter.getClass()))) {
                mCurrentFilter = filter;
                mEffectPreview.setFilter(mCurrentFilter);
                mFilterAdjuster = new FilterAdjuster(mCurrentFilter);
            }

    }

    private View.OnTouchListener setOnTouchListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int action = motionEvent.getAction();
                if (mCurrentFilter != null && last_position!=0) {
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            mEffectPreview.setFilter(new GPUImageFilter(GPUImageFilter.NO_FILTER_VERTEX_SHADER, GPUImageFilter. NO_FILTER_FRAGMENT_SHADER));
                            mEffectPreview.requestRender();
                            break;
                        case MotionEvent.ACTION_UP:
                            mEffectPreview.setFilter(mCurrentFilter);
                            mEffectPreview.requestRender();
                            break;
                        default:
                            break;
                    }
                }

                return true;
            }
        };
    }



    @OnClick(R.id.edit)
    void EditClick(){
        mEffectPreview.saveToPictures("mylistory", new_file_name, new GPUImage.OnPictureSavedListener() {
            @Override
            public void onPictureSaved(Uri uri) {
                startActivity(new Intent(FilterActivity.this, EditActivity.class).putExtra("path",getPath(uri)).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                finish();
            }
        });

    }


    @OnClick(R.id.back)
    void onClickback() {
        onBackPressed();
    }


    @OnClick(R.id.next_btn)
    void next_click(){
        mEffectPreview.saveToPictures("mylistory", new_file_name, new GPUImage.OnPictureSavedListener() {
            @Override
            public void onPictureSaved(Uri uri) {
                startActivity(new Intent(FilterActivity.this, UploadPhotot.class).putExtra("path",getPath(uri)).putExtra("content_type","PICTURE").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            }
        });

    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

}
