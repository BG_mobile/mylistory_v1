package com.bugingroup.mylistory.ImagePicker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bugingroup.mylistory.ImagePicker.commons.ui.CropImageView;
import com.bugingroup.mylistory.ImagePicker.components.gallery.GalleryPickerFragmentListener;
import com.bugingroup.mylistory.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

/**
 * Created by Nurik on 27.01.2017.
 */
public class FragmentGalleryPicker extends Fragment {


    @BindView(R.id.title)
    TextView title;

    @BindString(R.string.tab_gallery)
    String _tabGallery;
    @BindView(R.id.mGallereyGridview)
    GridView mGallereyGridview;
    @BindView(R.id.mPreview)
    CropImageView mPreview;
    @BindView(R.id.mAppBarContainer)
    AppBarLayout mAppBarContainer;
    @BindView(R.id.mMediaNotFoundWording)
    TextView mMediaNotFoundWording;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private GalleryPickerFragmentListener listener;
    private ArrayList<File> mFiles;

    File selected_image;

    private static final String EXTENSION_JPG = ".jpg";
    private static final String EXTENSION_VIDEO = ".mp4";
    GridViewAdapter adapter;


    @BindView(R.id.video)
    VideoView videoView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery_picker, container, false);
        ButterKnife.bind(this,view);
        initViews();
        return view;
    }

    private void initViews() {
        title.setText(_tabGallery);
        ViewCompat.setNestedScrollingEnabled(mGallereyGridview,true);
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) mAppBarContainer.getLayoutParams();
        lp.height = getResources().getDisplayMetrics().widthPixels;
        mAppBarContainer.setLayoutParams(lp);

        mFiles = new ArrayList<>();
        adapter = new GridViewAdapter(getActivity(), mFiles);;
        mGallereyGridview.setAdapter(adapter);
        fetchMedia();

    }
    private void fetchMedia() {
        File dirDcim = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        parseDir(dirDcim);
        File dirPictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        parseDir(dirPictures);

        if (mFiles.size() > 0) {
            displayPreview(mFiles.get(0));
           adapter.notifyDataSetChanged();
        } else {
            mMediaNotFoundWording.setVisibility(View.VISIBLE);
        }
    }

    private void parseDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            parseFileList(files);
        }
    }

    private void parseFileList(File[] files) {
        for (File file : files) {
            if (file.isDirectory()) {
                if (!file.getName().toLowerCase().startsWith(".")) {
                    parseDir(file);
                }
            } else {
                if (file.getName().toLowerCase().endsWith(EXTENSION_JPG)
                        || file.getName().toLowerCase().endsWith(EXTENSION_VIDEO)
                        ) {
                    mFiles.add(file);
                    Log.d("FILE",file.getAbsolutePath());
                }
            }
        }
    }

    private void displayPreview(File file) {
        videoView.setVisibility(View.GONE);
        mPreview.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        selected_image = file;
        if(file.getName().toLowerCase().endsWith(EXTENSION_VIDEO)){
            mPreview.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            MediaController mediaController = new MediaController(getActivity());
            videoView.setVideoPath(file.getAbsolutePath());
            videoView.setMediaController(mediaController);
            mediaController.setMediaPlayer(videoView);
            mediaController.setVisibility(View.INVISIBLE);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    mp.setLooping(true);
                    mp.setVolume(50, 50);
                    progressBar.setVisibility(View.GONE);
                }
            });
        }else {
            videoView.setVisibility(View.GONE);
            compressWithRx(file);
        }
    }


    public class GridViewAdapter extends BaseAdapter {

        ArrayList<File>  feed_item;
        Activity activity;

        public GridViewAdapter(Activity activity, ArrayList<File> feed_item) {
            this.feed_item = feed_item;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {

            @BindView(R.id.imageView)
            ImageView image;

            @BindView(R.id.ic_video)
            ImageView ic_video;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

           GridViewAdapter.ViewHolder viewHolder;
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home16_item_grid, viewGroup, false);
                viewHolder = new GridViewAdapter.ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (GridViewAdapter.ViewHolder) convertView.getTag();
            }

            viewHolder.image.setVisibility(View.VISIBLE);
            if(feed_item.get(position).getName().toLowerCase().endsWith(EXTENSION_VIDEO)) {
                viewHolder.ic_video.setVisibility(View.VISIBLE);
               Bitmap thumb_bitmap = getThumbnail(feed_item.get(position).getAbsolutePath());
                viewHolder.image.setImageBitmap(thumb_bitmap);
            }else {
                viewHolder.ic_video.setVisibility(View.GONE);
              //  new LoadImage( viewHolder.image,feed_item.get(position)).execute();
                Picasso.with(getActivity()).load(feed_item.get(position)).centerCrop().resize(100,100).into(viewHolder.image);
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayPreview(feed_item.get(position));
                    mAppBarContainer.setExpanded(true, true);
                }
            });

            return convertView;
        }


    }


    @OnClick(R.id.next_btn)
    void onClickNext() {
        Log.d("AAAAA","SSSSSSS1");
        if(selected_image.getName().toLowerCase().endsWith(EXTENSION_VIDEO)) {
            startActivity(new Intent(getActivity(), UploadPhotot.class).putExtra("path", selected_image.getAbsolutePath()).putExtra("content_type", "VIDEO"));
        }else {
            createImageFromBitmap(getResizedBitmap(mPreview.getCroppedImage(),700,700));
        }


    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        float scaleWidth = 1;
        float scaleHeight = 1;
        int width = bm.getWidth();

        int height = bm.getHeight();

        if(width>newWidth)
         scaleWidth = ((float) newWidth) / width;
       if(height>newHeight)
        scaleHeight = ((float) newHeight) / height;

// CREATE A MATRIX FOR THE MANIPULATION

        Matrix matrix = new Matrix();

// RESIZE THE BITMAP

        matrix.postScale(scaleWidth, scaleHeight);

// RECREATE THE NEW BITMAP

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;

    }

    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = getActivity().openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
            Intent i = new Intent(getActivity(), FilterActivity.class);
            startActivity(i);

        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

    private void compressWithRx(File file) {
      Luban.with(getActivity())
                .load(file)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                     }

                    @Override
                    public void onSuccess(File file) {
                        ImageSize targetSize = new ImageSize(700, 700); // result Bitmap will be fit to this size
                        Bitmap bmp = ImageLoader.getInstance().loadImageSync(String.valueOf(Uri.fromFile(file)), targetSize);
                        mPreview.setImageBitmap(bmp);
                        progressBar.setVisibility(View.GONE);
                        mPreview.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }).launch();
    }

    @OnClick(R.id.back)
    void onClickback() {
       getActivity().onBackPressed();
    }

    public Bitmap getThumbnail(String filePath){

        Bitmap bit = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
        if(bit == null){
            MediaMetadataRetriever m = new MediaMetadataRetriever();
            m.setDataSource(filePath);
            bit = m.getFrameAtTime();
        }

        return bit;
    }

}
