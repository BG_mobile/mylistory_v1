package com.bugingroup.mylistory.ImagePicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.Utils;
import com.bugingroup.mylistory.videoRecorder.recoder.RecoderActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 27.01.2017.
 */

public class ImagePicker extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_picker);
        ButterKnife.bind(this);
        replaceFragment(new FragmentGalleryPicker());

    }



    @OnClick(R.id.gallerey)
    void gallerey_OnClick(){
        replaceFragment(new FragmentGalleryPicker());
    }
    @OnClick(R.id.camera)
    void camera_OnClick(){
        startActivity(new Intent(ImagePicker.this,TakePhoto.class));
        finish();
    }
    @OnClick(R.id.video)
    void video_OnClick(){
        startActivity(new Intent(ImagePicker.this,RecoderActivity.class));
        finish();

    }



    public void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment);
        fragmentTransaction.commit();
    }


}
