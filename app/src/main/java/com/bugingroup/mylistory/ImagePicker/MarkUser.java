package com.bugingroup.mylistory.ImagePicker;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.ImagePicker.Views.TagImageView;
import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.tag_library.DIRECTION;
import com.bugingroup.tag_library.TagViewGroup;
import com.bugingroup.tag_library.views.ITagView;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 08.02.2017.
 */
public class MarkUser extends AppCompatActivity  {

    @BindView(R.id.tagImageView)
    TagImageView mTagImageView;

    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.search_layout)
    LinearLayout search_layout;
    private int num;
    private List<TagGroupModel> mModelList = new ArrayList<>();
    private ListViewAdapter adapter;
    ArrayList<FeedItem> feedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mark_user);
        ButterKnife.bind(this);
        search_layout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        String path = getIntent().getStringExtra("path");

        File file = new File(path);
//        File file = new File("/storage/emulated/0/Pictures/mylistory/mylistory1486534445.jpg");
      //  Log.d("UUURRRLLL",getIntent().getStringExtra("path"));
        mTagImageView.setEditMode(true);
        mTagImageView.setImageURI(Uri.fromFile(file));
        searchView.setVoiceSearch(false);
        searchView.setEllipsize(true);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                findUsers(newText);
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                search_layout.setVisibility(View.GONE);
            }
        });





    }

    @OnClick(R.id.back)
    void BackClick(){
       finish();
    }

    @OnClick(R.id.tagImageView)
    void onImageViewClick() {
        Log.d("AAAAA","SEARCH");
        search_layout.setVisibility(View.VISIBLE);
        searchView.showSearch();
    }
    @OnClick(R.id.done)
    void onDoneClick() {
        for (int i=0;i<mModelList.size();i++){
            Log.d("MARKUSER",i+"*");
            Log.d("MARKUSER","X:"+mModelList.get(i).getPercentX());
            Log.d("MARKUSER","Y:"+mModelList.get(i).getPercentY());
        }

        UploadPhotot.mModelList = mModelList;
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();


    }



    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
            search_layout.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private TagViewGroup.OnTagGroupClickListener mTagGroupClickListener = new TagViewGroup.OnTagGroupClickListener() {
            @Override
            public void onCircleClick(TagViewGroup group) {
                Toast.makeText(MarkUser.this, "Click", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTagClick(TagViewGroup group, ITagView tag, int index) {
                tag.setDirection(DIRECTION.valueOf((num++ % 10 + 1)));
                mModelList.get(mTagImageView.getTagGroupIndex(group)).getTags().get(index).setDirection(tag.getDirection().getValue());
                group.requestLayout();
            }

            @Override
            public void onScroll(TagViewGroup group, float percentX, float percentY) {
                mModelList.get(mTagImageView.getTagGroupIndex(group)).setPercentX(percentX);
                mModelList.get(mTagImageView.getTagGroupIndex(group)).setPercentY(percentY);
            }

            @Override
            public void onLongPress(final TagViewGroup group) {
                new AlertDialog.Builder(MarkUser.this)
                        .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(getResources().getString(R.string.done), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mTagImageView.removeTagGroup(group);
                                mModelList.remove(mTagImageView.getTagGroupIndex(group));
                                dialog.dismiss();
                            }
                        }).setTitle(getResources().getString(R.string.delete)).setMessage(getResources().getString(R.string.delete_text))
                        .create().show();
            }
        };




    private void findUsers(String search_text) {

        progressBar.setVisibility(View.VISIBLE);
        RequestParams params = new RequestParams();
        params.put("start", "0");
        params.put("limit", "10");
        params.put("searchText", search_text);


        MainRestApi.get("findUsers",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
               Log.d("MARKUSER",response+"*");
                feedItem = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                for (int i = 0; i <list.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                    item.setUser_id(jsonObject.getString("id"));
                                    item.setName(jsonObject.has("name")?jsonObject.getString("name"):" ");
                                    item.setLogin(jsonObject.has("login")?jsonObject.getString("login"):" ");
                                    item.setSurname(jsonObject.has("surname")?jsonObject.getString("surname"):" ");
                                    JSONObject ava_jsonObject = new JSONObject("{\"avatar\":{ }}");
                                    JSONObject avatar = jsonObject.has("avatar") ? jsonObject.getJSONObject("avatar") : ava_jsonObject.getJSONObject("avatar");
                                    if (!avatar.has("headers")) {
                                        item.setAvatar_hasHeader(false);
                                    } else {
                                        item.setAvatar_hasHeader(true);
                                        JSONObject ava_headers = avatar.getJSONObject("headers");
                                        item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                        item.setAva_authorization(ava_headers.getString("Authorization"));
                                        item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                        item.setAva_Host(ava_headers.getString("Host"));
                                    }
                                    item.setAvatar_url(avatar.has("url") ? avatar.getString("url") : "no");
                                    item.setAva_resourceId(avatar.has("resourceId") ? avatar.getString("resourceId") : "no");
                                    feedItem.add(item);
                                }
                            }
                        }
                    }else {
                        Toast.makeText(MarkUser.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                    adapter = new ListViewAdapter(MarkUser.this,feedItem);
                    listView.setAdapter(adapter);
                    progressBar.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                System.out.println(errorResponse);
            }

        });

    }

    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(FragmentActivity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.user_name)
            TextView user_name;
            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;
            @BindView(R.id.avatar)
            CircleImageView avatar;
            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.user_list_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.subscripe_btn.setVisibility(View.GONE);
            item = feed_item.get(position);
            viewHolder.user_name.setText(item.getLogin());
        //    viewHolder.user_name.setText(item.getName() + " " + item.getSurname());

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", MainActivity.token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(), viewHolder.avatar, ava_options, animateFirstListener);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                   // String name = item.getName()+" "+item.getSurname();
                    String name = item.getLogin();
                    String user_id = item.getUser_id();
                    TagGroupModel model = new TagGroupModel();
                List<TagGroupModel.Tag> tagList = new ArrayList<>();

                if (!TextUtils.isEmpty(name)) {
                    TagGroupModel.Tag tag = new TagGroupModel.Tag();
                    tag.setName(name);
                    tagList.add(tag);
                }
                    tagList.get(0).setDirection(DIRECTION.RIGHT_CENTER.getValue());
                    model.getTags().addAll(tagList);
                    model.setPercentX(0.5f);
                    model.setUser_id(user_id);
                    model.setPercentY(0.5f);
                    mModelList.add(model);
                    mTagImageView.addTagGroup(model, mTagGroupClickListener);
                    search_layout.setVisibility(View.GONE);
                    searchView.closeSearch();
                }
            });

            return convertView;
        }
    }




}
