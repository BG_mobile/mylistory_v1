package com.bugingroup.mylistory.ImagePicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bugingroup.mylistory.ImagePicker.commons.ui.CropImageView;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.Utils;
import com.bugingroup.mylistory.videoRecorder.recoder.RecoderActivity;
import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraHostProvider;
import com.commonsware.cwac.camera.CameraView;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;



/**
 * Created by Nurik on 10.03.2017.
 */

public class TakePhoto extends Activity implements CameraHostProvider {
    @BindView(R.id.cameraView)
    CameraView cameraView;

    @BindView(R.id.mBtnTakePhoto)
    ImageView btnTakePhoto;

    @BindView(R.id.imageview)
    CropImageView imageview;

    @BindView(R.id.title)
    TextView title;

    @BindString(R.string.tab_photo)
    String _tabPhoto;

    @BindView(R.id.mFlashPhoto)
    ImageButton mFlashPhoto;

    @BindView(R.id.mSwitchCamera)
    ImageButton mSwitchCamera;


    private static final String[] FLASH_OPTIONS = {
           "auto",
            "off",
            "on"
    };

    private static final int[] FLASH_ICONS = {
            R.drawable.ic_flash_auto,
            R.drawable.ic_flash_off,
            R.drawable.ic_flash_on,
    };

    private int mCurrentFlash;

    Boolean is_front;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        is_front = getIntent().getBooleanExtra("is_front",false);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.take_photo);


        ButterKnife.bind(this);
        imageview.setVisibility(View.GONE);
        cameraView.setVisibility(View.VISIBLE);
        title.setText(_tabPhoto);
        cameraView.autoFocus();

        cameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraView.autoFocus();
            }
        });


    }


    @OnClick(R.id.video)
    void VideoClick(){
        startActivity(new Intent(TakePhoto.this,RecoderActivity.class));
        finish();
    }
    @OnClick(R.id.gallerey)
    void GallereyClick(){
        startActivity(new Intent(TakePhoto.this,ImagePicker.class));
        finish();

    }


    @OnClick(R.id.next_btn)
    void NextClick(){

        Bitmap b = imageview.getCroppedImage();
        Bitmap bitmap = Bitmap.createScaledBitmap(b, 600, 600, false);
        createImageFromBitmap(bitmap);
    }

    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
            Intent i = new Intent(TakePhoto.this, FilterActivity.class);
            startActivity(i);

        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

     @OnClick(R.id.back)
    void BackClick(){
       finish();
    }




    @OnClick(R.id.mBtnTakePhoto)
    public void onTakePhotoClick() {
        imageview.setVisibility(View.VISIBLE);
        cameraView.setVisibility(View.GONE);
        btnTakePhoto.setEnabled(false);
        cameraView.takePicture(true, false);

    }

    @OnClick(R.id.mSwitchCamera)
    void onSwitchCamera() {
        finish();
        Intent intent = new Intent(TakePhoto.this,TakePhoto.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("is_front",!is_front);
        startActivity(intent);

    }


    @OnClick(R.id.mFlashPhoto)
    void onChangeFlashState() {
        if (cameraView != null) {
            mCurrentFlash = (mCurrentFlash + 1) % FLASH_OPTIONS.length;
            mFlashPhoto.setImageResource(FLASH_ICONS[mCurrentFlash]);
            cameraView.setFlashMode(FLASH_OPTIONS[mCurrentFlash]);
        }
    }
    @Override
    public CameraHost getCameraHost() {

        return new MyCameraHost(this,is_front);
    }


    private class MyCameraHost extends SimpleCameraHost {

        private Camera.Size previewSize;
        Boolean is_front;

        MyCameraHost(Context ctxt, Boolean is_front) {
            super(ctxt);
            this.is_front = is_front;
        }

        @Override
        public boolean useFullBleedPreview() {
            return true;
        }

        @Override
        public boolean useFrontFacingCamera() {
            return is_front;
        }

        @Override
        public Camera.Size getPictureSize(PictureTransaction xact, Camera.Parameters parameters) {
            return previewSize;
        }

        @Override
        public Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters) {
            Camera.Parameters parameters1 = super.adjustPreviewParameters(parameters);
            previewSize = parameters1.getPreviewSize();
            return parameters1;
        }

        @Override
        public void saveImage(PictureTransaction xact, final Bitmap bitmap) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showTakenPicture(bitmap);

                }
            });
        }


    }
    private void showTakenPicture(Bitmap bitmap) {

        cameraView.setVisibility(View.GONE);
        System.out.println("------------------------------------------");

        imageview.setImageBitmap(getResizedBitmap(bitmap,700));
        imageview.setZoom(1);
        System.out.println("--------------------yyyyy----------------------");

    }

    public Bitmap getResizedBitmap(Bitmap bm, int maxSize) {
        int outWidth;
        int outHeight;
        int inWidth = bm.getWidth();
        int inHeight = bm.getHeight();
        if(inWidth > inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

        return Bitmap.createScaledBitmap(bm, outWidth, outHeight, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        cameraView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.onPause();
    }


}
