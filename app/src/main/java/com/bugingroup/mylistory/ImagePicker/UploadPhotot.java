package com.bugingroup.mylistory.ImagePicker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bugingroup.mylistory.AWS.ExampleMethod;
import com.bugingroup.mylistory.AWS.auth.AWS4SignerBase;
import com.bugingroup.mylistory.AWS.util.BinaryUtils;
import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Nurik on 09.01.2017.
 */
public class UploadPhotot extends AppCompatActivity {
     @BindView(R.id.mEffectPreview)
    ImageView mEffectPreview;

    @BindView(R.id.done)
    TextView done;

    @BindView(R.id.description)
    EditText description;

    @BindView(R.id.video)
    VideoView videoView;

    Bitmap thumb_bitmap;

    String screenResourceId;

    @BindView(R.id.mark_user)
    LinearLayout mark_user;

    String labels = "";
    File file;

    String path;

    String Content_Type;

    Boolean is_send = false;

    ProgressDialog progress;
    public static List<TagGroupModel> mModelList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.upload_photo_view);
        ButterKnife.bind(this);
        is_send = false;
        path = getIntent().getStringExtra("path");
        Log.d("PATH",getIntent().getStringExtra("path")+"*");

        file = new File(path);
        Log.d("PATH2",getIntent().getStringExtra("path"));

        Content_Type = getIntent().getStringExtra("content_type");
        Log.d("PPPPPPP:"+Content_Type,file.getPath());
//        Picasso.with(UploadPhotot.this).load(file)
//                .noFade()
//                .noPlaceholder()
//                .into(mEffectPreview);

        if(Content_Type.equals("VIDEO")){
            thumb_bitmap = getThumbnail(path);
            videoView.setVisibility(View.VISIBLE);
            mEffectPreview.setVisibility(View.GONE);
            mark_user.setVisibility(View.GONE);
            MediaController mediaController = new MediaController(this);
            videoView.setVideoPath(path);
            videoView.setMediaController(mediaController);
            mediaController.setMediaPlayer(videoView);
            mediaController.setVisibility(View.INVISIBLE);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    mp.setLooping(true);
                    mp.setVolume(0, 0);
                }
            });

        }
        else {
            mEffectPreview.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            mEffectPreview.setImageURI(Uri.fromFile(file));

        }


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                if(Content_Type.equals("VIDEO") && !is_send){
                    is_send=true;
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();
                    PostThumbAWS(byteArray);
                }else if(!is_send){
                    is_send=true;
                        Bitmap bmp = ((BitmapDrawable)mEffectPreview.getDrawable()).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.PNG, 80, stream);
                        byte[] byteArray = stream.toByteArray();

//                        InputStream iStream = getContentResolver().openInputStream(Uri.fromFile(file));
//                        byte[] inputData = getBytes(iStream);
                        PostAWS(byteArray);

                }
                progress = ProgressDialog.show(UploadPhotot.this, "Загрузить...",
                        "", true);


            }
        });

    }
    @OnClick(R.id.back)
    void onClickback() {
        onBackPressed();
    }

    @OnClick(R.id.mark_user)
    void onMarkUser(){
        if(Content_Type.equals("PICTURE")) {
            Intent intent = new Intent(UploadPhotot.this, MarkUser.class);
            intent.putExtra("path", path);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivityForResult(intent,704);
        }
    }



    private void PostThumbAWS(final byte[] inputData) {
        byte[] contentHash = AWS4SignerBase.hash(inputData);
        final String contentHashString = BinaryUtils.toHex(contentHash);
        String body = "{\"contentHashString\":"+contentHashString+"," +
                "\"contentLength\":"+inputData.length+"}";
            Log.d("ASASAS","FDFD"+body);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);
        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(UploadPhotot.this, URL_CONSTANT.AwsToken, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                Log.d("JSONOBJECT1:",response+"");
                try {
                    if(response.getBoolean("success")){
                        String authorization = response.getString("authorization");
                        final String resourceId = response.getString("resourceId");
                        final String x_amz_date = response.getString("x-amz-date");
                        final String Host = response.getString("Host");
                        screenResourceId = resourceId;
                        try {
                            InputStream iStream = getContentResolver().openInputStream(Uri.fromFile(file));
                            byte[] inputData = getBytes(iStream);
                            PostAWS(inputData);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        ExampleMethod.test(resourceId,authorization,inputData,x_amz_date,Host);
                    }else {
                        progress.dismiss();
                        Toast.makeText(UploadPhotot.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    private void PostAWS(final byte[] inputData) {
        byte[] contentHash = AWS4SignerBase.hash(inputData);
        final String contentHashString = BinaryUtils.toHex(contentHash);
        String body = "{\"contentHashString\":"+contentHashString+"," +
                "\"contentLength\":"+inputData.length+"}";
            Log.d("ASASAS","FDFD"+body);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);
        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(UploadPhotot.this, URL_CONSTANT.AwsToken, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                Log.d("JSONOBJECT1:",response+"");
                try {
                    if(response.getBoolean("success")){
                        String authorization = response.getString("authorization");
                        final String resourceId = response.getString("resourceId");
                        final String x_amz_date = response.getString("x-amz-date");
                        final String Host = response.getString("Host");
                        AddPost(resourceId);
                        ExampleMethod.test(resourceId,authorization,inputData,x_amz_date,Host);
                    }else {
                        progress.dismiss();
                        Toast.makeText(UploadPhotot.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void AddPost(String resourceId) {
        String body = "{\"resourceId\":\""+resourceId+"\",\"description\":\""+description.getText().toString()+"\",\"contentType\":\""+Content_Type+"\",\"longitude\":43.2332442,\"latitude\":76.9240593";
        Log.d("UploadPhoto2BODY1",body+"*");



        if(!labels.equals("")){
            body = body+labels;
        }else if(Content_Type.equals("VIDEO")){
            body = body+",\"screenResourceId\":\""+screenResourceId+"\"}";
        } else {
            body =body+"}";
        }
        Log.d("UploadPhoto2BODY2",body+"*");

      MainRestApi.post(UploadPhotot.this,"addPost",body,new JsonHttpResponseHandler() {
          @Override
          public void onSuccess(int statusCode, Header[] head, JSONObject response) {
              try {
                  Log.d("UploadPhoto2REs:",response+"");
                  if(response.getBoolean("success")){
                      Intent intent = new Intent(UploadPhotot.this, MainActivity.class);
                      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                      startActivity(intent);
                      finish();
                  }else {
                      Toast.makeText(UploadPhotot.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                  }
              } catch (JSONException e) {
                  e.printStackTrace();
              }
          }});


        progress.dismiss();
    }


    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK && requestCode==704){
            labels = ",\"labels\":[";
            if(mModelList.size()>0){
                labels = labels+ "{\"x\":"+mModelList.get(0).getPercentX()+",\"y\":"+mModelList.get(0).getPercentY()+",\"userId\":\""+mModelList.get(0).getUser_id()+"\"}";
            }
            for (int i=1;i<mModelList.size();i++){
                Log.d("MARKUSER2",i+"*");
                Log.d("MARKUSER2","X:"+mModelList.get(i).getPercentX());
                Log.d("MARKUSER2","Y:"+mModelList.get(i).getPercentY());
                labels = labels+ ",{\"x\":"+mModelList.get(i).getPercentX()+",\"y\":"+mModelList.get(i).getPercentY()+",\"userId\":\""+mModelList.get(i).getUser_id()+"\"}";

            }
            labels = labels+"]}";

            Log.d("MARKUSER2",labels);
        }
    }


    public Bitmap getThumbnail(String filePath){

        Bitmap bit = ThumbnailUtils.createVideoThumbnail(filePath, android.provider.MediaStore.Video.Thumbnails.MINI_KIND);
        if(bit == null){
            MediaMetadataRetriever m = new MediaMetadataRetriever();
            m.setDataSource(filePath);
            bit = m.getFrameAtTime();
        }

        return bit;
    }


}
