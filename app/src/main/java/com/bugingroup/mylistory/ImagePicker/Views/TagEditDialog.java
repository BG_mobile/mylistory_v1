package com.bugingroup.mylistory.ImagePicker.Views;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.R;
import com.bugingroup.tag_library.DIRECTION;

import java.util.ArrayList;
import java.util.List;

public class TagEditDialog extends Dialog implements View.OnClickListener {

    private EditText tag01;
    private Button cancel, confirm;
    private OnTagEditDialogClickListener listener;

    public TagEditDialog(@NonNull Context context, OnTagEditDialogClickListener listener) {
        super(context);
        setContentView(R.layout.dialog_tag_edit);
        this.listener = listener;
        tag01 = (EditText) findViewById(R.id.tag01);
        cancel = (Button) findViewById(R.id.cancelBtn);
        confirm = (Button) findViewById(R.id.confirmBtn);
        cancel.setOnClickListener(this);
        confirm.setOnClickListener(this);
    }

    public TagGroupModel createTagGroup() {
        TagGroupModel model = new TagGroupModel();
        List<TagGroupModel.Tag> tagList = new ArrayList<>();
        String str01 = tag01.getText().toString();
        if (!TextUtils.isEmpty(str01)) {
            TagGroupModel.Tag tag = new TagGroupModel.Tag();
            tag.setName(str01);
            tagList.add(tag);
        }
        setTagDirection(tagList);
        model.getTags().addAll(tagList);
        model.setPercentX(0.5f);
        model.setPercentY(0.5f);
        return model;
    }

    private void setTagDirection(List<TagGroupModel.Tag> tagList) {
        switch (tagList.size()) {
            case 1:
                tagList.get(0).setDirection(DIRECTION.RIGHT_CENTER.getValue());
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelBtn:
                listener.onCancel();
                break;
            case R.id.confirmBtn:
                listener.onTagGroupCreated(createTagGroup());
                break;
        }
    }

    public interface OnTagEditDialogClickListener {
        void onCancel();

        void onTagGroupCreated(TagGroupModel group);
    }
}
