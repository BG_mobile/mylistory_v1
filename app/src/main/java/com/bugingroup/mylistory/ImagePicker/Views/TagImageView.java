package com.bugingroup.mylistory.ImagePicker.Views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;
import com.bugingroup.mylistory.R;
import com.bugingroup.tag_library.DIRECTION;
import com.bugingroup.tag_library.TagViewGroup;
import com.bugingroup.tag_library.utils.AnimatorUtils;
import com.bugingroup.tag_library.views.TagTextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.yixia.camera.util.Log;

import java.util.ArrayList;
import java.util.List;


public class TagImageView extends FrameLayout {

    private ImageView mImageView;
    private FrameLayout mContentLayout;
    private List<TagGroupModel> mTagGroupModelList = new ArrayList<>();
    private List<TagViewGroup> mTagGroupViewList = new ArrayList<>();
    private boolean mIsEditMode;

    public TagImageView(Context context) {
        this(context, null);
    }

    public TagImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TagImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.layout_tag_imageview, this, true);
        mImageView = (ImageView) rootView.findViewById(R.id.imageview);
        mContentLayout = (FrameLayout) rootView.findViewById(R.id.tagsGroup);
    }

    public void setTagList(List<TagGroupModel> tagGroupList) {
        mTagGroupModelList.clear();
        mContentLayout.removeAllViews();
        mTagGroupViewList.clear();
            mTagGroupModelList.addAll(tagGroupList);
            for (TagGroupModel model : mTagGroupModelList) {
                TagViewGroup tagViewGroup = getTagViewGroup(model);
                tagViewGroup.setVisibility(INVISIBLE);
                tagViewGroup.setHiden(true);
                tagViewGroup.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                mContentLayout.addView(tagViewGroup);
                mTagGroupViewList.add(tagViewGroup);

            }
    }
    public void setTagList(List<TagGroupModel> tagGroupList,TagViewGroup.OnTagGroupClickListenerr listener) {
        mTagGroupModelList.clear();
        mContentLayout.removeAllViews();
        mTagGroupViewList.clear();

            for (TagGroupModel model : tagGroupList) {
                mTagGroupModelList.add(model);
                TagViewGroup tagViewGroup = getTagViewGroup(model);
                tagViewGroup.setOnTagGroupClickListener(listener);
                tagViewGroup.setVisibility(INVISIBLE);
                tagViewGroup.setHiden(true);
                tagViewGroup.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                mContentLayout.addView(tagViewGroup);
                mTagGroupViewList.add(tagViewGroup);

            }
    }

    public void Remove(){
        mTagGroupModelList.clear();
        mContentLayout.removeAllViews();
        mTagGroupViewList.clear();
    }

    public void addTagGroup(TagGroupModel model, TagViewGroup.OnTagGroupClickListener listener) {
        mTagGroupModelList.add(model);
        TagViewGroup tagViewGroup = getTagViewGroup(model);
        tagViewGroup.setOnTagGroupClickListener(listener);
        tagViewGroup.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mContentLayout.addView(tagViewGroup);
        mTagGroupViewList.add(tagViewGroup);
    }

    public int getTagGroupIndex(TagViewGroup tagGroup) {
        return mTagGroupViewList.indexOf(tagGroup);
    }


    public TagViewGroup getTagViewGroup(TagGroupModel model) {
        TagViewGroup tagViewGroup = new TagViewGroup(getContext());
        if (!mIsEditMode) {
            setTagGroupAnimation(tagViewGroup);
        }
        for (TagGroupModel.Tag tag : model.getTags()) {
            tagViewGroup.addTag(makeTagTextView(tag));
        }
        tagViewGroup.setPercent(model.getPercentX(), model.getPercentY());
        return tagViewGroup;
    }

    public TagTextView makeTagTextView(TagGroupModel.Tag tag) {
        TagTextView tagTextView = new TagTextView(getContext());
        tagTextView.setDirection(DIRECTION.valueOf(tag.getDirection()));
        tagTextView.setText(tag.getName());
        return tagTextView;
    }

    public void setTagGroupAnimation(TagViewGroup group) {
        group.setShowAnimator(AnimatorUtils.getTagShowAnimator(group))
                .setHideAnimator(AnimatorUtils.getTagHideAnimator(group)).addRipple();
    }



    public void excuteTagsAnimation() {
        for (TagViewGroup tagViewGroup : mTagGroupViewList) {
            if (tagViewGroup.isHiden()) {
                tagViewGroup.showWithAnimation();
            } else {
                tagViewGroup.hideWithAnimation();
            }
        }
    }

    public  Boolean isHidenGroup(){
        TagViewGroup tagViewGroup =mTagGroupViewList.get(0);
        return tagViewGroup.isHiden();
    }

    public void setEditMode(boolean editMode) {
        mIsEditMode = editMode;
    }

    public void setImageURI(Uri uri) {
        mImageView.setImageURI(uri);
    }

    public void setImageUrl(String url, DisplayImageOptions header) {
        ImageLoader.getInstance().displayImage(url, mImageView, header);

        ImageLoader.getInstance().displayImage(url, mImageView, header, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }
            //    Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

            }
        });
    }

    public void setImageUrl(String url, DisplayImageOptions header, final ProgressBar progressBar) {

        ImageLoader.getInstance().displayImage(url, mImageView, header, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                progressBar.setVisibility(VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                String message = null;
                switch (failReason.getType()) {
                    case IO_ERROR:
                        message = "Input/Output error";
                        break;
                    case DECODING_ERROR:
                        message = "Image can't be decoded";
                        break;
                    case NETWORK_DENIED:
                        message = "Downloads are denied";
                        break;
                    case OUT_OF_MEMORY:
                        message = "Out Of Memory error";
                        break;
                    case UNKNOWN:
                        message = "Unknown error";
                        break;
                }
          //      Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Log.d("ASASASASSSSSSSSSS",imageUri+"$$");
                progressBar.setVisibility(GONE);
            }
        });
    }


    public void removeTagGroup(TagViewGroup tagViewGroup) {
        mContentLayout.removeView(tagViewGroup);
    }

    public Bitmap getImageBitmap(){
        Bitmap bitmap = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();
        return bitmap;
    }
}
