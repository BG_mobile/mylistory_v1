package com.bugingroup.mylistory.ImagePicker.commons.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Nurik on 07.01.2017.
 */

public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private final V view;

    ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}
