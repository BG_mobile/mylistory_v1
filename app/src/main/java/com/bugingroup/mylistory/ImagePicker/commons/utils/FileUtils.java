package com.bugingroup.mylistory.ImagePicker.commons.utils;

/**
 * Created by Nurik on 07.01.2017.
 */
import android.os.Environment;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class FileUtils {

    private static final String DIR_YUMMYPETS = "/mylistory";

    public static File getLocalDir() {
        File dir = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                DIR_YUMMYPETS);
        if (!dir.exists())
        {
            dir.mkdirs();
        }
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                DIR_YUMMYPETS);
    }

    public static String getNewFilePath() {
        return getLocalDir().getAbsolutePath() + "/" + getNewFileName();
    }

    public static String getNewFileName() {
        return "mylistory" + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) + ".jpg";
    }

}
