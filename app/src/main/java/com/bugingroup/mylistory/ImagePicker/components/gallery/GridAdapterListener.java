package com.bugingroup.mylistory.ImagePicker.components.gallery;

import java.io.File;

public interface GridAdapterListener {

    void onClickMediaItem(File file);

}
