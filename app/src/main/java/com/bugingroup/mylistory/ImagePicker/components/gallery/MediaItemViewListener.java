package com.bugingroup.mylistory.ImagePicker.components.gallery;

import java.io.File;

interface MediaItemViewListener {
    void onClickItem(File file);
}
