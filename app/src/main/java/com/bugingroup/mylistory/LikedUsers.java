package com.bugingroup.mylistory;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome16;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/**
 * Created by nurbaqyt on 20.06.17.
 */

public class LikedUsers extends Fragment {


    @BindView(R.id.listView)
    ListView listView;

    String postId;
    ArrayList<FeedItem> feedItem;


    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.likeduser, container, false);
        ButterKnife.bind(this,view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            postId = bundle.getString("postId");
        }

        GetAllUsers();

        return view;
    }

    private void GetAllUsers() {
        feedItem = new ArrayList<FeedItem>();
        RequestParams params = new RequestParams();
        params.put("postId", postId);
        params.put("start","0");
        params.put("limit","40");
        MainRestApi.get("likedUsersList",params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){

                try {
                    if(response.getBoolean("success")){
                        JSONObject body = response.getJSONObject("body");
                        JSONArray list = body.getJSONArray("list");
                        for(int i=0;i<list.length();i++){
                            FeedItem item = new FeedItem();
                            JSONObject user = (JSONObject) list.get(i);
                            JSONObject jsonObject =new JSONObject("{\"avatar\":{ }}");
                            JSONObject avatar =user.has("avatar")?user.getJSONObject("avatar"):jsonObject.getJSONObject("avatar");

                            if(!avatar.has("headers")){
                                item.setAvatar_hasHeader(false);
                            }else {
                                item.setAvatar_hasHeader(true);
                                JSONObject ava_headers = avatar.getJSONObject("headers");
                                item.setAva_x_amz_content_sha256(ava_headers.getString("x-amz-content-sha256"));
                                item.setAva_authorization(ava_headers.getString("Authorization"));
                                item.setAva_x_amz_date(ava_headers.getString("x-amz-date"));
                                item.setAva_Host(ava_headers.getString("Host"));
                            }
                            item.setAvatar_url(avatar.has("url")?avatar.getString("url"):"no");
                            item.setAva_resourceId(avatar.has("resourceId")?avatar.getString("resourceId"):"no");
                            item.setUser_id(user.getString("id"));
                            item.setLogin(user.has("login")?user.getString("login"):"");
                            item.setName(user.has("name")?user.getString("name"):"");
                            feedItem.add(item);
                        }

                        ListViewAdapter adapter = new ListViewAdapter(getActivity(),feedItem);
                        listView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        ListViewAdapter(Activity activity, ArrayList<FeedItem> feed_list) {

            this.activity = activity;
            this.feed_item = feed_list;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.avatar)
            CircleImageView avatar;

            @BindView(R.id.user_name)
            TextView name;

            @BindView(R.id.subscripe_btn)
            Button subscripe_btn;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.user_list_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);

            viewHolder.name.setText(item.getLogin());
           viewHolder.subscripe_btn.setVisibility(View.GONE);

            Map<String, String> ava_headers = new HashMap<String, String>();
            if (item.getAvatar_hasHeader()) {
                ava_headers.put("x-amz-content-sha256", item.getAva_x_amz_content_sha256());
                ava_headers.put("x-amz-date", item.getAva_x_amz_date());
                ava_headers.put("Host", item.getAva_Host());
                ava_headers.put("Authorization", item.getAva_authorization());
            } else {
                ava_headers.put("auth", token);
            }

            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(ava_headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("userId", item.getUser_id());
                    Fragment fragment = new FragmentHome16();
                    fragment.setArguments(bundle);
                    ((MainActivity)getActivity()).replaceFragment(fragment);
                }
            });
            return convertView;
        }
    }


    @OnClick(R.id.back)
    void backClik(){
        getActivity().onBackPressed();
    }

}
