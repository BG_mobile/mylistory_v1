package com.bugingroup.mylistory;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome15;
import com.bugingroup.mylistory.FRAGMENT.like.FragmentLike;
import com.bugingroup.mylistory.FRAGMENT.profile.FragmentProfile43;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch24;
import com.bugingroup.mylistory.ImagePicker.ImagePicker;
import com.bugingroup.mylistory.start.Start1;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnLongClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 11.11.2016.
 */

public class MainActivity  extends AppCompatActivity {
        public static String token;
        public static String METHOD;
        String [] name;
        public static DialogPlus dialogPlus;

        @BindView(R.id.menu_home)
        ImageButton menu_home;
        @BindView(R.id.menu_search)
        ImageButton menu_search;
        @BindView(R.id.menu_upload)
        ImageButton menu_upload;
        @BindView(R.id.menu_favorites)
        ImageButton menu_favorites;
        @BindView(R.id.menu_profile)
        ImageButton menu_profile;

        SharedPreferences prefs;
        public static SharedPreferences prefsMain;



        static int t;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                Utils.onActivityCreateSetTheme(this);
                setContentView(R.layout.activity_main);
                ButterKnife.bind(this);

                MainRestApi.MainRestApi(MainActivity.this);
                prefs = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
                int user_num = prefs.getInt("user_number",1);
                Log.d("TOKENSSSSSS",user_num+"****" );
                System.out.println("TOKENSSSSSS"+user_num);

                switch (user_num){
                        case 1:
                                prefsMain = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
                                Log.d("MAPshare1","1111");
                                System.out.println("1111"+user_num);
                                break;
                        case 2:
                                prefsMain = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                                Log.d("MAPshare2","2222");
                                System.out.println("2222"+user_num);
                                break;
                }



                token = prefsMain.getString("token", null);
                 t = prefsMain.getInt("theme",0);
                Log.d("MAPshareTTTOKEN",token+"*");
                METHOD = prefsMain.getString("domain", null)+"/services/";
                Log.d("MAPshareMETHOD",METHOD);
                synchronizeUser();
                menu_home.setSelected(true);
                menu_home.setClickable(false);
                menu_search.setSelected(false);
                menu_search.setClickable(true);
                menu_favorites.setSelected(false);
                menu_favorites.setClickable(true);
                menu_profile.setSelected(false);
                menu_profile.setClickable(true);
                if(getIntent().hasExtra("profile")){
                        replaceFragment(new FragmentProfile43());
                }else{
                        replaceFragment(new FragmentHome15());
                }
                dialogPlus = DialogPlus.newDialog(MainActivity.this).create();


        }

        private void synchronizeUser() {
                RequestParams params = new RequestParams();
                params.put("userId","e51b2b0c-89e9-4e55-83c9-6e9f39e343c2");
                MainRestApi.post("synchronizeUser",params,new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                Log.d("synchronizeUser",response+"*");
                                Log.d("synchronizeUser","synchronizeUser  TRUE");
                        }
                });
        }

        @OnLongClick(R.id.menu_profile)
        public boolean onLongClick(View v) {
                SharedPreferences prefs1 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
                SharedPreferences prefs2 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                if(prefs1.contains("name") && prefs2.contains("name")) {
                        name = new String[2];
                        name[0] = prefs1.getString("name", "null");
                        name[1] = prefs2.getString("name", "null");


                        DialogPlus dialogPlus = DialogPlus.newDialog(this)
                                .setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, name))
                                .setCancelable(true)
                                .setOnItemClickListener(new OnItemClickListener() {
                                        @Override
                                        public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                                SharedPreferences.Editor editor1 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
                                                editor1.putInt("user_number", position + 1);
                                                editor1.commit();
                                                Intent intent = new Intent(MainActivity.this, Start1.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                        }
                                })
                                .create();

                        dialogPlus.show();
                }
                        return true;

        }


        //  Menu click
        public void MenuClick(View view) {
                Fragment fragment = null;
                switch (view.getId()) {
                        case R.id.menu_home:
                                menu_home.setSelected(true);
                                menu_search.setSelected(false);
                                menu_favorites.setSelected(false);
                                menu_profile.setSelected(false);
                                menu_home.setClickable(false);
                                menu_search.setClickable(true);
                                menu_favorites.setClickable(true);
                                menu_profile.setClickable(true);
                                fragment = new FragmentHome15();
                                break;
                        case R.id.menu_search:
                                menu_home.setSelected(false);
                                menu_search.setSelected(true);
                                menu_favorites.setSelected(false);
                                menu_profile.setSelected(false);
                                menu_home.setClickable(true);
                                menu_search.setClickable(true);
                                menu_favorites.setClickable(true);
                                menu_profile.setClickable(true);
                                fragment = new FragmentSearch24();
                                break;
                        case R.id.menu_upload:
                                startActivity(new Intent(MainActivity.this,ImagePicker.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                break;
                        case R.id.menu_favorites:
                                menu_home.setSelected(false);
                                menu_search.setSelected(false);
                                menu_favorites.setSelected(true);
                                menu_profile.setSelected(false);
                                menu_home.setClickable(true);
                                menu_search.setClickable(true);
                                menu_favorites.setClickable(false);
                                menu_profile.setClickable(true);
                                fragment = new FragmentLike();
                                break;
                        case R.id.menu_profile:
                                menu_home.setSelected(false);
                                menu_search.setSelected(false);
                                menu_favorites.setSelected(false);
                                menu_profile.setSelected(true);
                                menu_home.setClickable(true);
                                menu_search.setClickable(true);
                                menu_favorites.setClickable(true);
                                menu_profile.setClickable(false);
                                fragment = new FragmentProfile43();
                                break;
                }
                if(fragment!=null){
                        replaceFragment(fragment);
                }

        }

        public  void replaceFragment(Fragment fragment){
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
        }



        @Override
        public void onBackPressed() {
                if(dialogPlus.isShowing()){
                        dialogPlus.dismiss();
                }else {
                        super.onBackPressed();
                }
        }
}
