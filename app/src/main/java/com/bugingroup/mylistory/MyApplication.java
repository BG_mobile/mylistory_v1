package com.bugingroup.mylistory;


import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;

import com.bugingroup.mylistory.Custom.CustomImageDownaloder;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.yixia.camera.VCamera;
import com.yixia.camera.util.DeviceUtils;

import java.io.File;

/**
 * Created by Nurik on 26.01.2017.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        if (URL_CONSTANT.Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build());
        }
        super.onCreate();
        FacebookSdk.sdkInitialize(this);
        initImageLoader(getApplicationContext());

        File dcim = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (DeviceUtils.isZte()) {
            if (dcim.exists()) {
                VCamera.setVideoCachePath(dcim + "/mylistory/");
            } else {
                VCamera.setVideoCachePath(dcim.getPath().replace("/sdcard/",
                        "/sdcard-ext/")
                        + "/mylistory/");
            }
        } else {
            VCamera.setVideoCachePath(dcim + "/mylistory/");
        }

        VCamera.setDebugMode(true);
//        if (Build.VERSION.SDK_INT >= 21) {
//            VCamera.initialize(this);
//        }
        VCamera.initialize(this);

    }
    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2)
        .denyCacheImageMultipleSizesInMemory()
        .diskCacheFileNameGenerator(new Md5FileNameGenerator())
        .diskCacheSize(50 * 1024 * 1024) // 50 MiB
        .tasksProcessingOrder(QueueProcessingType.LIFO)
        .imageDownloader(new CustomImageDownaloder(context))
        .writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

}