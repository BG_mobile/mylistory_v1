package com.bugingroup.mylistory.PhotoVideo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bugingroup.mylibrary.GPUImageView;
import com.bugingroup.mylistory.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 03.02.2017.
 */
public class ImageEffectAdapter extends BaseAdapter {


    GPUImageFilterTools.GPUImageFilterList feed_item;
    Bitmap bitmap;
    Activity activity;

    public ImageEffectAdapter(Activity activity, GPUImageFilterTools.GPUImageFilterList feed_item, Bitmap bitmap) {
        this.feed_item = feed_item;
        this.activity = activity;
        this.bitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, false);
    }

    @Override
    public int getCount() {
        return feed_item.filters.size();
    }

    @Override
    public Object getItem(int location) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder {

        @BindView(R.id.gpuimage)
        GPUImageView gpuimage;

        @BindView(R.id.name)
        TextView name;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.filter_item_grid, viewGroup, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.gpuimage.setImage(bitmap);
        viewHolder.gpuimage.setFilter(feed_item.filters.get(position));
        viewHolder.name.setText(feed_item.names.get(position));
        return convertView;
    }


}
