package com.bugingroup.mylistory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bugingroup.mylistory.FRAGMENT.home.FragmentHome16;
import com.bugingroup.mylistory.FRAGMENT.search.FragmentSearch25;
import com.bugingroup.mylistory.start.Start1;
import com.bugingroup.mylistory.utils.Utils;

/**
 * Created by Nurik on 21.02.2017.
 */

public class PushReceived extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_push_received);

        String id = getIntent().getStringExtra("id");
        String operation = getIntent().getStringExtra("operation");
        Bundle bundle = new Bundle();
        Fragment fragment = null;

        switch (operation){
            case "like":
            case "comment":
                bundle.putString("postId", id);
                fragment = new FragmentSearch25();
                fragment.setArguments(bundle);
                break;
            case "subscribe":
                bundle.putString("userId",id);
                 fragment = new FragmentHome16();
                fragment.setArguments(bundle);
                break;
        }
        if (fragment!=null)
        replaceFragment(fragment);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(PushReceived.this, Start1.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    public void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
