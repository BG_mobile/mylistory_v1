package com.bugingroup.mylistory;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.ImagePicker.Views.TagImageView;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.token;

/*
 * Created by Nurik on 23.02.2017.
 */

public class SingleChat extends AppCompatActivity {

    @BindView(R.id.message)
    EditText message;

    @BindView(R.id.name)
    TextView Name;

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.avatar)
    CircleImageView avatar;

    private ListViewAdapter adapter;
    ImageButton add,back;

    String chatId,userId,name,avatar_url;

    ArrayList<FeedItem> feedItem;
    private Handler handler;
    private Runnable runable;
    String last_id="0";
    Boolean isFirst = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.single_chat);
        ButterKnife.bind(this);


        userId = getIntent().getStringExtra("userId");
        name = getIntent().hasExtra("name")?getIntent().getStringExtra("name"):"";
        avatar_url = getIntent().hasExtra("avatar_url")?getIntent().getStringExtra("avatar_url"):"";
        HashMap<String, String> ava_headers  = (HashMap<String, String>) getIntent().getSerializableExtra("ava_headers");

        final DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                .extraForDownloader(ava_headers)
                .showImageOnLoading(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();


        ImageLoader.getInstance().displayImage(avatar_url, avatar, ava_options);
        Name.setText(name);


        if(getIntent().hasExtra("notifi")){
            final ProgressDialog progressDialog = new ProgressDialog(SingleChat.this);
            progressDialog.show();

            String body = "{\"name\":\"nurik\", \"users\":[\""+userId+"\"]}";

            MainRestApi.post(SingleChat.this,"addChat",body,new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    try {
                        Log.d("RESPONS",response+"*");
                        if(response.getBoolean("success")){
                            chatId =  response.getString("body");
                            messageList(chatId);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }finally {
                        progressDialog.dismiss();
                    }

                }
            });
        }else {
            chatId = getIntent().getStringExtra("chatId");
            messageList(chatId);
        }






        handler = new Handler();
        runable = new Runnable() {

            @Override
            public void run() {

                messageList(chatId);

                Log.d("AAA","FDF");
                //also call the same runnable
                handler.postDelayed(this, 10000);
            }
        };
        handler.postDelayed(runable,10000);

    }

    @OnClick(R.id.back)
    void BackClick(){
        handler.removeCallbacks(runable);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runable);
        finish();
    }

    @OnClick(R.id.ic_send)
    void SendClick(){
        if(message.getText().length()>0) {
            String body = "{\"chatId\":\"" + chatId + "\",  \"description\":\"" + message.getText().toString() + "\", \"contentType\":\"TEXT\", \"resourceId\": null}";

            MainRestApi.post(SingleChat.this, "sendMessage", body, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Log.d("RESPONS", response + "*");
                        if (response.getBoolean("success")) {
                            message.setText(null);
                            messageList(chatId);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void messageList(String chatId) {


        RequestParams params = new RequestParams();
        params.put("chatId", chatId);
        params.put("limit", "100");



        MainRestApi.get("messageList",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("messageList",response+"*");
                feedItem = new ArrayList<FeedItem>();
                String chat_id = null;
                try {
                    if(response.getBoolean("success")){
                        if(response.has("body")) {
                            JSONObject body = response.getJSONObject("body");
                            if(body.has("list")) {
                                JSONArray list = body.getJSONArray("list");
                                chat_id = ((JSONObject) list.get(list.length()-1)).getString("id");
                                for (int i = 0; i <list.length(); i++) {

                                    JSONObject jsonObject = (JSONObject) list.get(i);
                                    FeedItem item = new FeedItem();
                                    if(i==list.length()-1 && !last_id.equals("0")){
                                            last_id = jsonObject.getString("id");
                                    }
                                    item.setChatId(jsonObject.getString("id"));
                                    item.setFrom(jsonObject.getString("from"));
                                    item.setCreateDate(jsonObject.getLong("createDate"));
                                    item.setPost_id(jsonObject.has("postId")?jsonObject.getString("postId"):"no");
                                    JSONObject content = jsonObject.getJSONObject("content");
                                    item.setContentType(content.getString("contentType"));
                                    switch (content.getString("contentType")){
                                        case "PICTURE":
                                            if(!content.has("headers")){
                                                item.setHasHeader(false);
                                            }else {
                                                JSONObject header = content.getJSONObject("headers");
                                                item.setX_amz_content_sha256(header.getString("x-amz-content-sha256"));
                                                item.setAuthorization(header.getString("Authorization"));
                                                item.setX_amz_date(header.getString("x-amz-date"));
                                                item.setHost(header.getString("Host"));
                                                item.setHasHeader(true);
                                            }
                                            item.setUrl(content.has("url")?content.getString("url"):"no");
                                            item.setResourceId(content.has("resourceId")?content.getString("resourceId"):"no");
                                            break;
                                        case "TEXT":
                                            item.setDescription(content.has("description")?content.getString("description"):"");
                                            break;
                                        case "VIDEO":
                                            break;

                                    }

                                    feedItem.add(item);
                                }
                            }
                        }
                    }else {
                        Toast.makeText(SingleChat.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

//                    Log.d("SSSSSSS0",isFirst+"*"+chat_id+"**"+last_id);
//                    if(isFirst) {
                        adapter = new ListViewAdapter(SingleChat.this, feedItem);
                        listView.setAdapter(adapter);
                        isFirst =false;
                        Log.d("SSSSSSS1",isFirst+"*"+chat_id+"**"+last_id);
//                    }else if(!chat_id.equals(last_id) ) {
//                        adapter = new ListViewAdapter(SingleChat.this, feedItem);
//                        listView.setAdapter(adapter);
//                        last_id=chat_id;
//                        Log.d("SSSSSSS2",isFirst+"*"+chat_id+"**"+last_id);
//                    }else {
//                        adapter.notifyDataSetChanged();
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
            }

        });

    }
    //  ListViewAdapter
    public class ListViewAdapter extends BaseAdapter {

        ArrayList<FeedItem> feed_item;
        FeedItem item;
        Activity activity;

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();


        public ListViewAdapter(Activity activity, ArrayList<FeedItem> feedItem) {
            this.activity = activity;
            this.feed_item = feedItem;
        }


        @Override
        public int getCount() {
            return feed_item.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {

            @BindView(R.id.layout_m)
            RelativeLayout layout_m;

            @BindView(R.id.layout_y)
            RelativeLayout layout_y;

            @BindView(R.id.message_m)
            TextView message_m;

            @BindView(R.id.message_y)
            TextView message_y;

            @BindView(R.id.date_m)
            TextView date_m;

            @BindView(R.id.date_y)
            TextView date_y;

            @BindView(R.id.contents_image_m)
            TagImageView contents_image_m;

            @BindView(R.id.contents_image_y)
            TagImageView contents_image_y;


            @BindView(R.id.ic_video_m)
            ImageView ic_video_m;

            @BindView(R.id.ic_video_y)
            ImageView ic_video_y;




            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.home21_item_list, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            item = feed_item.get(position);
            if (Build.VERSION.SDK_INT <= 20) {
                viewHolder.message_m.setTextColor(getResources().getColor(R.color.black));
                viewHolder.message_y.setTextColor(getResources().getColor(R.color.black));
                viewHolder.date_m.setTextColor(getResources().getColor(R.color.black));
                viewHolder.date_y.setTextColor(getResources().getColor(R.color.black));
            }


            if(item.getFrom().equals(userId)){
                viewHolder.layout_y.setVisibility(View.VISIBLE);
                viewHolder.layout_m.setVisibility(View.GONE);
               if(item.getContentType().equals("PICTURE")) {
                   viewHolder.message_y.setVisibility(View.GONE);
                   Map<String, String> headers_content = new HashMap<String, String>();
                   if (item.getHasHeader()) {
                       headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                       headers_content.put("x-amz-date", item.getX_amz_date());
                       headers_content.put("Host", item.getHost());
                       headers_content.put("Authorization", item.getAuthorization());
                   } else {
                       headers_content.put("auth", token);
                   }
                   viewHolder.contents_image_y.setVisibility(View.VISIBLE);
                   DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                           .extraForDownloader(headers_content)
                           .showImageOnLoading(R.drawable.loading)
                           .cacheInMemory(true)
                           .cacheOnDisk(true)
                           .considerExifParams(true)
                           .build();

                   viewHolder.contents_image_y.setImageUrl(item.getUrl(), header_options);
               }else if(item.getContentType().equals("TEXT")) {
                   viewHolder.contents_image_y.setVisibility(View.GONE);
                   viewHolder.message_y.setText(item.getDescription());
                   viewHolder.date_y.setText(URL_CONSTANT.getTimeAgo(item.getCreateDate(), SingleChat.this));
               }

            }else {
                viewHolder.layout_m.setVisibility(View.VISIBLE);
                viewHolder.layout_y.setVisibility(View.GONE);
               if(item.getContentType().equals("PICTURE")) {
                   Map<String, String> headers_content = new HashMap<String, String>();
                   if (item.getHasHeader()) {
                       headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                       headers_content.put("x-amz-date", item.getX_amz_date());
                       headers_content.put("Host", item.getHost());
                       headers_content.put("Authorization", item.getAuthorization());
                   } else {
                       headers_content.put("auth", token);
                   }
                   viewHolder.contents_image_m.setVisibility(View.VISIBLE);
                   DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                           .extraForDownloader(headers_content)
                           .showImageOnLoading(R.drawable.loading)
                           .cacheInMemory(true)
                           .cacheOnDisk(true)
                           .considerExifParams(true)
                           .build();

                   viewHolder.contents_image_m.setImageUrl(item.getUrl(), header_options);
               }else if(item.getContentType().equals("TEXT")) {
                   viewHolder.contents_image_m.setVisibility(View.GONE);
                   viewHolder.message_m.setText(item.getDescription());
                   viewHolder.date_m.setText(URL_CONSTANT.getTimeAgo(item.getCreateDate(), SingleChat.this));
               }
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = feed_item.get(position);
                    if(item.getContentType().equals("PICTURE")){
                        if(item.getPost_id().equals("no")){
                            final Dialog dialog = new Dialog(SingleChat.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_chat_picture);
                            TagImageView image = (TagImageView) dialog.findViewById(R.id.image);
                            Map<String, String> headers_content = new HashMap<String, String>();
                            if (item.getHasHeader()) {
                                headers_content.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                                headers_content.put("x-amz-date", item.getX_amz_date());
                                headers_content.put("Host", item.getHost());
                                headers_content.put("Authorization", item.getAuthorization());
                            } else {
                                headers_content.put("auth", token);
                            }
                            DisplayImageOptions header_options = new DisplayImageOptions.Builder()
                                    .extraForDownloader(headers_content)
                                    .showImageOnLoading(R.drawable.loading)
                                    .cacheInMemory(true)
                                    .cacheOnDisk(true)
                                    .considerExifParams(true)
                                    .build();

                            image.setImageUrl(item.getUrl(), header_options);
                            dialog.show();
                        }else {
                            Intent intent = new Intent(SingleChat.this, PushReceived.class);
                            intent.putExtra("id", item.getPost_id());
                            intent.putExtra("operation", "comment");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                        }
                    }
                }
            });

            return convertView;
        }


    }


}