package com.bugingroup.mylistory.models;

import com.bugingroup.mylistory.ImagePicker.model.TagGroupModel;

import java.util.ArrayList;

/**
 * Created by Nurik on 05.01.2017.
 */

public class FeedItem {
    private String user_id,post_id,request_id,avatar, surname,name,description,headers,
            url,contentType,avatar_url,scren_url;
    private Boolean ignored,hasHeader,scren_hasHeader,avatar_hasHeader,liked,followed;
    private int likeCount,commentCount;
    private long createDate,likeDate;
    //  For avatar
    private String scren_resourceId ,scren_x_amz_content_sha256,scren_authorization,scren_x_amz_date,scren_Host;
    private String resourceId ,x_amz_content_sha256,authorization,x_amz_date,Host;
    private String ava_resourceId,ava_x_amz_content_sha256,ava_authorization,ava_x_amz_date,ava_Host,Hashtag;

    String login;
    //CHAT
    private String chatId,from;

    ArrayList<FeedItem> postsItem;
    ArrayList<TagGroupModel> labels;

    public ArrayList<TagGroupModel> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<TagGroupModel> labels) {
        this.labels = labels;
    }

    public ArrayList<FeedItem> getPostsItem() {
        return postsItem;
    }

    public void setPostsItem(ArrayList<FeedItem> postsItem) {
        this.postsItem = postsItem;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLikeDate() {
        return likeDate;
    }

    public void setLikeDate(long likeDate) {
        this.likeDate = likeDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public String getHashtag() {
        return Hashtag;
    }

    public void setHashtag(String hashtag) {
        Hashtag = hashtag;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public Boolean getIgnored() {
        return ignored;
    }

    public void setIgnored(Boolean ignored) {
        this.ignored = ignored;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getAva_resourceId() {
        return ava_resourceId;
    }

    public void setAva_resourceId(String ava_resourceId) {
        this.ava_resourceId = ava_resourceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public Boolean getHasHeader() {
        return hasHeader;
    }

    public void setHasHeader(Boolean hasHeader) {
        this.hasHeader = hasHeader;
    }

    public Boolean getAvatar_hasHeader() {
        return avatar_hasHeader;
    }

    public void setAvatar_hasHeader(Boolean avatar_hasHeader) {
        this.avatar_hasHeader = avatar_hasHeader;
    }

    public String getX_amz_content_sha256() {
        return x_amz_content_sha256;
    }

    public void setX_amz_content_sha256(String x_amz_content_sha256) {
        this.x_amz_content_sha256 = x_amz_content_sha256;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getX_amz_date() {
        return x_amz_date;
    }

    public void setX_amz_date(String x_amz_date) {
        this.x_amz_date = x_amz_date;
    }

    public String getHost() {
        return Host;
    }

    public void setHost(String host) {
        Host = host;
    }

    public String getAva_x_amz_content_sha256() {
        return ava_x_amz_content_sha256;
    }

    public void setAva_x_amz_content_sha256(String ava_x_amz_content_sha256) {
        this.ava_x_amz_content_sha256 = ava_x_amz_content_sha256;
    }

    public String getAva_authorization() {
        return ava_authorization;
    }

    public void setAva_authorization(String ava_authorization) {
        this.ava_authorization = ava_authorization;
    }

    public String getAva_x_amz_date() {
        return ava_x_amz_date;
    }

    public void setAva_x_amz_date(String ava_x_amz_date) {
        this.ava_x_amz_date = ava_x_amz_date;
    }

    public String getAva_Host() {
        return ava_Host;
    }

    public void setAva_Host(String ava_Host) {
        this.ava_Host = ava_Host;
    }


    public Boolean getFollowed() {
        return followed;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }


    public String getScren_resourceId() {
        return scren_resourceId;
    }

    public void setScren_resourceId(String scren_resourceId) {
        this.scren_resourceId = scren_resourceId;
    }

    public String getScren_x_amz_content_sha256() {
        return scren_x_amz_content_sha256;
    }

    public void setScren_x_amz_content_sha256(String scren_x_amz_content_sha256) {
        this.scren_x_amz_content_sha256 = scren_x_amz_content_sha256;
    }

    public String getScren_authorization() {
        return scren_authorization;
    }

    public void setScren_authorization(String scren_authorization) {
        this.scren_authorization = scren_authorization;
    }

    public String getScren_x_amz_date() {
        return scren_x_amz_date;
    }

    public void setScren_x_amz_date(String scren_x_amz_date) {
        this.scren_x_amz_date = scren_x_amz_date;
    }

    public String getScren_Host() {
        return scren_Host;
    }

    public void setScren_Host(String scren_Host) {
        this.scren_Host = scren_Host;
    }

    public Boolean getScren_hasHeader() {
        return scren_hasHeader;
    }

    public void setScren_hasHeader(Boolean scren_hasHeader) {
        this.scren_hasHeader = scren_hasHeader;
    }

    public String getScren_url() {
        return scren_url;
    }

    public void setScren_url(String scren_url) {
        this.scren_url = scren_url;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
