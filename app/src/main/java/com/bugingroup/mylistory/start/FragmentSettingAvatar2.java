package com.bugingroup.mylistory.start;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bugingroup.mylistory.ImagePicker.commons.bus.RxBusNext;
import com.bugingroup.mylistory.ImagePicker.commons.models.Session;
import com.bugingroup.mylistory.ImagePicker.commons.ui.CropImageView;
import com.bugingroup.mylistory.ImagePicker.commons.utils.FileUtils;
import com.bugingroup.mylistory.ImagePicker.components.gallery.GridAdapter;
import com.bugingroup.mylistory.ImagePicker.components.gallery.GridAdapterListener;
import com.bugingroup.mylistory.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

import static com.bugingroup.mylistory.start.SettingAvatar2.avatar_3;

/**
 * Created by Nurik on 16.01.2017.
 */
public class FragmentSettingAvatar2 extends Fragment implements GridAdapterListener {

    @BindView(R.id.mGalleryRecyclerView)
    RecyclerView mGalleryRecyclerView;
    @BindView(R.id.mPreview)
    CropImageView mPreview;
    @BindView(R.id.mAppBarContainer)
    AppBarLayout mAppBarContainer;
    @BindView(R.id.mMediaNotFoundWording)
    TextView mMediaNotFoundWording;

    private static final String EXTENSION_JPG = ".jpg";
    private static final int MARGING_GRID = 2;

    private Session mSession = Session.getInstance();
    private final RxBusNext mRxBus = RxBusNext.getInstance();

    private GridAdapter mGridAdapter;
    private ArrayList<File> mFiles;
    private boolean isFirstLoad = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting_avatar, container, false);
        ButterKnife.bind(this, v);
        initViews();
        return v;
    }

    private void initViews() {
        if (isFirstLoad) {
            mGridAdapter = new GridAdapter(getActivity());
        }
        mGridAdapter.setListener(this);
        mGalleryRecyclerView.setAdapter(mGridAdapter);
        mGalleryRecyclerView.setHasFixedSize(true);
        mGalleryRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mGalleryRecyclerView.addItemDecoration(addItemDecoration());

        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) mAppBarContainer.getLayoutParams();
        lp.height = getResources().getDisplayMetrics().widthPixels;
        mAppBarContainer.setLayoutParams(lp);

        fetchMedia();
    }

    private RecyclerView.ItemDecoration addItemDecoration() {
        return new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view,
                                       RecyclerView parent, RecyclerView.State state) {
                outRect.left = MARGING_GRID;
                outRect.right = MARGING_GRID;
                outRect.bottom = MARGING_GRID;
                if (parent.getChildLayoutPosition(view) >= 0 && parent.getChildLayoutPosition(view) <= 3) {
                    outRect.top = MARGING_GRID;
                }
            }
        };
    }

    private void fetchMedia() {
        mFiles = new ArrayList<>();
        File dirDownloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        parseDir(dirDownloads);
        File dirDcim = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        parseDir(dirDcim);
        File dirPictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        parseDir(dirPictures);
        File dirDocuments = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        parseDir(dirDocuments);

        if (mFiles.size() > 0) {
            displayPreview(mFiles.get(0));
            mGridAdapter.setItems(mFiles);
        } else {
            mMediaNotFoundWording.setVisibility(View.VISIBLE);
        }
        isFirstLoad = false;
    }

    private void parseDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            parseFileList(files);
        }
    }
    private void parseFileList(File[] files) {
        for (File file : files) {
            if (file.isDirectory()) {
                if (!file.getName().toLowerCase().startsWith(".")) {
                    parseDir(file);
                }
            } else {
                if (file.getName().toLowerCase().endsWith(EXTENSION_JPG)) {
                    mFiles.add(file);
                }
            }
        }
    }

    private void displayPreview(File file) {

        Picasso.with(getActivity())
                .load(Uri.fromFile(file))
                .noFade()
                .noPlaceholder()
                .resize(500,500)
                .centerCrop()
                .into(mPreview);

        Picasso.with(getActivity())
                .load(Uri.fromFile(file))
                .noFade()
                .resize(80,80)
                .centerCrop()
                .noPlaceholder()
                .into(avatar_3);
    }

    private void eventBus() {
        mRxBus.toObserverable()
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        mSession.setFileToUpload(saveBitmap(getResizedBitmap(mPreview.getCroppedImage(),500,500),
                                FileUtils.getNewFilePath()));
                    }
                });
    }

    private File saveBitmap(Bitmap bitmap, String path) {
        File file = null;
        if (bitmap != null) {
            file = new File(path);
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 85, outputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onPause() {
        super.onPause();
        Picasso.with(getActivity()).cancelRequest(mPreview);
        mRxBus.reset();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClickMediaItem(File file) {
        displayPreview(file);
        mAppBarContainer.setExpanded(true, true);
    }
    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        float scaleWidth = 1;
        float scaleHeight = 1;
        int width = bm.getWidth();

        int height = bm.getHeight();

        if(width>newWidth)
            scaleWidth = ((float) newWidth) / width;
        if(height>newHeight)
            scaleHeight = ((float) newHeight) / height;

// CREATE A MATRIX FOR THE MANIPULATION

        Matrix matrix = new Matrix();

// RESIZE THE BITMAP

        matrix.postScale(scaleWidth, scaleHeight);

// RECREATE THE NEW BITMAP

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;

    }
}
