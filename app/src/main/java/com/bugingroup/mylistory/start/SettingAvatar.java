package com.bugingroup.mylistory.start;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bugingroup.mylistory.ImagePicker.commons.bus.RxBusNext;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.Utils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 16.01.2017.
 */
public class SettingAvatar extends AppCompatActivity {


    private RxBusNext mRxBusNext = RxBusNext.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_setting_avatar);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {

        Fragment fragment = new FragmentSettingAvatar();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mMainContent, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @OnClick(R.id.done)
    void DoneClick(){
        mRxBusNext.send(true);
    }


}