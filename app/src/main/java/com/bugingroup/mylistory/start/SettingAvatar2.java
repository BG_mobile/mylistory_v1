package com.bugingroup.mylistory.start;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.bugingroup.mylistory.AWS.ExampleMethod;
import com.bugingroup.mylistory.AWS.auth.AWS4SignerBase;
import com.bugingroup.mylistory.AWS.util.BinaryUtils;
import com.bugingroup.mylistory.ImagePicker.commons.bus.RxBusNext;
import com.bugingroup.mylistory.ImagePicker.commons.models.Session;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Nurik on 16.01.2017.
 */
public class SettingAvatar2 extends AppCompatActivity {


    private Session mSession = Session.getInstance();
    private RxBusNext mRxBusNext = RxBusNext.getInstance();
    ProgressDialog progress;
   static CircleImageView avatar_3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_setting_avatar);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        avatar_3 = (CircleImageView) findViewById(R.id.avatar3);
        Fragment fragment = new FragmentSettingAvatar2();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mMainContent, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @OnClick(R.id.back)
    void BackClick(){
        finish();
    }


    @OnClick(R.id.done)
    void DoneClick(){
        mRxBusNext.send(true);

        progress = new ProgressDialog(this);
        progress.setTitle("");
        progress.setMessage("Загрузить...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        InputStream iStream  = null;
        try {
            iStream = getContentResolver().openInputStream(Uri.fromFile(mSession.getFileToUpload()));
            byte[] inputData = getBytes(iStream);
            PostTOAWS(inputData);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void PostTOAWS(final byte[] inputData) {
        byte[] contentHash = AWS4SignerBase.hash(inputData);
        final String contentHashString = BinaryUtils.toHex(contentHash);


        String body = "{\"contentHashString\":"+contentHashString+"," +
                "\"contentLength\":"+inputData.length+"}";
        Log.d("ASASAS","FDFD"+body);


        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);

        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(SettingAvatar2.this, URL_CONSTANT.AwsToken,entity,"application/json",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        System.out.println("JSONOBJECT1:"+response);
                        Log.d("JSONOBJECT1:",response+"");
                        String authorization = response.getString("authorization");
                        final String resourceId = response.getString("resourceId");
                        final String x_amz_date = response.getString("x-amz-date");
                        final String Host = response.getString("Host");
                        ExampleMethod.test(resourceId,authorization,inputData,x_amz_date,Host);
                        UpdateAvatar(resourceId);
                    }else {
                        progress.dismiss();
                        Toast.makeText(SettingAvatar2.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});




    }

    private void UpdateAvatar(final String resourceId) {
        String body = "{\"resourceId\":\""+resourceId+"\"}";

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);

        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(SettingAvatar2.this, URL_CONSTANT.DOMAIN + "UpdateAvatar", entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    System.out.println("JSONOBJECT2:"+response);
                    Log.d("JSONOBJECT2:", response + "");
                    if (response.getBoolean("success")) {
                        startActivity(new Intent(SettingAvatar2.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("profile",true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK) );
                    } else {
                        Toast.makeText(SettingAvatar2.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        progress.dismiss();

    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }




}