package com.bugingroup.mylistory.start;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.bugingroup.mylistory.PushReceived;
import com.bugingroup.mylistory.SingleChat;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Nurik on 19.10.2016.
 */

public class Start1 extends Activity {
    SharedPreferences prefs;
    public static SharedPreferences prefsStart;


//    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
//    private static final String TAG = "Start1";
//
//    private BroadcastReceiver mRegistrationBroadcastReceiver;
//    private boolean isReceiverRegistered=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        PermissionListener permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            }


        };


        new TedPermission(this)
                .setPermissionListener(permissionlistener)
                .setRationaleMessage("Mylistory нужен разрешение")
                .setRationaleConfirmText("Настройкй")
                .setDeniedMessage("Если вы отклонить доступ, то не можете пользоваться Mylistory\n\nПожалуйста, дайте доступ к приложению в [Настройки] > [Разрешения]")
                .setGotoSettingButtonText("Настройкй")
                .setDeniedCloseButtonText("Отмена")
                .setPermissions(Manifest.permission.READ_CONTACTS,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.CAMERA)
                .check();


        prefs = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
        String firstValue = prefs.getString("lang", "ru");
        Locale locale = new Locale(firstValue);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        int user_num = prefs.getInt("user_number", 1);
        Log.d("TOKENSSSSSS", user_num + "****");
        System.out.println("TOKENSSSSSS" + user_num);

        switch (user_num) {
            case 1:
                prefsStart = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
                Log.d("MAPshare1", "1111");
                System.out.println("1111" + user_num);
                break;
            case 2:
                prefsStart = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                Log.d("MAPshare2", "2222");
                System.out.println("2222" + user_num);
                break;
        }


        Intent intent = getIntent();



        if(intent.hasExtra("operation")){

            Bundle b = getIntent().getExtras();
            String operation =b.getString("operation");

             Log.d("aaaaaaa",b+"*"+b.toString());
            String id = b.getString("id");
            String user_name =b.getString("user_name");

            Log.d("someData",operation+"*"+user_name+"*"+id);

            StartIntent(operation,id,user_name);

            Log.d("someData",b.toString()+"*");

        }else {
             Log.d("someData", "nullll*");

             if (!prefsStart.getString("token", "*").equals("*")) {
                 Login();
             }
             setContentView(R.layout.start1);
             getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
         }
    }


   public void StartIntent(String operation, String id, String user_name){
       Log.d("someData","start");
       if(operation.equals("message")) {
           finish();
             startActivity(new Intent(Start1.this, SingleChat.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("userId", id).putExtra("operation", operation).putExtra("notifi", "true"));


       }else {
           Intent intent = new Intent(this, PushReceived.class);
           intent.putExtra("id", id);
           intent.putExtra("operation", operation);
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           startActivity(intent);
       }
   }

    public void nextPage(View view) {
        startActivity(new Intent(Start1.this, Start2.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }


    public void Login(){

            startActivity(new Intent(Start1.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            Utils.changeToThemeFirst(prefsStart.getInt("theme", 0));
            finish();

    }

}
