package com.bugingroup.mylistory.start;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.models.FeedItem;
import com.bugingroup.mylistory.utils.AnimateFirstDisplayListener;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start11 extends Activity {
    @BindView(R.id.count_frend)
    TextView count_frend;

    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.subscribe_for_all)
    Button subscribe_for_all;


    private UserListAdapter adapter;
    ArrayList<String> alContacts;
    ArrayList<FeedItem> feed_list;
    ProgressDialog progress;
    String token;
    String METHOD;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start11);
        ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        SharedPreferences prefs = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
        token = prefs.getString("token", null);
        METHOD = prefs.getString("domain", null)+"/services/";
        token = prefs.getString("token", null);
        progress = ProgressDialog.show(this, "Загрузить...",
                "", true);

        initializeResources();



    }

    private void initializeResources() {
        ContentResolver cr = getContentResolver(); //Activity/MyApplication android.content.Context
        @SuppressLint("Recycle") Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        assert cursor != null;
        if(cursor.moveToFirst())
        {
            alContacts = new ArrayList<String>();
            do
            {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{ id }, null);
                    while (pCur.moveToNext())
                    {
                        String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        alContacts.add(contactNumber);
                        break;
                    }
                    pCur.close();
                }
            } while (cursor.moveToNext()) ;
        }



        final JSONArray jsArray = new JSONArray(alContacts);
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", token);
        StringEntity entity = new StringEntity(jsArray+"", ContentType.APPLICATION_JSON);

        client.post(Start11.this,URL_CONSTANT.frendsByPhoneList,entity,"application/json",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                feed_list = new ArrayList<FeedItem>();
                try {
                    if(response.getBoolean("success")){
                        JSONArray list = response.getJSONArray("list");
                        initializeText(list.length());
                        for (int i=0;i<list.length();i++){
                            JSONObject user = (JSONObject) list.get(i);
                            FeedItem item = new FeedItem();
                            item.setUser_id(user.getString("id"));
                            item.setSurname(user.getString("surname"));
                            item.setLogin(user.getString("login"));
                            item.setName(user.getString("name"));
                            JSONObject avatar = user.getJSONObject("avatar");
                            if(avatar.has("headers")){
                                item.setHasHeader(true);
                                JSONObject headers = avatar.getJSONObject("headers");
                                item.setX_amz_content_sha256(headers.getString("x-amz-content-sha256"));
                                item.setAuthorization(headers.getString("Authorization"));
                                item.setX_amz_date(headers.getString("x-amz-date"));
                                item.setHost(headers.getString("Host"));
                            }else {
                                item.setHasHeader(false);
                            }
                            item.setResourceId(avatar.getString("resourceId"));
                            item.setAvatar_url(avatar.getString("url"));

                            feed_list.add(item);
                        }
                        adapter = new UserListAdapter (Start11.this,feed_list);
                        listView.setAdapter(adapter);
                        progress.dismiss();
                    }else {
                        progress.dismiss();
                        Toast.makeText(Start11.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progress.dismiss();
                }
            }});
        }




    @OnClick (R.id.subscribe_for_all)
    public void subscribe_for_all(){
        progress = ProgressDialog.show(Start11.this, "Загрузить...",
                "", true);
        for (int i=0;i<feed_list.size();i++){
            final int finalI = i;

            AsyncHttpClient client = new AsyncHttpClient();
            client.addHeader("auth",token);
            RequestParams params = new RequestParams();
            params.put("favoriteId", feed_list.get(i).getUser_id());


            client.post(METHOD+"addFollower",params,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                        startActivity(new Intent(Start11.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                        finish();
                    progress.dismiss();
                }
            });

        }

    }


    private void initializeText(int count) {
        if(count==0){
            subscribe_for_all.setVisibility(View.GONE);
        }
        Resources res = getResources();
        String text = res.getString(R.string.your_frend_in_Mylistory, count);
        count_frend.setText(text);
    }


    public void back_click(View view){
       startActivity(new Intent(Start11.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
    }


    public class UserListAdapter extends BaseAdapter {
        private Activity activity;
        ArrayList<FeedItem> alContacts;
        FeedItem item;
        UserListAdapter(Activity activity, ArrayList<FeedItem> alContacts) {
            this.activity = activity;
            this.alContacts = alContacts;
        }


        @Override
        public int getCount() {
            return alContacts.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            @BindView(R.id.user_name) TextView user_name;
            @BindView(R.id.subscripe_btn) Button subscripe_btn;
            @BindView(R.id.avatar) CircleImageView avatar;


            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.user_list_item, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            item = alContacts.get(position);

//            viewHolder.user_name.setText(item.getName()+" "+item.getSurname());
            viewHolder.user_name.setText(item.getLogin());

            Map<String, String> headers = new HashMap<String, String>();
            if(item.getHasHeader()) {
                headers.put("x-amz-content-sha256", item.getX_amz_content_sha256());
                headers.put("x-amz-date", item.getX_amz_date());
                headers.put("Host", item.getHost());
                headers.put("Authorization", item.getAuthorization());
            }else {
                headers.put("auth", token);
            }
            DisplayImageOptions ava_options = new DisplayImageOptions.Builder()
                    .extraForDownloader(headers)
                    .showImageOnLoading(R.drawable.loading)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            ImageLoader.getInstance().displayImage(item.getAvatar_url(),viewHolder.avatar, ava_options, animateFirstListener);

            viewHolder.subscripe_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item = alContacts.get(position);

                    AsyncHttpClient client = new AsyncHttpClient();
                    client.addHeader("auth",token);
                    RequestParams params = new RequestParams();
                    params.put("favoriteId", item.getUser_id());


                    client.post(METHOD+"addFollower",params,new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                            try {
                                if(response.getBoolean("success")){
                                    startActivity(new Intent(Start11.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                    finish();
                                }else {
                                    Toast.makeText(Start11.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }});


                }
            });
            return convertView;
        }


    }
}
