package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start12 extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start12);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        initializeResources();



    }

    private void initializeResources() {

    }
    public void toUserOrEmail(View view){
        startActivity(new Intent(Start12.this,Start13.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void toCMC(View view){
        startActivity(new Intent(Start12.this,Start15.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void back_click(View view){
        finish();
    }
}
