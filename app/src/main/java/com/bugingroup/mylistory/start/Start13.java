package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start13 extends Activity {

    @BindView(R.id.login)
    EditText login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start13);
        ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        initializeResources();



    }

    private void initializeResources() {

    }


    public void back_click(View view){
        finish();
    }
    public void next_btn(View view){
        if (isEmailValid(login.getText().toString())) {
            String body = "{\n" +
                    "\"type\":\"EMAIL\"," +
                    "\"login\":\""+login.getText().toString()+"\"" +
                    "}";

            RestApi.post(Start13.this,"ResetRequest",body,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        if(response.getBoolean("success")){
                            startActivity(new Intent(Start13.this,Start14.class).putExtra("type","EMAIL").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }else {
                            Toast.makeText(Start13.this,  response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }});

        }

    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @OnClick(R.id.clean)
    void Clean(){
        login.setText(null);
    }

}
