package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bugingroup.mylistory.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start14 extends Activity {

    @BindView(R.id.info)
    TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start14);
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        initializeResources();
        String type = getIntent().getStringExtra("type");
        if(type.equals("EMAIL")){
            info.setText(getResources().getString(R.string.text14));
        }else if(type.equals("PHONE")){
            info.setText(getResources().getString(R.string.text142));

        }


    }

    private void initializeResources() {

    }


    public void back_click(View view){
        startActivity(new Intent(Start14.this,Start2.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Start14.this,Start2.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
    }
}
