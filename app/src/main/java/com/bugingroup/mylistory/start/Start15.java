package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start15 extends Activity {

    @BindView(R.id.login)
    EditText login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start15);
        ButterKnife.bind(this);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        initializeResources();



    }

    private void initializeResources() {

    }


    public void back_click(View view){
        finish();
    }
    public void next_btn(View view){
            String body = "{\n" +
                    "\"type\":\"PHONE\"," +
                    "\"login\":\""+login.getText().toString()+"\"" +
                    "}";

            RestApi.post(Start15.this,"SmsResetRequest",body,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        Log.d("AAAAAA",response+"*");
                        if(response.getBoolean("success")){
                            startActivity(new Intent(Start15.this,Start14.class).putExtra("type","PHONE").addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }else {
                            Toast.makeText(Start15.this,  response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }});
    }

    @OnClick(R.id.clean)
    void Clean(){
        login.setText(null);
    }


}
