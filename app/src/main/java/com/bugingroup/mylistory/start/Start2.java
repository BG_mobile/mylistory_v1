package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.MainRestApi;
import com.bugingroup.mylistory.utils.RestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import static com.bugingroup.mylistory.MainActivity.prefsMain;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start2 extends Activity {


    @BindView(R.id.login)
    EditText login;

    @BindView(R.id.password)
    EditText password;

    LoginButton login_button;
    CallbackManager callbackManager;
    GraphRequest request=null;

    SharedPreferences prefs;

    Boolean is_one=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start2);
        callbackManager = CallbackManager.Factory.create();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        ButterKnife.bind(this);

        login_button = (LoginButton) findViewById(R.id.login_button);
        // If using in a fragment
        login_button.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends"));

        login_button.registerCallback(callbackManager, callback);

        prefs = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);


    }

    @OnClick(R.id.need_help)
     void needHelp(View view){
        startActivity(new Intent(Start2.this,Start3.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    @OnClick(R.id.forgot_password)
     void ForgotPassword(View view){
        startActivity(new Intent(Start2.this,Start12.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }
    public void startHome(View view){
        String type = null;
        if(login.getText().toString().equals("") || password.getText().toString().equals("")){
            Toast.makeText(this, "Введите данные", Toast.LENGTH_SHORT).show();
        }else {
            if (isEmailValid(login.getText().toString())) {
                type = "EMAIL";
            } else if (login.getText().toString().matches("\\d+(?:\\.\\d+)?")) {
                type = "PHONE";
            } else {
                Toast.makeText(this, "Введите данные в указанном формате", Toast.LENGTH_SHORT).show();
            }

            String body = "{\"type\":"+type+",\"login\":"+login.getText().toString()+",\"password\":"+password.getText().toString()+"}";

            final String finalType = type;
            RestApi.post(Start2.this,"login/auth",body,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        if(response.getBoolean("success")){


                             SharedPreferences.Editor editor2 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE).edit();
                             SharedPreferences.Editor editor1 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
                            if(prefs.contains("token")) {
                                is_one=false;
                                Log.d("ASASA","222222");
                                editor2.putString("token", response.getString("token"));
                                editor2.putLong("expires", response.getLong("expires"));
                                editor2.putString("domain", response.getString("domain"));
                                editor2.putString("type", finalType);
                                editor2.putString("login", login.getText().toString());
                                editor2.putString("password", password.getText().toString());
                                editor1.putInt("user_number",2);
                                Start1.prefsStart = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                            }else {
                                is_one=true;
                                Log.d("ASASA","111111");
                                editor1.putInt("user_number",1);
                                editor1.putString("token", response.getString("token"));
                                editor1.putLong("expires", response.getLong("expires"));
                                editor1.putString("domain", response.getString("domain"));
                                editor1.putString("type", finalType);
                                editor1.putString("login", login.getText().toString());
                                editor1.putString("password", password.getText().toString());
                                editor1.putInt("user_number",1);
                            }
                            editor1.commit();
                            editor2.commit();
                            addDevice(response.getString("domain"),response.getString("token"));
                            AsyncHttpClient  client = new AsyncHttpClient();
                            client.addHeader("auth",response.getString("token"));
                            client.addHeader("Content-Type","application/json");

                            client.get(response.getString("domain")+"/services/profile",null,new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] head, JSONObject responsee) {
                                    Log.d("DDDD!",responsee+"//"+statusCode);
                                    try {
                                        if(responsee.getBoolean("success")){

                                            SharedPreferences.Editor editor2 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE).edit();
                                            SharedPreferences.Editor editor1 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
                                            JSONObject body = responsee.getJSONObject("body");
                                            String surname = body.has("surname")?body.getString("surname"):"";
                                            String Name = body.has("name")?body.getString("name"):"";
                                            if(!is_one) {
                                                editor2.putString("name", surname + " " + Name);
                                                editor2.putString("userId", body.getString("id"));

                                            }else {
                                                editor1.putString("name", surname + " " + Name);
                                                editor1.putString("userId", body.getString("id"));

                                            }
                                            editor1.commit();
                                            editor2.commit();
                                            startActivity(new Intent(Start2.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                            finish();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }});



                        }else {
                            Toast.makeText(Start2.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }});


    }

    }

    private void addDevice(String domain,String token) {
        String reg_id = FirebaseInstanceId.getInstance().getToken();
        Log.d("REGISTRATION_ID:","aa"+reg_id);
        Log.d("REGISTRATION_ID:",token);
        Log.d("DOMAIN_IS",domain+"/services/addDevice");
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("registrationId", reg_id);
        client.addHeader("auth",token);
        client.post(domain+"/services/addDevice",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                Log.d("DOMAIN_ISRRR",response+"*");
                try {
                    if(response.getBoolean("success")){

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("DOMAIN_ISEEEE",responseString+"*");
            }
        });
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public void VK_AUTH (View view){
   startActivityForResult(new Intent(Start2.this,VK_Auth.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION),704);
    }


    FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            String token = loginResult.getAccessToken().getToken();
            Log.d("ASASASA_FACEBOOK",token);
            RequestParams params = new RequestParams();
            params.put("accessToken", token);
            RestApi.get("login/authFB",params,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if(response.getBoolean("success")){
                            SharedPreferences.Editor editor2 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE).edit();
                            SharedPreferences.Editor editor1 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
                            if(prefs.contains("token")) {
                                Log.d("ASASA","222222");
                                editor2.putString("token", response.getString("token"));
                                editor2.putLong("expires", response.getLong("expires"));
                                editor2.putString("domain", response.getString("domain"));
                                editor1.putInt("user_number",2);
                                Start1.prefsStart = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                            }else {
                                Log.d("ASASA","111111");
                                editor1.putInt("user_number",1);
                                editor1.putString("token", response.getString("token"));
                                editor1.putLong("expires", response.getLong("expires"));
                                editor1.putString("domain", response.getString("domain"));

                            }
                            editor1.commit();
                            editor2.commit();
                            addDevice(response.getString("domain"),response.getString("token"));
                            LoginManager.getInstance().logOut();
                            startActivity(new Intent(Start2.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                            finish();
                        }else {
                            Toast.makeText(Start2.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                });
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==704 && resultCode==RESULT_OK){
            startActivity(new Intent(Start2.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
            finish();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(request!=null)
            request.executeAsync().cancel(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(request!=null)
            request.executeAsync().cancel(true);
    }

}
