package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bugingroup.mylistory.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start3 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start3);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        ButterKnife.bind(this);


    }

    @OnClick(R.id.phone)
    public void phoneNumber(View view){
        startActivity(new Intent(Start3.this,Start4.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }
    @OnClick(R.id.email)
    public void EmailAddress(View view){
        startActivity(new Intent(Start3.this,Start6.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void back_click(View view){
        finish();
    }

}
