package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.yixia.camera.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start4 extends Activity {
    @BindView(R.id.phone_number)
    EditText phone_number;

    @BindView(R.id.checkBox)
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start4);
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

    }

    @OnClick(R.id.check)
    void ckeckClick(){
        if(checkBox.isChecked()) checkBox.setChecked(false);
        else checkBox.setChecked(true);
    }

    public void next_btn(View view){
        String PhoneNo = phone_number.getText().toString();
        String Regex = "[^\\d]";
       String PhoneDigits = PhoneNo.replaceAll(Regex, "");
        if (checkBox.isChecked()) {
            if(PhoneDigits.length()==10) {
//                startActivity(new Intent(Start4.this, Start5.class).putExtra("phone","7"+phone_number.getText().toString()));

                    RestApi.get("phoneRegistration/7"+phone_number.getText().toString(),null,new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] head, JSONObject response) {

                            Log.d("RRr",response+"**");
                            try {
                                if(response.getBoolean("success") ){
                                    if(!response.getBoolean("body")) {
                                        startActivity(new Intent(Start4.this, Start5.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("phone", "7" + phone_number.getText().toString()));
                                    }else {
                                        Toast.makeText(Start4.this, "Этот номер уже зарегистрирован", Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    Toast.makeText(Start4.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }});


            }else {
                Toast.makeText(this, getString(R.string.enter_a_valid_phone_number), Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, getString(R.string.agree), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.email)
    public void using_Email_signUp(View view){
        startActivity(new Intent(Start4.this, Start6.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void back_click(View view){
        finish();
    }
}
