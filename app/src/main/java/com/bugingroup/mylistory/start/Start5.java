package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start5 extends Activity {

    @BindView(R.id.confirmation_code)
    EditText confirmation_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start5);
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);




    }


    public void next_btn(View view){
        final ProgressDialog progress = ProgressDialog.show(Start5.this, "Загрузить...",
            "", true);

        if(confirmation_code.getText().length()>0){
           String body = "{\"phone\":\""+getIntent().getStringExtra("phone")+"\",\"securityKey\":\""+confirmation_code.getText().toString()+"\"}";
            RestApi.post(Start5.this,"PhoneRegiCodeCheck",body,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        if(response.getBoolean("success")){
                            if(response.getBoolean("body")){
                                startActivity(new Intent(Start5.this,Start7.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("phone",getIntent().getStringExtra("phone")));
                            }else {
                                Toast.makeText(Start5.this, "не правильный код", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(Start5.this, "Сервер не доступен", Toast.LENGTH_SHORT).show();

                        }
                        progress.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }});

        }else {
            progress.dismiss();
        }

    }

    public void back_click(View view){
        finish();
    }
}
