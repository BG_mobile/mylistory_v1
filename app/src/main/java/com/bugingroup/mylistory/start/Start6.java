package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start6 extends Activity {

    @BindView(R.id.email_address)
    EditText email_address;

    @BindView(R.id.checkBox)
    CheckBox checkBox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start6);
        ButterKnife.bind(this);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

    }

    @OnClick(R.id.ckeck)
    void ckeckClick(){
        if(checkBox.isChecked()) checkBox.setChecked(false);
        else checkBox.setChecked(true);
    }

    public void next_btn(View view){
        if (checkBox.isChecked()) {
            if(isValidEmail(email_address.getText().toString())) {

                RequestParams params = new RequestParams();
                params.put("email",email_address.getText().toString());
                RestApi.post(Start6.this,"eMailRegistration",params,new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                        System.out.println(response+"*");
                        try {
                            if(response.getBoolean("success") ){
                                if(!response.getBoolean("body"))
                                    startActivity(new Intent(Start6.this, Start7.class).putExtra("email",email_address.getText().toString()).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                else {
                                    Toast.makeText(Start6.this, R.string.email_is_already_registered, Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(Start6.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }});


            }else {
                Toast.makeText(this, getString(R.string.enter_a_valid_email_address), Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, getString(R.string.agree), Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick(R.id.phone)
    public void using_Phone_signUp(View view){
        startActivity(new Intent(Start6.this, Start4.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void back_click(View view){
        finish();
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}
