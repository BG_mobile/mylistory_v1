package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.RestApi;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yixia.camera.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.util.TextUtils;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start7 extends Activity {
    @BindView(R.id.nick_name)
    EditText nick_name;

    String email,phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start7);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        ButterKnife.bind(this);
    }


    public void next_btn(View view) {
       if(isValid(nick_name.getText().toString())){
           checkLogin();
       }else {
           Toast.makeText(this, "В именах пользователей можно использовать только буквы, цифры, символы подчеркивания и точко", Toast.LENGTH_SHORT).show();
       }

//        if(!nick_name.getText().toString().equals("")) {
//            if (getIntent().hasExtra("email")) {
//                email = getIntent().getStringExtra("email");
//                startActivity(new Intent(Start7.this, Start8.class).putExtra("email", email).putExtra("name", nick_name.getText().toString()));
//
//            }
//            if (getIntent().hasExtra("phone")) {
//                phone = getIntent().getStringExtra("phone");
//                startActivity(new Intent(Start7.this, Start8.class).putExtra("phone", phone).putExtra("name", nick_name.getText().toString()));
//
//            }
//        }

    }

    private boolean isValid(CharSequence s) {
        Pattern sPattern = Pattern.compile("^[a-zA-Z0-9_]*$");
        return sPattern.matcher(s).matches();
    }

    public void checkLogin(){

        String n = nick_name.getText().toString();
        if(!TextUtils.isEmpty(nick_name.getText().toString())) {

            RestApi.get("loginRegistration/"+n,null,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {

                    Log.d("RRr",response+"**");
                    try {
                        if(response.getBoolean("success") ){
                            if(!response.getBoolean("body")) {
                                if (getIntent().hasExtra("email")) {
                                    email = getIntent().getStringExtra("email");
                                    startActivity(new Intent(Start7.this, Start8.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("email", email).putExtra("name", nick_name.getText().toString()));
                                }
                                if (getIntent().hasExtra("phone")) {
                                    phone = getIntent().getStringExtra("phone");
                                    startActivity(new Intent(Start7.this, Start8.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("phone", phone).putExtra("name", nick_name.getText().toString()));
                                }
                            }else {
                                Toast.makeText(Start7.this, getString(R.string.login_is_already_registered), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(Start7.this, "Сервер не доступен", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }});


        }else {
            Toast.makeText(this, getString(R.string.enter_a_login), Toast.LENGTH_SHORT).show();
        }
    }

    public void back_click(View view){
        finish();
    }
}
