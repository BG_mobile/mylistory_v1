package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bugingroup.mylistory.AWS.ExampleMethod;
import com.bugingroup.mylistory.AWS.auth.AWS4SignerBase;
import com.bugingroup.mylistory.AWS.util.BinaryUtils;
import com.bugingroup.mylistory.ImagePicker.commons.models.Session;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.CircleImageView;
import com.bugingroup.mylistory.utils.RestApi;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start8 extends Activity {

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.avatar)
    CircleImageView avatar;
    Boolean has_photo = false;
    private Session mSession = Session.getInstance();
    ProgressDialog progress;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start8);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        ButterKnife.bind(this);

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivityForResult(new Intent(Start8.this, SettingAvatar.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION),704);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 704 && resultCode == Activity.RESULT_OK) {
            Picasso.with(Start8.this).load(Uri.fromFile(mSession.getFileToUpload()))
                    .noFade()
                    .noPlaceholder()
                    .into(avatar);
            has_photo=true;
        }
    }

    public void next_btn(View view) {
        if (password.getText().toString().length() >= 6) {
            progress = ProgressDialog.show(Start8.this, "Загрузить...",
                    "", true);
            String[] splited = name.getText().toString().split("\\s+");
            String Name = " ";
            String Surname = " ";
                 Name = splited[0];
            if(splited.length>1) {
                Surname = splited[1];
            }

            String body = "{" +
                    "\"password\":\"" + password.getText().toString() + "\"," +
                    "\"surname\":\"" + Surname + "\"," +
                    "\"name\":\"" + Name + "\",";
            if(getIntent().hasExtra("name")) {
                body = body+ "\"login\":\"" + getIntent().getStringExtra("name") + "\"" + ",";
            }
            if(getIntent().hasExtra("phone")) {
                body = body+ "\"phone\":\"" + getIntent().getStringExtra("phone") + "\"" + "}";
            }
            if(getIntent().hasExtra("email")) {
                body = body+ "\"email\":\"" + getIntent().getStringExtra("email") + "\"" + "}";
            }

            final String finalBody = body;
            RestApi.post(Start8.this,"registrationData",body,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    Log.d("JSONOBJECT0:", finalBody +"***"+response+"");
                    try {
                        if (response.getBoolean("success")) {
                            String bodyLogin = null;
                            String type = null;
                            String login = null;
                            if(getIntent().hasExtra("phone")) {
                                type = "PHONE";
                                login = getIntent().getStringExtra("phone");
                                bodyLogin = "{\"type\":\"PHONE\",\"login\":\"" + getIntent().getStringExtra("phone") + "\",\"password\":\"" + password.getText().toString() + "\"}\n";
                            }
                            if(getIntent().hasExtra("email")) {
                                type = "EMAIL";
                                login = getIntent().getStringExtra("email");
                                bodyLogin = "{\"type\":\"EMAIL\",\"login\":\"" + getIntent().getStringExtra("email") + "\",\"password\":\"" + password.getText().toString() + "\"}\n";
                            }

                            Log.d("bodyLogin",bodyLogin);
                            final String finalType = type;
                            final String finalLogin = login;
                            RestApi.post(Start8.this,"login/auth",bodyLogin,new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                                    try {
                                        Log.d("AAAAA",response+"*");
                                        if (response.getBoolean("success")) {
                                            SharedPreferences.Editor editor = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
                                            editor.putString("token", response.getString("token"));
                                            token=response.getString("token");
                                            editor.putLong("expires", response.getLong("expires"));
                                            editor.putString("domain", response.getString("domain"));
                                            editor.putString("type", finalType);
                                            editor.putString("login", finalLogin);
                                            editor.putString("password", password.getText().toString());
                                            editor.commit();
                                            if(has_photo){
                                                InputStream  iStream = null;
                                                try {
                                                    iStream = getContentResolver().openInputStream(Uri.fromFile(mSession.getFileToUpload()));
                                                    byte[] inputData = getBytes(iStream);
                                                    PostTOAWS(inputData);
                                                } catch (FileNotFoundException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }

                                            }else {
                                                startActivity(new Intent(Start8.this, Start9.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                                                finish();
                                            }
                                        } else {
                                            Toast.makeText(Start8.this, response.has("errorDesc")?response.getString("errorDesc"):response.getString("errorCode"), Toast.LENGTH_SHORT).show();
                                            progress.dismiss();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }});
                        } else {
                            progress.dismiss();
                            Toast.makeText(Start8.this, response.has("errorDesc")?response.getString("errorDesc"):response.getString("errorCode"), Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }});



        } else {
            Toast.makeText(this, getString(R.string.passwords_must_contain_at_least_6_characters), Toast.LENGTH_SHORT).show();
        }
    }

    private void PostTOAWS(final byte[] inputData) {
        byte[] contentHash = AWS4SignerBase.hash(inputData);
        final String contentHashString = BinaryUtils.toHex(contentHash);


        String body = "{\"contentHashString\":"+contentHashString+"," +
                "\"contentLength\":"+inputData.length+"}";
        Log.d("ASASAS","FDFD"+body);

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", token);

            StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
            client.post(Start8.this, URL_CONSTANT.AwsToken, entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        if(response.getBoolean("success")){
                            Log.d("JSONOBJECT1:",response+"");
                            String authorization = response.getString("authorization");
                            final String resourceId = response.getString("resourceId");
                            final String x_amz_date = response.getString("x-amz-date");
                            final String Host = response.getString("Host");
                            ExampleMethod.test(resourceId,authorization,inputData,x_amz_date,Host);
                            UpdateAvatar(resourceId);
                        }else {
                            progress.dismiss();
                            Toast.makeText(Start8.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });



    }

    private void UpdateAvatar(String resourceId) {
        String body = "{\"resourceId\":\""+resourceId+"\"}";
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", token);
            StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
            client.post(Start8.this, URL_CONSTANT.DOMAIN +"UpdateAvatar", entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                    try {
                        Log.d("JSONOBJECT2:", response + "");
                        if (response.getBoolean("success")) {
                            startActivity(new Intent(Start8.this, Start9.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                            finish();
                        } else {
                            Toast.makeText(Start8.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        progress.dismiss();
    }

    public void back_click(View view) {
        finish();
    }
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


}
