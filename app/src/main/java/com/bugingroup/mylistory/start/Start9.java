package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;

/**
 * Created by Nurik on 19.10.2016.
 */

public class Start9 extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start9);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        initializeResources();



    }

    private void initializeResources() {

    }
    public void next_btn(View view){
        startActivity(new Intent(Start9.this,Start11.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
    }

    public void back_click(View view){
        startActivity(new Intent(Start9.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        finish();
    }
}
