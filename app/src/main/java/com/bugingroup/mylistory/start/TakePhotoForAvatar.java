package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.AWS.ExampleMethod;
import com.bugingroup.mylistory.AWS.auth.AWS4SignerBase;
import com.bugingroup.mylistory.AWS.util.BinaryUtils;
import com.bugingroup.mylistory.ImagePicker.FilterActivity;
import com.bugingroup.mylistory.ImagePicker.ImagePicker;
import com.bugingroup.mylistory.ImagePicker.commons.ui.CropImageView;
import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.bugingroup.mylistory.utils.Utils;
import com.bugingroup.mylistory.videoRecorder.recoder.RecoderActivity;
import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraHostProvider;
import com.commonsware.cwac.camera.CameraView;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;


/**
 * Created by Nurik on 10.03.2017.
 */

public class TakePhotoForAvatar extends Activity implements CameraHostProvider {
    @BindView(R.id.cameraView)
    CameraView cameraView;

    @BindView(R.id.mBtnTakePhoto)
    ImageView btnTakePhoto;

    @BindView(R.id.mPreview)
    CropImageView mPreview;

    @BindView(R.id.title)
    TextView title;

    @BindString(R.string.tab_photo)
    String _tabPhoto;

    @BindView(R.id.mFlashPhoto)
    ImageButton mFlashPhoto;

    @BindView(R.id.mSwitchCamera)
    ImageButton mSwitchCamera;
    ProgressDialog progress;


    private static final String[] FLASH_OPTIONS = {
           "auto",
            "off",
            "on"
    };

    private static final int[] FLASH_ICONS = {
            R.drawable.ic_flash_auto,
            R.drawable.ic_flash_off,
            R.drawable.ic_flash_on,
    };

    private int mCurrentFlash;

    Boolean is_front;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        is_front = getIntent().getBooleanExtra("is_front",false);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.take_photo_for_avatar);
        ButterKnife.bind(this);
       // imageview.setVisibility(View.GONE);

        cameraView.setVisibility(View.VISIBLE);
        title.setText(_tabPhoto);
        cameraView.autoFocus();

        cameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraView.autoFocus();
            }
        });


    }

    @OnClick(R.id.next_btn)
    void NextClick(){

        Bitmap b = mPreview.getCroppedImage();
        Bitmap bitmap = Bitmap.createScaledBitmap(b, 600, 600, false);

        progress = new ProgressDialog(this);
        progress.setTitle("");
        progress.setMessage("Загрузить...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();

        PostTOAWS(byteArray);
    }


     @OnClick(R.id.back)
    void BackClick(){
       finish();
    }




    @OnClick(R.id.mBtnTakePhoto)
    public void onTakePhotoClick() {
        btnTakePhoto.setEnabled(false);
        cameraView.takePicture(true, false);

    }

    @OnClick(R.id.mSwitchCamera)
    void onSwitchCamera() {
        finish();
        Intent intent = new Intent(TakePhotoForAvatar.this,TakePhotoForAvatar.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("is_front",!is_front);
        startActivity(intent);

    }


    @OnClick(R.id.mFlashPhoto)
    void onChangeFlashState() {
        if (cameraView != null) {
            mCurrentFlash = (mCurrentFlash + 1) % FLASH_OPTIONS.length;
            mFlashPhoto.setImageResource(FLASH_ICONS[mCurrentFlash]);
            cameraView.setFlashMode(FLASH_OPTIONS[mCurrentFlash]);
        }
    }
    @Override
    public CameraHost getCameraHost() {

        return new MyCameraHost(this,is_front);
    }


    private class MyCameraHost extends SimpleCameraHost {

        private Camera.Size previewSize;
        Boolean is_front;

        MyCameraHost(Context ctxt, Boolean is_front) {
            super(ctxt);
            this.is_front = is_front;
        }

        @Override
        public boolean useFullBleedPreview() {
            return true;
        }

        @Override
        public boolean useFrontFacingCamera() {
            return is_front;
        }

        @Override
        public Camera.Size getPictureSize(PictureTransaction xact, Camera.Parameters parameters) {
            return previewSize;
        }

        @Override
        public Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters) {
            Camera.Parameters parameters1 = super.adjustPreviewParameters(parameters);
            previewSize = parameters1.getPreviewSize();
            return parameters1;
        }

        @Override
        public void saveImage(PictureTransaction xact, final Bitmap bitmap) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showTakenPicture(bitmap);
                }
            });
        }


    }

    private void showTakenPicture(Bitmap bitmap) {

        cameraView.setVisibility(View.GONE);
        System.out.println("------------------------------------------");

        mPreview.setImageBitmap(getResizedBitmap(bitmap,700));
        mPreview.setZoom(1);
        System.out.println("--------------------yyyyy----------------------");

    }


    @Override
    public void onResume() {
        super.onResume();
        cameraView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.onPause();
    }

    public Bitmap getResizedBitmap(Bitmap bm, int maxSize) {
        int outWidth;
        int outHeight;
        int inWidth = bm.getWidth();
        int inHeight = bm.getHeight();
        if(inWidth > inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }

       return Bitmap.createScaledBitmap(bm, outWidth, outHeight, false);

    }


    private void PostTOAWS(final byte[] inputData) {
        byte[] contentHash = AWS4SignerBase.hash(inputData);
        final String contentHashString = BinaryUtils.toHex(contentHash);


        String body = "{\"contentHashString\":"+contentHashString+"," +
                "\"contentLength\":"+inputData.length+"}";
        Log.d("ASASAS","FDFD"+body);


        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);

        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(TakePhotoForAvatar.this, URL_CONSTANT.AwsToken,entity,"application/json",new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    if(response.getBoolean("success")){
                        System.out.println("JSONOBJECT1:"+response);
                        Log.d("JSONOBJECT1:",response+"");
                        String authorization = response.getString("authorization");
                        final String resourceId = response.getString("resourceId");
                        final String x_amz_date = response.getString("x-amz-date");
                        final String Host = response.getString("Host");
                        ExampleMethod.test(resourceId,authorization,inputData,x_amz_date,Host);
                        UpdateAvatar(resourceId);
                    }else {
                        progress.dismiss();
                        Toast.makeText(TakePhotoForAvatar.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }});




    }

    private void UpdateAvatar(final String resourceId) {
        String body = "{\"resourceId\":\""+resourceId+"\"}";

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("auth", MainActivity.token);

        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(TakePhotoForAvatar.this, URL_CONSTANT.DOMAIN + "UpdateAvatar", entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                try {
                    System.out.println("JSONOBJECT2:"+response);
                    Log.d("JSONOBJECT2:", response + "");
                    if (response.getBoolean("success")) {
                        startActivity(new Intent(TakePhotoForAvatar.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("profile",true).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK) );
                    } else {
                        Toast.makeText(TakePhotoForAvatar.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        progress.dismiss();

    }
}
