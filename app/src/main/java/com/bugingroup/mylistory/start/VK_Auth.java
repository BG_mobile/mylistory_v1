package com.bugingroup.mylistory.start;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.Toast;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.URL_CONSTANT;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Nurik on 27.01.2017.
 */
public class VK_Auth extends Activity{
    private WebView web;
    private ListView listView;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vk_auth);
        prefs = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);


        listView = (ListView) findViewById(R.id.listView);
        web = (WebView) findViewById(R.id.webView);
        web.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        web.loadUrl("https://oauth.vk.com/authorize?client_id=5731417&client_secret=HPjaWdZ5rnBFlpQRuEdw&redirect_uri=http://mylistory.com:8080/reg-server/services/login/authVK&response_type=code&display=mobile");
        web.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if(url.contains("http://mylistory.com:8080")){
                    AsyncHttpClient client = new AsyncHttpClient();
                    System.out.println("ASASAS1"+"BABABA");
                    client.get(url,   new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            System.out.println("ASASAS33333"+ response+"/");
                            try {
                                if(response.getBoolean("success")){


                                    SharedPreferences.Editor editor2 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE).edit();
                                    SharedPreferences.Editor editor1 = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE).edit();
                                    if(prefs.contains("token")) {
                                        Log.d("ASASA","222222");
                                        editor2.putString("token", response.getString("token"));
                                        editor2.putLong("expires", response.getLong("expires"));
                                        editor2.putString("domain", response.getString("domain"));
                                        editor1.putInt("user_number",2);
                                        MainActivity.prefsMain = getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                                    }else {
                                        Log.d("ASASA","111111");
                                        editor1.putInt("user_number",1);
                                        editor1.putString("token", response.getString("token"));
                                        editor1.putLong("expires", response.getLong("expires"));
                                        editor1.putString("domain", response.getString("domain"));

                                    }
                                    editor1.commit();
                                    editor2.commit();
                                    addDevice(response.getString("domain"),response.getString("token"));
                                    Intent intent =  new Intent();
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }else {
                                    Toast.makeText(VK_Auth.this, response.getString("errorDesc"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }else {

                    view.loadUrl(url);
                }
                Log.d("ASASAS3", url);
                return true;
            }

        });
    }

    private void addDevice(String domain,String token) {
        String reg_id = FirebaseInstanceId.getInstance().getToken();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("registrationId", reg_id);
        client.addHeader("auth",token);
        client.post(domain+"/services/addDevice",params,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] head, JSONObject response) {
                Log.d("DOMAIN_ISRRR",response+"*");
                try {
                    if(response.getBoolean("success")){

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("DOMAIN_ISEEEE",responseString+"*");
            }
        });
    }
}
