package com.bugingroup.mylistory.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

/**
 * Created by Nurik on 24.01.2017.
 */

public class ExampleAsynTask{
    private LruCache<String, Bitmap> mMemoryCache;

    private static ExampleAsynTask instance = null;


    public static ExampleAsynTask getInstance() {
        if (instance == null) {
            instance = new ExampleAsynTask();
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
        }
        return instance;
    }


    //Lock the constructor from public instances
    private ExampleAsynTask() {

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 4;


        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    private Map<String,String> Maps;
    private String Url;
    private String ResourceId;
    public void loadBitmap( String resourceId,  String url, ImageView imageView,  Map<String,String> map ) {

        Bitmap bitmap = getBitmapFromMemCache(resourceId);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            this.Maps = map;
            this.Url = url;
            this.ResourceId = resourceId;

            BitmapWorkerTask task = new BitmapWorkerTask(imageView);
            task.execute();
        }
    }

    private class BitmapWorkerTask extends AsyncTask<Void, Void, Void> {
        Bitmap bitmap;
        private final WeakReference<ImageView> imageViewReference;
        private int data = 0;

        BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background.
        @Override
        protected Void doInBackground(Void... params) {
            URL endpointUrl;
            try {
                endpointUrl = new URL(Url);

                byte[] bitmapdata ;

                URLConnection ucon = endpointUrl.openConnection();
                for ( String headerKey : Maps.keySet() ) {
                    System.out.println(headerKey + ": " + Maps.get(headerKey));
                    ucon.setRequestProperty(headerKey, Maps.get(headerKey));
                }
                InputStream is = ucon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);

                ByteArrayBuffer baf = new ByteArrayBuffer(500);
                int current = 0;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                bitmapdata =  baf.toByteArray();
                bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata .length);

            } catch (Exception e) {
                Log.d("ImageManager", "Error: " + e.toString());
                e.printStackTrace();
                System.err.println(e);
            }
            return null;
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Void params) {
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                    addBitmapToMemoryCache(ResourceId, bitmap);
                }
            }
        }
    }




    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }


}
