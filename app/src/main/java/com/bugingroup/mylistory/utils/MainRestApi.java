package com.bugingroup.mylistory.utils;

/**
 * Created by Nurik on 21.01.2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.bugingroup.mylistory.MainActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

import static android.content.Context.MODE_PRIVATE;
import static com.bugingroup.mylistory.start.Start1.prefsStart;

public  class MainRestApi {

    static SharedPreferences preferences;
    static String token;
    public static void MainRestApi(Activity activity){
        SharedPreferences prefs = activity.getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
        int user_num = prefs.getInt("user_number",1);

        switch (user_num){
            case 1:
                preferences = activity.getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME1, MODE_PRIVATE);
                token = preferences.getString("token", null);

                Log.d("MAPshareREST","1111");
                System.out.println("1111"+user_num);
                break;
            case 2:
                preferences = activity.getSharedPreferences(URL_CONSTANT.MY_PREFS_NAME2, MODE_PRIVATE);
                token = preferences.getString("token", null);

                Log.d("MAPshareREST","2222");
                System.out.println("2222"+user_num);
                break;
        }
    }


    private static AsyncHttpClient  client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.addHeader("auth",token);
        client.addHeader("Content-Type","application/json");
        client.get(getAbsoluteUrl(url), params, responseHandler);
        Log.d("MainRestApi:",getAbsoluteUrl(url)+"*"+token);
    }

    public static void post(Context context, String url, String body, AsyncHttpResponseHandler responseHandler) {
        client.addHeader("auth",token);
        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(context,getAbsoluteUrl(url), entity,"application/json", responseHandler);
        Log.d("MainRestApi:",getAbsoluteUrl(url)+"*"+MainActivity.token);


    }

    public static void post( String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.addHeader("auth",token);
        client.addHeader("Content-Type","application/json");
        client.post(getAbsoluteUrl(url), params, responseHandler);
        Log.d("MainRestApi:",getAbsoluteUrl(url)+"*"+MainActivity.token);

    }




    private static String getAbsoluteUrl(String relativeUrl) {
      String u =   prefsStart.getString("domain", null)+"/services/";
        return u + relativeUrl;
    }
}
