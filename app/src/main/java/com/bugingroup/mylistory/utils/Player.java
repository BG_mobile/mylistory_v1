package com.bugingroup.mylistory.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.widget.VideoView;

import com.yixia.camera.util.Log;

/**
 * Created by Nurik on 01.02.2017.
 */

public class Player extends VideoView implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    private MediaPlayer mediaPlayer;
    boolean is_mute=true;


    private int mVideoWidth=1080;
    private int mVideoHeight=720;


    public Player(Context context, AttributeSet attributes) {
        super(context, attributes);

        this.setOnPreparedListener(this);
        this.setOnCompletionListener(this);
        this.setOnErrorListener(this);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
        this.mediaPlayer.setLooping(true);
        mute();



    }

    public String getVideoHeight(){
        mVideoWidth = mediaPlayer.getVideoHeight();
        mVideoHeight = mediaPlayer.getVideoWidth();
        setVideoSize(720,720);
        return mediaPlayer.getVideoHeight()+"**"+mediaPlayer.getVideoWidth();
    }

    public void setVideoSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        double r;
        if(width<1080){
            r =(double) 1080/width;
            mVideoWidth = 1080;
            mVideoHeight = (int) (height*r);
        }else {
            mVideoWidth = width;
            mVideoHeight = height;
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Log.i("@@@", "onMeasure");
        int width = getDefaultSize(mVideoWidth, widthMeasureSpec);
        Log.d("aaaa:",mVideoWidth+"/"+widthMeasureSpec);
        int height = getDefaultSize(mVideoHeight, heightMeasureSpec);
        Log.d("wwwww:",width+"/"+height);
        if (mVideoWidth > 0 && mVideoHeight > 0) {
            if (mVideoWidth * height > width * mVideoHeight) {
                // Log.i("@@@", "image too tall, correcting");
                height = width * mVideoHeight / mVideoWidth;
            } else if (mVideoWidth * height < width * mVideoHeight) {
                // Log.i("@@@", "image too wide, correcting");
                width = height * mVideoWidth / mVideoHeight;
            } else {
                // Log.i("@@@", "aspect ratio is correct: " +
                // width+"/"+height+"="+
                // mVideoWidth+"/"+mVideoHeight);
            }
        }
        // Log.i("@@@", "setting size: " + width + 'x' + height);
        setMeasuredDimension(width, height);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {  return false;}

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.d("AAAA222","DDDD");
    }

    public void mute() {
        this.is_mute=true;
        this.setVolume(0);

    }

    public void unmute() {
        this.is_mute=false;
        this.setVolume(100);
    }

    public boolean ismute(){
       return this.is_mute;
    }

    public void setVolume(float amount) {
        final int max = 100;
        final double numerator = max - amount > 0 ? Math.log(max - amount) : 0;
        final float volume = (float) (1 - (numerator / Math.log(max)));

        this.mediaPlayer.setVolume(volume, volume);
    }
}