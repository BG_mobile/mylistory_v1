package com.bugingroup.mylistory.utils;

/**
 * Created by Nurik on 21.01.2017.
 */

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.yixia.camera.util.Log;

import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;

public  class RestApi {

    private static final String BASE_URL = "http://mylistory.com:8080/reg-server/services/";

    private static AsyncHttpClient  client = new AsyncHttpClient();


    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
        Log.d("AAAA",getAbsoluteUrl(url));
    }

    public static void post(Context context, String url, String body, AsyncHttpResponseHandler responseHandler) {
        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        client.post(context,getAbsoluteUrl(url), entity,"application/json", responseHandler);
    }

    public static void post(Context context,String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.addHeader("Content-Type","application/x-www-form-urlencoded");
        client.post(context,getAbsoluteUrl(url), params, responseHandler);
    }




    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
