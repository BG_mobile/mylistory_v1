package com.bugingroup.mylistory.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Environment;
import android.text.format.DateFormat;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bugingroup.mylistory.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Nurik on 04.01.2017.
 */

public class URL_CONSTANT {

    public static class Config {
        public static final boolean DEVELOPER_MODE = false;
    }


    public static final String MY_PREFS_NAME1 = "MylistoryFile1";
    public static final String MY_PREFS_NAME2 = "MylistoryFile2";
    public static final String MY_PREFS_NAME3 = "MylistoryFile3";
    public static final String MY_PREFS_NAME4 = "MylistoryFile4";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String REGID = "regid";


    public static final String DOMAIN = "http://mylistory.com:8080/reg-server/services/";
    public static final String frendsByPhoneList = DOMAIN+"frendsByPhoneList";
    public static final String RefreshToken = DOMAIN+"login/refresh";
    public static final String AwsToken = DOMAIN+"AwsToken";


    public static String getTimeAgo( long givenDateString, Context ctx) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        long now = System.currentTimeMillis();
      cal.setTimeInMillis(now);
        Date nowD = cal.getTime();
        int nowH = nowD.getHours();
        String message = "";
        cal.setTimeInMillis(givenDateString);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        Date data = cal.getTime();
       int  mHour = data.getHours();
       int  mMinut = data.getMinutes();

        long t = (now - givenDateString)/1000;
        if (t < 10) {
            message = ctx.getResources().getString(R.string.just);
        } else if (t < 60) {
            message =  ctx.getResources().getString(R.string.sec_ago, t);
        } else if (t < 60 * 60) {
            message = ctx.getResources().getString(R.string.min_ago,(int)(t/60));
        } else if (t < 2 * 60 * 60) {
            message = ctx.getResources().getString(R.string.hour_ago,(int)(t / 3600));
        } else if (t < 3 * 60 * 60) {
            message =  ctx.getResources().getString(R.string.hours_ago,(int)(t / 3600));
        } else if (t < 4 * 60 * 60) {
            message = ctx.getResources().getString(R.string.hours_ago,(int)(t / 3600));
        } else if (t > 4 * 60 * 60 && t < 24 * 60 * 60 && mHour <nowH) {
            String h = mHour+":"+mMinut;
            message =  ctx.getResources().getString(R.string.today_at,h);
        } else if (t > 4 * 60 * 60 && t < 24 * 60 * 60 && mHour > nowH) {
            String h = mHour+":"+mMinut;
            message = ctx.getResources().getString(R.string.yesterday_in,h);
        } else if (t > 24 * 60 * 60 && t < 48 * 60 * 60) {
            String h = mHour+":"+mMinut;
            message = ctx.getResources().getString(R.string.yesterday_in,h);
        } else {
            message = date;
        }

        return message;

    }

    public static String ChatTimeAgo( long givenDateString, Context ctx) {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        long now = System.currentTimeMillis();
      cal.setTimeInMillis(now);
        Date nowD = cal.getTime();
        int nowH = nowD.getHours();
        String message = "";
        cal.setTimeInMillis(givenDateString);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        Date data = cal.getTime();
       int  mHour = data.getHours();
       int  mMinut = data.getMinutes();

        long t = (now - givenDateString)/1000;
        if (t < 10) {
            message = ctx.getResources().getString(R.string.just);
        } else if (t < 60) {
            message =  ctx.getResources().getString(R.string.sec_ago, t);
        } else if (t < 60 * 60) {
            message = ctx.getResources().getString(R.string.min_ago,(int)(t/60));
        } else if ( mHour <nowH) {
            String h = mHour+":"+mMinut;
            message =  ctx.getResources().getString(R.string.today_at,h);
        } else if (t > 4 * 60 * 60 && t < 24 * 60 * 60 && mHour > nowH) {
            String h = mHour+":"+mMinut;
            message = ctx.getResources().getString(R.string.yesterday_in,h);
        } else if (t > 24 * 60 * 60 && t < 48 * 60 * 60) {
            String h = mHour+":"+mMinut;
            message = ctx.getResources().getString(R.string.yesterday_in,h);
        } else {
            message = date;
        }

        return message;

    }


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }







    public static void SaveImage(Activity activity, Bitmap finalBitmap) {



        int w = finalBitmap.getWidth();
        int h = finalBitmap.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, finalBitmap.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(finalBitmap, 0, 0, null);

        Bitmap waterMark = BitmapFactory.decodeResource(activity.getResources(), R.drawable.watermark);
        canvas.drawBitmap(waterMark, w-110, h-80, null);


        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Pictures/mylistory/");
        myDir.mkdirs();
        long tsLong = System.currentTimeMillis()/1000;

        String fname = tsLong +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            result.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            Toast.makeText(activity, "Downloaded success", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
