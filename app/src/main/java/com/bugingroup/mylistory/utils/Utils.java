package com.bugingroup.mylistory.utils;

import android.app.Activity;
import android.content.Intent;

import com.bugingroup.mylistory.MainActivity;
import com.bugingroup.mylistory.R;

public class Utils
{
	private static int sTheme;

	public final static int THEME_DEFAULT = 0;
	public final static int THEME_WHITE = 1;
	public final static int THEME_BLUE = 2;
	public final static int THEME_GREEN = 3;
	public final static int THEME_PINK = 4;
	public final static int THEME_BLACK = 5;

	/**
	 * Set the theme of the Activity, and restart it by creating a new Activity
	 * of the same type.
	 */
	public static void changeToTheme(Activity activity, int theme, Class<MainActivity> activity2)
	{
		sTheme = theme;
		activity.finish();

		activity.startActivity(new Intent(activity, activity2).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
		activity.overridePendingTransition(0, 0);
	}
	public static void changeToThemeFirst( int theme)
	{
		sTheme = theme;
	}

	/** Set the theme of the activity, according to the configuration. */
	public static void onActivityCreateSetTheme(Activity activity)
	{
		switch (sTheme)
		{
		default:
		case THEME_DEFAULT:
			break;
		case THEME_WHITE:
			activity.setTheme(R.style.Theme_White);
			break;
		case THEME_BLUE:
			activity.setTheme(R.style.Theme_Blue);
			break;
		case THEME_GREEN:
			activity.setTheme(R.style.Theme_Green);
			break;
		case THEME_PINK:
			activity.setTheme(R.style.Theme_Pink);
			break;
		case THEME_BLACK:
			activity.setTheme(R.style.Theme_Black);
			break;
		}
	}
}
