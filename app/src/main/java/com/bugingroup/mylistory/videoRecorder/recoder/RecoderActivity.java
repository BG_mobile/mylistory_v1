package com.bugingroup.mylistory.videoRecorder.recoder;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.mylistory.ImagePicker.ImagePicker;
import com.bugingroup.mylistory.ImagePicker.TakePhoto;
import com.bugingroup.mylistory.ImagePicker.UploadPhotot;
import com.bugingroup.mylistory.R;
import com.bugingroup.mylistory.utils.Utils;
import com.bugingroup.mylistory.videoRecorder.BaseActivity;
import com.bugingroup.mylistory.videoRecorder.utils.ConvertToUtils;
import com.bugingroup.mylistory.videoRecorder.widget.RecoderProgress;
import com.yixia.camera.MediaRecorderBase;
import com.yixia.camera.MediaRecorderNative;
import com.yixia.camera.VCamera;
import com.yixia.camera.model.MediaObject;
import com.yixia.camera.util.DeviceUtils;
import com.yixia.camera.util.FileUtils;
import com.yixia.videoeditor.adapter.UtilityAdapter;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @Author maimingliang@gmail.com
 *
 * Created by maimingliang on 2016/9/25.
 */
public class RecoderActivity extends BaseActivity implements MediaRecorderBase.OnErrorListener, MediaRecorderBase.OnEncodeListener {




    public static final String VIDEO_PATH="video_path";
    /**
     * 宽高比
     */
    private static  int WIDTH_RATIO = 4;
    private static  int HEIGHT_RATIO = 4;

    /**
     * 录制最长时间
     */
    public  static int RECORD_TIME_MAX = 60 * 1000;
    /**
     * 录制最小时间
     */
    public  static int RECORD_TIME_MIN = 3 * 1000;

    /**
     * 按住拍偏移距离
     */
    private static  float OFFSET_DRUTION = 25.0f;

    /**
     *titel_bar 取消颜色
     */
    private static  int TITEL_BAR_CANCEL_TEXT_COLOR = 0xd7b469;


    /**
     * 按住拍 字体颜色
     */
    private static  int PRESS_BTN_COLOR = 0xd7b469;

    /**
     * progress 小于录制最少时间的颜色
     */
    private static int LOW_MIN_TIME_PROGRESS_COLOR = 0xFFFC2828;
    /**
     * progress 颜色
     */
    private static int PROGRESS_COLOR = 0xFF00FF00;

    private static int PRESS_BTN_BG = 0xd7b469;

    /**
     * 对焦图片宽度
     */
    private int mFocusWidth;
    /**
     * 底部背景色
     */
    private int mBackgroundColorNormal, mBackgroundColorPress;
    /**
     * 屏幕宽度
     */
    private int mWindowWidth;

    /**
     * SDK视频录制对象
     */
    private MediaRecorderBase mMediaRecorder;
    /**
     * 视频信息
     */
    private MediaObject mMediaObject;

    /**
     * 对焦动画
     */
    private Animation mFocusAnimation;
    private boolean mCreated;


    private boolean isCancelRecoder;
    private boolean isRecoder;

    private Handler mHandler = new Handler();


    private static OnDialogListener mOnDialogListener;


    SurfaceView mSurfaceView;
    ImageView mImgRecordFocusing;
    TextView mTvRecoderTips;
    RecoderProgress mRecorderProgress;
    ImageView mBtnPress;
    RelativeLayout mRlRecorderBottom;
    RelativeLayout mRlBottomRecoder;

    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mCreated = false;
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 防止锁屏
        setContentView(R.layout.activity_video_recoder);
        ButterKnife.bind(this);
        initCustomerAttrs();
        initView();
        initData();
        mCreated = true;
    }


    @OnClick(R.id.back)
    void BackClick(){
        finish();
    }

    @OnClick(R.id.title)
    void titleClick(){
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Video"),704);

    }

    @OnClick(R.id.camera)
    void CameraClick(){
        startActivity(new Intent(RecoderActivity.this,TakePhoto.class));
        finish();
    }
    @OnClick(R.id.gallerey)
    void GallereyClick(){
        startActivity(new Intent(RecoderActivity.this,ImagePicker.class));
        finish();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 704) {
                Uri selectedImageUri = data.getData();

                // MEDIA GALLERY
               String selectedImagePath = getPath(selectedImageUri);
                System.out.println("11111"+selectedImagePath);
                if (selectedImagePath != null) {
                    System.out.println("2222"+selectedImagePath);
                    startActivity(new Intent(RecoderActivity.this, UploadPhotot.class).putExtra("path",selectedImagePath).putExtra("content_type","VIDEO"));
                }
            }
        }
    }

    // UPDATED!
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        UtilityAdapter.freeFilterParser();
        UtilityAdapter.initFilterParser();

        if (mMediaRecorder == null) {
            initMediaRecorder();
        } else {
            mMediaRecorder.prepare();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopRecord();
        UtilityAdapter.freeFilterParser();
        if (mMediaRecorder != null)
            mMediaRecorder.release();

    }

    /**
     * 初始化拍摄SDK
     */
    private void initMediaRecorder() {
        mMediaRecorder = new MediaRecorderNative();

        mMediaRecorder.setOnErrorListener(this);
        mMediaRecorder.setOnEncodeListener(this);
        File f = new File(VCamera.getVideoCachePath());
        if (!FileUtils.checkFile(f)) {
            f.mkdirs();
        }
        String key = String.valueOf(System.currentTimeMillis());
        mMediaObject = mMediaRecorder.setOutputDirectory(key,
                VCamera.getVideoCachePath());
        mMediaRecorder.setSurfaceHolder(mSurfaceView.getHolder());
        mMediaRecorder.prepare();
    }

    /**
     * 手动对焦
     */
    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
//    private boolean checkCameraFocus(MotionEvent event) {
//        mImgRecordFocusing.setVisibility(View.GONE);
//        float x = event.getX();
//        float y = event.getY();
//        float touchMajor = event.getTouchMajor();
//        float touchMinor = event.getTouchMinor();
//
//        Rect touchRect = new Rect((int) (x - touchMajor / 2),
//                (int) (y - touchMinor / 2), (int) (x + touchMajor / 2),
//                (int) (y + touchMinor / 2));
//        // The direction is relative to the sensor orientation, that is, what
//        // the sensor sees. The direction is not affected by the rotation or
//        // mirroring of setDisplayOrientation(int). Coordinates of the rectangle
//        // range from -1000 to 1000. (-1000, -1000) is the upper left point.
//        // (1000, 1000) is the lower right point. The width and height of focus
//        // areas cannot be 0 or negative.
//        // No matter what the zoom level is, (-1000,-1000) represents the top of
//        // the currently visible camera frame
//        if (touchRect.right > 1000)
//            touchRect.right = 1000;
//        if (touchRect.bottom > 1000)
//            touchRect.bottom = 1000;
//        if (touchRect.left < 0)
//            touchRect.left = 0;
//        if (touchRect.right < 0)
//            touchRect.right = 0;
//
//        if (touchRect.left >= touchRect.right
//                || touchRect.top >= touchRect.bottom)
//            return false;
//
//        ArrayList<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
//        focusAreas.add(new Camera.Area(touchRect, 1000));
//        if (!mMediaRecorder.manualFocus(new Camera.AutoFocusCallback() {
//
//            @Override
//            public void onAutoFocus(boolean success, Camera camera) {
//                // if (success) {
//                mImgRecordFocusing.setVisibility(View.GONE);
//                // }
//            }
//        }, focusAreas)) {
//            mImgRecordFocusing.setVisibility(View.GONE);
//        }
//
//        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mImgRecordFocusing
//                .getLayoutParams();
//        int left = touchRect.left - (mFocusWidth / 2);// (int) x -
//        // (focusingImage.getWidth()
//        // / 2);
//        int top = touchRect.top - (mFocusWidth / 2);// (int) y -
//        // (focusingImage.getHeight()
//        // / 2);
//        if (left < 0)
//            left = 0;
//        else if (left + mFocusWidth > mWindowWidth)
//            left = mWindowWidth - mFocusWidth;
//        if (top + mFocusWidth > mWindowWidth)
//            top = mWindowWidth - mFocusWidth;
//
//        lp.leftMargin = left;
//        lp.topMargin = top;
//        mImgRecordFocusing.setLayoutParams(lp);
//        mImgRecordFocusing.setVisibility(View.VISIBLE);
//
//        if (mFocusAnimation == null)
//            mFocusAnimation = AnimationUtils.loadAnimation(this,
//                    R.anim.record_focus);
//
//        mImgRecordFocusing.startAnimation(mFocusAnimation);
//
////		mHandler.sendEmptyMessageDelayed(HANDLE_HIDE_RECORD_FOCUS, 3500);// 最多3.5秒也要消失
//        return true;
//    }


    private void initCustomerAttrs() {



        int maxTime = getIntent().getIntExtra(CommonCons.RECORD_TIME_MAX, 0);

        if(maxTime != 0){
            RECORD_TIME_MAX = maxTime;
        }

        int minTime = getIntent().getIntExtra(CommonCons.RECORD_TIME_MIN, 0);

        if(minTime != 0){
            RECORD_TIME_MIN = minTime;
        }

        int offset = getIntent().getIntExtra(CommonCons.OFFSET_DRUTION, 0);

        if(offset != 0){
            OFFSET_DRUTION = offset;
        }

        int cancelColor = getIntent().getIntExtra(CommonCons.TITEL_BAR_CANCEL_TEXT_COLOR, 0);

        if(cancelColor != 0){
            TITEL_BAR_CANCEL_TEXT_COLOR = cancelColor;
        }

        int btnColor = getIntent().getIntExtra(CommonCons.PRESS_BTN_COLOR, 0);

        if(btnColor != 0){
            PRESS_BTN_COLOR = btnColor;
        }

        int minTimeProgressColor = getIntent().getIntExtra(CommonCons.LOW_MIN_TIME_PROGRESS_COLOR, 0);

        if(minTimeProgressColor != 0 ){
            LOW_MIN_TIME_PROGRESS_COLOR = minTimeProgressColor;
        }
        int color = getIntent().getIntExtra(CommonCons.PROGRESS_COLOR, 0);


        if(color != 0){
            PROGRESS_COLOR = color;
        }

        int pressbg = getIntent().getIntExtra(CommonCons.PRESS_BTN_BG, 0);

        if(pressbg != 0){
            PRESS_BTN_BG = pressbg;
        }

    }


    private void initData() {
        mWindowWidth = DeviceUtils.getScreenWidth(this);

        mFocusWidth = ConvertToUtils.dipToPX(this, 64);
        try {
            mImgRecordFocusing.setImageResource(R.drawable.ms_video_focus_icon);
        } catch (OutOfMemoryError e) {
            Log.e("maiml",e.getMessage());
        }


        mRecorderProgress.setMaxTime(RECORD_TIME_MAX);
        mRecorderProgress.setMinRecordertime(RECORD_TIME_MIN);
        mRecorderProgress.setLowMinTimeProgressColor(LOW_MIN_TIME_PROGRESS_COLOR);
        mRecorderProgress.setProgressColor(PROGRESS_COLOR);

        setListener();
    }


    private void startRecoder() {


        isCancelRecoder = false;
        mTvRecoderTips.setText("Прокрутите вверх, чтобы удалить");
        mTvRecoderTips.setTextColor(getResources().getColor(R.color.red));
        mTvRecoderTips.setBackgroundColor(getResources().getColor(R.color.transparent));
        mTvRecoderTips.setVisibility(View.VISIBLE);
        if (mMediaRecorder == null) {
            return;
        }
        MediaObject.MediaPart part = mMediaRecorder.startRecord();
        if (part == null) {
            return;
        }
        this.mRecorderProgress.startAnimation();
        isRecoder = true;

    }


    private void startEncoding() {
        mMediaRecorder.startEncoding();
    }


    private void stopAll() {

        mTvRecoderTips.setVisibility(View.INVISIBLE);
        mRecorderProgress.stopAnimation();
        stopRecord();
        isRecoder = false;


    }

    private void releaseCancelRecoder() {


        isCancelRecoder = true;
        mTvRecoderTips.setTextColor(getResources().getColor(R.color.white));
        mTvRecoderTips.setBackgroundColor(getResources().getColor(R.color.red));
        mTvRecoderTips.setVisibility(View.VISIBLE);
        mTvRecoderTips.setText("Отменить запись выпустили");


    }

    private void slideCancelRecoder() {

        isCancelRecoder = false;
        mTvRecoderTips.setText("Прокрутите вверх, чтобы удалить");
        mTvRecoderTips.setTextColor(getResources().getColor(R.color.white));
        mTvRecoderTips.setBackgroundColor(getResources().getColor(R.color.transparent));
        mTvRecoderTips.setVisibility(View.VISIBLE);


    }

    private void recoderShortTime() {
        mTvRecoderTips.setText("Время записи является слишком коротким");
        mTvRecoderTips.setVisibility(View.VISIBLE);
        mTvRecoderTips.setTextColor(getResources().getColor(R.color.white));
        mTvRecoderTips.setBackgroundColor(getResources().getColor(R.color.red));
        removeRecoderPart();
        mHandler.postDelayed(mRunable, 1000l);
    }

    private void hideRecoderTxt() {
        mTvRecoderTips.setVisibility(View.INVISIBLE);
        if(isRecoder){
            mTvRecoderTips.setVisibility(View.VISIBLE);
        }
    }

    private void removeRecoderPart() {
        // 回删
        if (mMediaObject != null) {
            mMediaObject.removeAllPart();

        }
    }


    /**
     * 停止录制
     */
    private void stopRecord() {
        if (mMediaRecorder != null) {
            mMediaRecorder.stopRecord();
        }
    }

    private Runnable mRunable = new Runnable() {
        @Override
        public void run() {
            hideRecoderTxt();
        }
    };

    private void startRecoderAnim(View paramView) {
        AnimatorSet locald = new AnimatorSet();
        locald.playTogether(
                ObjectAnimator.ofFloat(paramView, "scaleX", new float[]{1.0F, 1.2F, 1.5F}),
                ObjectAnimator.ofFloat(paramView, "scaleY", new float[]{1.0F, 1.2F, 1.5F}),
                ObjectAnimator.ofFloat(paramView, "alpha", new float[]{1.0F, 0.25F, 0.0F})
        );

        locald.setDuration(300L).start();
    }

    private void stopRecoderAnim(View paramView) {
        AnimatorSet locald = new AnimatorSet();
        locald.playTogether(
                ObjectAnimator.ofFloat(paramView, "scaleX", new float[]{1.5F, 1.2F, 1.0F}),
                ObjectAnimator.ofFloat(paramView, "scaleY", new float[]{1.5F, 1.2F, 1.0F}),
                ObjectAnimator.ofFloat(paramView, "alpha", new float[]{0.0F, 0.25F, 1.0F})
        );

        locald.setDuration(300L).start();
    }


    private void setListener() {
        if (DeviceUtils.hasICS()) {
            mSurfaceView.setOnTouchListener(onSurfaveViewTouchListener);
        }

        mBtnPress.setOnTouchListener(onVideoRecoderTouchListener);

    }


    private void initView() {

        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        mImgRecordFocusing = (ImageView) findViewById(R.id.img_record_focusing);
        mTvRecoderTips = (TextView) findViewById(R.id.tv_recoder_tips);
        mRecorderProgress = (RecoderProgress) findViewById(R.id.recorder_progress);
        mBtnPress = (ImageView) findViewById(R.id.btn_press);
        mRlRecorderBottom = (RelativeLayout) findViewById(R.id.rl_recorder_bottom);
        mRlBottomRecoder = (RelativeLayout) findViewById(R.id.ll_bottom_recoder);
    }


    /**
     * 点击屏幕录制
     */
    private View.OnTouchListener onVideoRecoderTouchListener = new View.OnTouchListener() {
         private float startY;
         private float moveY;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (mMediaRecorder == null) {
                return false;
            }

            switch (event.getAction()) {

                default:
                    return true;
                case MotionEvent.ACTION_DOWN:

                    startY = event.getY();
                    startRecoderAnim(mBtnPress);
                    startRecoder();
                    break;
                case MotionEvent.ACTION_MOVE:
                    int durationMove = mMediaObject.getDuration();
                    if (durationMove >= RECORD_TIME_MAX) {
                        stopAll();
                        stopRecoderAnim(mBtnPress);
                        return true;
                    }
                    moveY = event.getY();
                    float drution = moveY - startY;

                    if ((drution > 0.0f) && Math.abs(drution) > OFFSET_DRUTION) {
                        slideCancelRecoder();

                    }
                    if ((drution < 0.0f) && (Math.abs(drution) > OFFSET_DRUTION)) {
                        releaseCancelRecoder();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    stopAll();
                    stopRecoderAnim(mBtnPress);
                    if (isCancelRecoder) {
                        hideRecoderTxt();
                        removeRecoderPart();
                        return true;
                    }
                    int duration = mMediaObject.getDuration();
                    if (duration < RECORD_TIME_MIN) {
                        recoderShortTime();
                        return true;
                    }
                    startEncoding();
                    break;

            }

            return true;


        }

    };


    /**
     * 点击屏幕录制
     */
    private View.OnTouchListener onSurfaveViewTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (mMediaRecorder == null || !mCreated) {
                return false;
            }

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // 检测是否手动对焦
//                    if (checkCameraFocus(event))
//                    return true;
                    break;
            }
            return true;
        }

    };


    @Override
    public void onVideoError(int what, int extra) {

    }

    @Override
    public void onAudioError(int what, String message) {

    }

    @Override
    public void onEncodeStart() {

        if(mOnDialogListener != null){
            mOnDialogListener.onShowDialog(this);
        }else {
            showProgress("","обрабатывается...");

        }
    }

    @Override
    public void onEncodeProgress(int progress) {

    }

    @Override
    public void onEncodeComplete() {

        if (mOnDialogListener != null){
            mOnDialogListener.onHideDialog(this);
        }else{
            hideProgress();
        }


        String outputVideoPath = mMediaObject.getOutputVideoPath();
        startActivity(new Intent(RecoderActivity.this, UploadPhotot.class).putExtra("path",outputVideoPath).putExtra("content_type","VIDEO"));
          }

    @Override
    public void onEncodeError() {
        if(mOnDialogListener != null){
            mOnDialogListener.onHideDialog(this);
        }else{
            hideProgress();
        }
        Toast.makeText(this, "Отказ транскодирование видео",
                Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onBackPressed() {
        if (mMediaObject != null)
            mMediaObject.delete();
        finish();
        overridePendingTransition(R.anim.push_bottom_in, R.anim.push_bottom_out);
    }



}
