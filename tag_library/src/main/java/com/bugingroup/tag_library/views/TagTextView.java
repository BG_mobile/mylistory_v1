package com.bugingroup.tag_library.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bugingroup.tag_library.DIRECTION;
import com.bugingroup.tag_library.R;
import com.bugingroup.tag_library.utils.DipConvertUtils;


public class TagTextView extends TextView implements ITagView {

    private DIRECTION mDirection;

    public TagTextView(Context context) {
        this(context, null);
    }

    public TagTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TagTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTextColor(Color.BLACK);
        setTextSize(13);
        setBackground(getResources().getDrawable(R.drawable.mark));
      //  setShadowLayer(7, 0, 0, Color.BLACK);
      //  setBackgroundColor(Color.parseColor("#99000000"));
        setPadding(DipConvertUtils.dip2px(getContext(), 12), DipConvertUtils.dip2px(getContext(), 4)
                , DipConvertUtils.dip2px(getContext(), 12), DipConvertUtils.dip2px(getContext(), 4));
    }

    @Override
    public void setDirection(DIRECTION direction) {
        mDirection = direction;
    }

    @Override
    public DIRECTION getDirection() {
        return mDirection;
    }
}
